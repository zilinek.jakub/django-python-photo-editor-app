# @author Kuba
from django.apps import AppConfig
import subprocess


class ApiCore(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api_core'

    def ready(self):
        pass
        # print("[PreRunConfig] Type code that should be run before runserver")
