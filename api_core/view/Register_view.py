from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView
from ..serializers import *


class RegisterView(APIView):

    @swagger_auto_schema(operation_summary="Register new User",
                         responses={400 : 'username is NOT uniq or'
                                          ' email is NOT in right format or'
                                          ' request has bad format',
                                    status.HTTP_201_CREATED : openapi.Response(description="response",
                                                                               schema = UserSerializer())},
                         request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                                     required=["username", "email", "password"],
                                                     properties={
                                                         'username': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                    description=''),
                                                         'name': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                    description='name and surname default=EMPTY'),
                                                         'email': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                    description='email in right format'),
                                                         'password': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                    description=''),
                                                         'free_trial': openapi.Schema(type=openapi.TYPE_BOOLEAN,
                                                                                description='default=True'),
                                                     })
                         )
    def post(self, request):
        ##TODO Should we make token when we register new user ..?
        dat = request.data
        user = {'username': '', 'name': 'EMPTY', 'email': '', 'password': '', 'free_trial': True}
        for i in dat:
            user[i] = dat[i]
        serializer = UserSerializer(data=user)
        return serializer_validation(serializer)
