from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_account import AccountTestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_flat_rate_type import FlatRateTypeTestData
from test.api_core.test_role import RoleTestData
from test.api_core.test_user import UserTestData
from test.api_core.utility import date_str_eq


class FlatRateCoreFuncSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        available_edits = []
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)

        for idx, ent in enumerate(EditTestData.VALID_DATA):
            available_edits.append({"edit_name": ent["name"]})

        self.create_dependency_data(reverse('ban_edit'), available_edits)
        self.create_dependency_data(reverse('FR_type'), FlatRateTypeTestData.VALID_DATA)
        self.create_dependency_data(reverse('account'), AccountTestData.VALID_DATA)
        self.create_dependency_data(reverse('role'), RoleTestData.VALID_DATA)
        self.create_dependency_data(reverse('user'), UserTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse('FR')

    def get_test_data(self) -> TestData:
        return FlatRateTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("user"), 1,
                                              UserTestData.get_pk_field_name(),
                                              [FlatRateTestData.VALID_DATA[0],
                                               FlatRateTestData.VALID_DATA[1]],
                                              self.method_test_single)

        self._shared_suite.check_type_cascade(reverse("FR_type"), 4,
                                              FlatRateTypeTestData.get_pk_field_name(),
                                              [FlatRateTestData.VALID_DATA[2]],
                                              self.method_test_single)


class FlatRateTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        d = {"valid_from": date_str_eq,
             "valid_to": date_str_eq}
        if name not in d:
            return val1 == val2
        return d[name](val1, val2)

    IRRELEVANT_KEY = "irrelevant_field"
    VALID_DATA = [
        {
            "valid_from": "2022-02-22T15:00:00Z",
            "valid_to": "2023-02-22T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 3
        }, {
            "valid_from": "2022-05-03T15:00:00Z",
            "valid_to": "2023-02-22T17:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 2
        }, {
            IRRELEVANT_KEY: 15,
            "valid_from": "2022-08-22T15:00:00Z",
            "valid_to": "2025-02-01T15:00:00Z",
            "fk_user": 3,
            "fk_flat_rate_type": 4
        }, {
            "valid_from": "2020-02-22T15:00:00Z",
            "valid_to": "2023-07-06T10:00:00Z",
            "fk_user": 2,
            "fk_flat_rate_type": 1
        }
    ]
    VALID_DATA_PUT = [
        {
            "id": 1,
            "valid_from": "2022-02-22T15:00:00Z",
            "valid_to": "2024-02-22T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 2
        }, {
            "id": 3,
            IRRELEVANT_KEY: 15,
            "valid_from": "2023-08-22T15:00:00Z",
            "valid_to": "2025-02-01T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 4
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": 2,
            "valid_to": "2023-02-22T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 3
        }, {
            "id": 2,
            "valid_from": "2023-02-22T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 3
        }, {
            "id": 1,
            "valid_to": "2023-02-22T15:00:00Z",
            "valid_from": "2024-02-22T15:00:00Z",
        },
        {}
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "valid_from": "2020-02-22T15:00:00Z",
            "valid_to": "2023-07-06T10:00:00Z",
            "fk_user": 8,
            "fk_flat_rate_type": 1
        }, {
            "id": 2,
            "valid_from": "2020-02-22T15:00:00Z",
            "valid_to": "2023-07-06T10:00:00Z",
            "fk_user": 2,
            "fk_flat_rate_type": 7
        }, {
            "id": 2,
            "valid_from": "2023-07-06T10:00:00Z",
            "valid_to": "2020-02-22T15:00:00Z",
            "fk_user": 2,
            "fk_flat_rate_type": 1
        }
    ]

    INVALID_DATA_400 = [
        {
            "valid_to": "2023-02-22T15:00:00Z",
            "valid_from": "2024-02-22T15:00:00Z"
        }, {
            "daily_limit": 153,
            "fk_user": 1,
        }, {
            "valid_from": "today",
            "valid_to": "2024-02-22T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": 2
        }, {
            "valid_from": "2023-02-22T15:00:00Z",
            "valid_to": "2024-02-22T15:00:00Z",
            "fk_user": 1,
            "fk_flat_rate_type": "IO"
        }, {}, ""
    ]
    INVALID_DATA_422 = INVALID_DATA_PUT_422
