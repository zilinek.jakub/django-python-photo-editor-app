from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from ..serializers import *
from rest_framework.views import APIView


class ParameterTypeView(APIView):
    res_many = {400: 'name is empty or request has a bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=ParameterTypeSerializer(many=True))}
    res_query = {404: 'object for this slug wasn\'t found',
                 400: '?name in query wasn\'t specified or request has bad format',
                 200: openapi.Response(description="response",
                                       schema=ParameterTypeSerializer())}
    single = openapi.Response(description="response",
                              schema=ParameterTypeSerializer())

    __NAME_FIELD_NAME__ = 'name'

    @swagger_auto_schema(operation_summary=doc_sum_get("ParamType"),
                         operation_description=doc_desc_get("ParamType"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("param_type")])
    def get(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        if _pk:
            return get_by_pk(_pk, ParameterType, ParameterTypeSerializer, None)
        param_type = ParameterType.objects.all()
        serializer = ParameterTypeSerializer(param_type, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new ParamType object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=ParameterTypeSerializer)
    def post(self, request):
        serializer = ParameterTypeSerializer(data=request.data)
        return serializer_validation(serializer)

    @staticmethod
    def __validate(pk):
        return pk

    @swagger_auto_schema(operation_summary="Update ParamType",
                         operation_description=doc_desc_put("ParamType"),
                         responses=res_query,
                         manual_parameters=[doc_parm_id('param_type')],
                         request_body=ParameterTypeSerializer)
    def put(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        return put_by_pk(_pk, ParameterType, ParameterTypeSerializer, request, self.__validate)

    @swagger_auto_schema(operation_summary="Delete ParamType",
                         operation_description=doc_desc_delete("ParamType"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('param_type')])
    def delete(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        return delete_by_pk(_pk, ParameterType, self.__validate)
