#!/bin/bash

rm -rf db.sqlite3;
python3 manage.py makemigrations;
python3 manage.py migrate --run-syncdb;
