"""
@requires 'redmine' file in project root directory, this file needs to be in textile syntax

@return 0 on succesfully uploading file to redmine wiki page, -1 if not success
"""
import sys

from redminelib import Redmine

redmine = Redmine('https://dbs.fit.cvut.cz/redmine/', key="5e4fa2e4344c32c584255a8f993283117c253017")

try:
    file = open("../README.textile")
    text_of_wiki_page = file.read()
    file.close()

    redmine.wiki_page.update(
        'Mediagraphix readme',
        project_id='50',
        title='Mediagraphix readme',
        text=text_of_wiki_page,
        uploads=[{'path': '../README.md'}]
    )
except Exception as x:
    print(x)
    sys.exit(-1)
print("\nSuccess! \t> README updated.")
sys.exit(0)
