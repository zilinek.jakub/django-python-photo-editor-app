from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from api_core.views import *


class Param_values_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=ParameterValuesSerializer(many=True))}
    res_quary = {404: 'object for this slug wasn\'t found',
                 400: '?id in quary wasn\'t specified or request has bad format',
                 200: openapi.Response(description="response",
                                       schema=ParameterValuesSerializer())}
    single = openapi.Response(description="response",
                              schema=ParameterValuesSerializer())

    def find_by_id(self, _id):
        try:
            return ParameterValue.objects.get(pk=_id)
        except ParameterValue.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("ParamValues"),
                         operation_description=doc_desc_get("ParamValues"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("param_values")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, ParameterValuesSerializer)
        price = ParameterValue.objects.all()
        serializer = ParameterValuesSerializer(price, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new ParamValues object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=ParameterValuesSerializer)
    def post(self, request):
        serializer = ParameterValuesSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update ParamValues",
                         operation_description=doc_desc_put("ParamValues"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('param_values')],
                         request_body=ParameterValuesSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, ParameterValuesSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete ParamValues",
                         operation_description=doc_desc_delete("ParamValues"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('param_values')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
