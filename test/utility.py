import os
from pathlib import Path


def get_path(path, file_of_call=__file__):
    return Path(os.path.dirname(file_of_call)).joinpath(path).absolute().__str__()
