from typing import Any

import requests
from django.urls import reverse
from rest_framework import status

from test.api_core import utility
from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_defined_parameter import DefinedParameterTestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_edit_configuration import EditConfigurationTestData
from test.api_core.test_parameter_type import ParameterTypeTestData
from test.api_core.test_parameter_value import ParameterValueTestData
from test.api_core.test_photo import PhotoTestData
from test.api_core.test_request import RequestTestData
from test.api_core.test_role import RoleTestData
from test.api_core.test_user import UserTestData

USER_DEPENDENCY = [
    {
        "username": "abs(legend)",
        "name": "Adam Procházka",
        "email": "adam.prochazka@profifoto.cz",
        "password": "123456789",
        "free_trial": False,
        "fk_account": None,
        "roles": [2]
    }, {
        "username": "novmar7",
        "name": "Marek Novotný",
        "email": "novmar7@photofire.com",
        "password": "251251251oo",
        "free_trial": False,
        "fk_account": None,
        "roles": [1, 3]
    }, {
        "username": "PanK",
        "name": "Petr Mok",
        "email": "mokp@photofire.com",
        "password": "weeI112-.8",
        "free_trial": False,
        "fk_account": None,
        "roles": [3]
    }
]


class OutputTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('role'), RoleTestData.VALID_DATA)
        self.create_dependency_data(reverse('user'), USER_DEPENDENCY)

        self.create_dependency_data(reverse('type_param'), ParameterTypeTestData.VALID_DATA)
        self.create_dependency_data(reverse("edit"), EditTestData.VALID_DATA)
        data = DefinedParameterTestData.VALID_DATA
        for edit in data:
            edit.pop(DefinedParameterTestData.IRRELEVANT_KEY, None)
        self.create_dependency_data(reverse('def_param'), data)
        self.create_dependency_data(reverse('values'), ParameterValueTestData.VALID_DATA)
        self.create_dependency_data(reverse("edit_conf"), EditConfigurationTestData.VALID_DATA)

        photo_test_data = PhotoTestData()
        photo_data = photo_test_data.get_test_data("post", status.HTTP_201_CREATED).copy()
        photo_data.pop()
        self.create_dependency_data(reverse("photo"), photo_data, form="multipart")
        self.create_dependency_data(reverse("request"), RequestTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse("output")

    def get_test_data(self) -> TestData:
        return OutputTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("user"), 1,
                                              UserTestData.get_pk_field_name(),
                                              [OutputTestData.VALID_DATA[0]],
                                              self.method_test_single)
        # Putting one as an id is not possible as it was cascaded by deleting the user who issued
        # the request
        self._shared_suite.check_type_cascade(reverse("request"), 2,
                                              RequestTestData.get_pk_field_name(),
                                              [OutputTestData.VALID_DATA[1]],
                                              self.method_test_single)


class OutputTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_409,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_404_NOT_FOUND): self.INVALID_DATA_PUT_404,
            ("put", status.HTTP_409_CONFLICT): self.INVALID_DATA_PUT_409,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        data = {
            "datetime": utility.date_str_eq
        }
        if name not in data:
            return utility.eq("none", val1, val2)
        return data[name](val1, val2)

    IRRELEVANT_KEY = "sadkjfhasdkjhf"

    VALID_DATA = [
        {
            "datetime": "2021-11-11T02:11:31+00:00",
            "fk_user": 1,
            "request": 1
        }, {
            "fk_user": 2,
            "request": 2,
            IRRELEVANT_KEY: "test"
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "fk_user": 2,
            "request": 3
        }, {
            "id": 2,
            "fk_user": 3,
            "request": 2,
            IRRELEVANT_KEY: "test"
        }
    ]

    INVALID_DATA_400 = [
        {
            "request": 3,
            "datetime": "2021-11-11T02:11:31+00:00"
        }, {
            "fk_user": 3,
            "datetime": "2021-11-11T02:11:31+00:00"
        }, {
            "request": 3,
            "fk_user": 3,
            "datetime": "this is not the time you are looking for"
        }, {
            "request": "this is not id",
            "fk_user": 3,
            "datetime": "2021-11-11T02:11:31+00:00"
        }, {
            "request": 3,
            "fk_user": "this is not id",
            "datetime": "2021-11-11T02:11:31+00:00"
        }
    ]

    INVALID_DATA_409 = [
        {
            "fk_user": 3,
            "request": 1
        }
    ]

    INVALID_DATA_422 = [
        {
            "fk_user": 50,
            "request": 3,
        }, {
            "fk_user": 3,
            "request": 50
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": 1,
            "request": 3,
            "datetime": "2021-11-11T02:11:31+00:00"
        }, {
            "id": 1,
            "fk_user": 3,
            "datetime": "2021-11-11T02:11:31+00:00"
        }, {
            "id": 1,
            "request": 3,
            "fk_user": 3,
            "datetime": "this is not the time you are looking for"
        }, {
            "id": 1,
            "request": "this is not id",
            "fk_user": 3,
            "datetime": "2021-11-11T02:11:31+00:00"
        }, {
            "id": 1,
            "request": 3,
            "fk_user": "this is not id",
            "datetime": "2021-11-11T02:11:31+00:00"
        }
    ]

    INVALID_DATA_PUT_404 = [
        {
            "id": 50,
            "fk_user": 3,
            "request": 3
        }
    ]

    INVALID_DATA_PUT_409 = [
        {
            "id": 2,
            "fk_user": 3,
            "request": 1
        }
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "fk_user": 50,
            "request": 3,
        }, {
            "id": 1,
            "fk_user": 3,
            "request": 50
        }
    ]