from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class EditView(APIView):
    __NAME_FIELD_NAME__ = 'name'

    res_many = {400: 'The request has a bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=EditSerializer(many=True))}
    res_query = {404: 'object for this slug wasn\'t found',
                 400: '?name in query wasn\'t specified or the request has a bad format',
                 200: openapi.Response(description="response",
                                       schema=EditSerializer())}
    single = openapi.Response(description="response",
                              schema=EditSerializer())

    @staticmethod
    def __validation(pk):
        return pk

    @swagger_auto_schema(operation_summary=doc_sum_get("Edit"),
                         operation_description=doc_desc_get("Edit"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("edit")])
    def get(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        if _pk:
            return get_by_pk(_pk, Edit, EditSerializer, None)
        edit = Edit.objects.all()
        serializer = EditSerializer(edit, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Edit object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=EditSerializer)
    def post(self, request):
        serializer = EditSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Edit",
                         operation_description=doc_desc_put("Edit"),
                         responses=res_query,
                         manual_parameters=[doc_parm_id('edit')],
                         request_body=EditSerializer)
    def put(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        return put_by_pk(_pk, Edit, EditSerializer, request, self.__validation)

    @swagger_auto_schema(operation_summary="Delete Edit",
                         operation_description=doc_desc_delete("Edit"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('edit')])
    def delete(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        return delete_by_pk(_pk, Edit, self.__validation)
