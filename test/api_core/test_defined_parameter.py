from typing import Any, Optional

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_parameter_type import ParameterTypeTestData
from test.api_core.utility import create_id_url, format_assert_code_mismatch_help


class DefinedParameterCoreFuncTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)
        self.create_dependency_data(reverse('type_param'), ParameterTypeTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse('def_param')

    def get_test_data(self) -> TestData:
        return DefinedParameterTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("type_param"), "string",
                                              ParameterTypeTestData.get_pk_field_name(),
                                              [DefinedParameterTestData.VALID_DATA[4],
                                               DefinedParameterTestData.VALID_DATA[5]],
                                              self.method_test_single)

    def test_core_functionality_put_fail_extra_malformed(self):
        self.create_post_data()

        id_field_name = self._test_data.get_pk_field_name()
        data = self._test_data.get_test_data("put_special", status.HTTP_400_BAD_REQUEST)

        self.method_test_data_src(self.client.put, data, status.HTTP_400_BAD_REQUEST,
                                  url_get=lambda index, ent:
                                  create_id_url(self._url, "width", id_field_name))

    def test_core_functionality_get_fail(self):
        response = self.client.delete(create_id_url(self._url,
                                                    "widthv2",
                                                    DefinedParameterTestData.get_pk_field_name()))
        self.assertEqual(response.status_code,
                         status.HTTP_404_NOT_FOUND,
                         format_assert_code_mismatch_help({"primary_key": "widthv2"},
                                                          response.status_code,
                                                          status.HTTP_404_NOT_FOUND,
                                                          response.content))

    def test_core_functionality_delete_fail(self):
        self._shared_suite.test_core_functionality_delete_fail(
            DefinedParameterTestData.get_pk_field_name(),
            "widthv6",
            "",
            self.create_post_data
        )


class DefinedParameterTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_409,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_404_NOT_FOUND): self.INVALID_DATA_PUT_404,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
            ("put_special", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_SPECIAL_PUT_400
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def get_pk_from_data(cls, data) -> Optional[str]:
        return data["name"] if "name" in data else None

    @classmethod
    def get_pk_field_name(cls) -> str:
        return "name"

    IRRELEVANT_KEY = "irrelevant_field"
    VALID_DATA = [
        {
            "name": "width",
            "fk_parameter_type": "int",
            "fk_edit": ["Watermark"]
        }, {
            "name": "height",
            "fk_parameter_type": "int",
            "fk_edit": ["Watermark"]
        }, {
            "name": "size_mp",
            "fk_parameter_type": "float",
            "fk_edit": ["Resize"],
            IRRELEVANT_KEY: "bflmpsvz"
        }, {
            "name": "make_super_awesome",
            "fk_parameter_type": "bool",
            "fk_edit": ["CrazyEdit"]
        }, {
            "name": "description",
            "fk_parameter_type": "string",
            "fk_edit": ["Reformat"]
        }, {
            "name": "format",
            "fk_parameter_type": "string",
            "fk_edit": ["AllIn1", "Reformat"]
        }]

    VALID_DATA_PUT = [
        {
           "name": "size_mp",
           "fk_parameter_type": "double",
           "fk_edit": ["Resize"],
           IRRELEVANT_KEY: "bflmpsvz"
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "name": "",
            "fk_parameter_type": "int"
        }, {
            "name": "width",
            "fk_edit": ["AllIn1", "Reformat"]
        }, {
            "name": "width",
            "fk_parameter_type": ""
        }, {
            "fk_parameter_type": "",
            "name": ""
        }]
    INVALID_DATA_PUT_422 = [
        {
            "name": "width",
            IRRELEVANT_KEY: "bflmpsvz",
            "fk_parameter_type": "bint",
            "fk_edit": ["Watermark", "Reformat"]
        }]

    INVALID_DATA_PUT_404 = [
        {
            "name": "heightv2",
            "fk_parameter_type": "int",
            "fk_edit": ["Watermark", "Reformat"]
        }
    ]

    INVALID_DATA_SPECIAL_PUT_400 = [
        "", {}, "{\"name\":\"width\"}"
    ]

    INVALID_DATA_400 = [
        {
            "fk_parameter_type": "int"
        }, {
            "name": "widthv2"
        }, {
            "name": "widthv2",
            "fk_parameter_type": ""
        }, {
            "name": ""
        }, "", {}, {
            "name": "yes, parma",
            "fk_edit": "Reformat",
            "fk_parameter_type": "string"
        }]
    INVALID_DATA_422 = [
        {
            "name": "widthv2",
            "fk_parameter_type": "bloat",
            "fk_edit": ["Watermark", "Reformat"]
        }
    ]
    INVALID_DATA_409 = [
        {
            "name": "width",
            "fk_parameter_type": "int",
            "fk_edit": ["Watermark", "Reformat"]
        }, {
            "name": "height",
            "fk_parameter_type": "string",
            "fk_edit": ["Watermark"]
        }]
