import os
import uuid

from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
import mimetypes
import json
from PIL import Image

import api_core.views
import src.photo
from api_core.views import *
from src.edit.reformat import Reformat
from src.edit.resize import Resize
from src.edit.watermark import Watermark


def getPhoto(photo):
    serializer = PhotoFileSerializer(data=photo)
    if serializer.is_valid():
        try:
            file = serializer.validated_data.get("file")
            mime_type = mimetypes.guess_extension(file.content_type)
            file_name = './income_photos/' + uuid.uuid4().hex + mime_type
            with open(file_name, "wb") as f:
                f.write(file.read())
            file.flush()
            file.close()

            photo2 = api_core.views.Photo.objects.create()
            OriginalPhoto(photo_path=file_name, photo=photo2).save()
            return (True, file_name, photo2)
        except Exception as e:
            return (False, serializer)

    return (False, serializer)


class New_Request_view(APIView):
    req = {'photo': openapi.Schema(type=openapi.TYPE_FILE),
           'watermark_<n>': openapi.Schema(type=openapi.TYPE_FILE,
                                           description="n-th watermark file"),
           'json': openapi.Schema(
               type=openapi.TYPE_OBJECT,
               required=["username"],
               properties={
                   'username': openapi.Schema(type=openapi.TYPE_STRING),
                   'edits': openapi.Schema(type=openapi.TYPE_ARRAY,
                                           description="Array of edit. Each object represents one "
                                                       "edit. "
                                                       "Edits will be run in given order.",
                                           items=openapi.Schema(
                                               type=openapi.TYPE_OBJECT,
                                               required=["edit", "parameter_values"],
                                               properties={
                                                   'edit': openapi.Schema(type=openapi.TYPE_STRING,
                                                                          description="name of "
                                                                                      "edit("
                                                                                      "watermark, "
                                                                                      "resize...)"),
                                                   'parameter_values': openapi.Schema(
                                                       type=openapi.TYPE_OBJECT,
                                                       description="Parameters for this edit.",
                                                       properties={
                                                           'x': openapi.Schema(
                                                               type=openapi.TYPE_INTEGER),
                                                           'y': openapi.Schema(
                                                               type=openapi.TYPE_INTEGER),
                                                           'format':
                                                               openapi.Schema(
                                                                   type=openapi.TYPE_STRING),
                                                           'width': openapi.Schema(
                                                               type=openapi.TYPE_INTEGER),
                                                           'height': openapi.Schema(
                                                               type=openapi.TYPE_INTEGER),
                                                           'percent': openapi.Schema(
                                                               type=openapi.TYPE_INTEGER),
                                                           'transparency': openapi.Schema(
                                                               type=openapi.TYPE_INTEGER),
                                                       })}))})}

    implemented_edits = ('watermark', 'resize', 'reformat')
    all_edits = ('watermark', 'resize', 'reformat', 'object_detection', 'metadata_editor',
                 'compress')

    def watermark_check(self, param):
        if 'x' not in param or 'y' not in param or 'height' not in param \
                or 'width' not in param or 'transparency' not in param:
            return False
        return True

    def resize_check(self, param):
        if 'height' not in param or 'width' not in param:
            return False
        return True

    def reformat_check(self, param):
        if 'format' not in param:
            return False
        return True

    def check_req_form(self, request):
        if 'username' not in request:
            return 'Username in request body is missing'
        if 'edits' not in request:
            return 'Edits in request body is missing'
        for edit in request['edits']:
            if "edit" not in edit:
                return 'One of the edit does not have edit name'
            if "parameter_values" not in edit:
                return 'One of edit does not have parameter_values'
            if edit['edit'] == 'watermark':
                if not self.watermark_check(edit['parameter_values']):
                    return 'Wrong value in watermark parameters'
            if edit['edit'] == 'resize':
                if not self.resize_check(edit['parameter_values']):
                    return 'Wrong value in resize parameters'
            if edit['edit'] == 'reformat':
                if not self.reformat_check(edit['parameter_values']):
                    return 'Wrong value in reformat parameters'
            if edit['edit'] == 'object_detection':
                return None  ##TODO
            if edit['edit'] == 'compress':
                return None  ##TODO
        return None

    def find_by_username(self, _user):
        try:
            return User.objects.get(username=_user)
        except User.DoesNotExist:
            return None

    def find_by_account_id(self, _id):
        try:
            return Account.objects.get(pk=_id)
        except Account.DoesNotExist:
            return None

    ## TODO check if user have flat rate
    def check_user_money(self, user, required_money):
        if not user.fk_account_id:
            return False
        account = self.find_by_account_id(user.fk_account_id)
        print(account.credit, account.credit < required_money)
        if account.credit < required_money:
            return False
        account.credit = account.credit - required_money
        account.save()
        return True

    def find_by_edit(self, name):
        try:
            return Price.objects.get(edit=name)
        except Price.DoesNotExist:
            return None

    def make_operation(self, request_json, request, path, user, total_price, photo_id):
        edits = request_json["edits"]
        edit_list = list()
        watermark_paths = list()

        for edit in edits:
            param = edit['parameter_values']
            if edit['edit'] == 'watermark':
                watermark_count = len(watermark_paths) + 1
                watermark_name = 'watermark_' + str(watermark_count)
                if watermark_name not in request.data:
                    return Response(watermark_name + " file is required for this request.",
                                    status=status.HTTP_400_BAD_REQUEST)

                watermark = getPhoto({'file': request.data[watermark_name]})
                if not watermark[0]:
                    return Response(watermark[1].errors, status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
                watermark_path = watermark[1]
                # Save watermark path for later deletion in destructor
                watermark_paths.append(watermark_path)

                edit_list.append(Watermark(Image.open(watermark_path), param['x'], param['y'],
                                           param['width'], param['height'], param['transparency']))

            elif edit['edit'] == 'resize':
                edit_list.append(Resize(param['width'], param['height']))
            elif edit['edit'] == 'reformat':
                edit_list.append(Reformat(param['format'], path))

        photo = src.photo.Photo(path, edit_list)
        try:
            photo.execute_all_edits()
        except Exception as e:
            return Response("error with execution edits",
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        path = photo.photo_path
        photo.save(path)

        for photo in watermark_paths:
            os.remove(photo)
        watermark_paths.clear()

        processed = ProcessedPhoto(photo_path=path, photo=photo_id)
        processed.save()

        return FileResponse(open(path, 'rb'), filename=str(processed.pk))

    @swagger_auto_schema(operation_summary="Create new request",
                         operation_description="For each edit are required parameters.\n"
                                               "WATERMARK - add watermark to photo."
                                               "\n____ Parameters: array of x(int), y(int),"
                                               " height(int), width(int), watermark_n(file), "
                                               "transparency(int)"
                                               "\n____ `x` represents x axe of middle of watermark"
                                               "\n____ `y` represents y axe of middle of watermark"
                                               "\n____ `height` represents height of watermark"
                                               "\n____ `width` represents width of watermark"
                                               "\n____ `watermark_<n>` represents  watermark, "
                                               "where the `n` represents count in sequence of "
                                               "watermarks files (n-th watermark file for n-th "
                                               "watermark edit)"
                                               "\n____ `transparency` represents transparency in "
                                               "percent of watermark"
                                               "\n\nRESIZE - resize photo to specific width and "
                                               "height"
                                               "\n____ Parameters: height(int), width(int)"
                                               "\n____ `height` represents height of photo"
                                               "\n____ `width` represents width of photo"
                                               "\n\nREFORMAT - reformat photo to specific format"
                                               "\n____ Parameters: format(string)"
                                               "\n____ `format` represents required format"
                                               "\n\nOBJECT DETECTION - TODO"
                                               "\n\nCOMPRESS - TODO",
                         responses={400: 'request has bad format, some of param values missing',
                                    415: 'when photo is NOT in valid format',
                                    402: 'when user doesnt have enough money',
                                    501: 'when edit is NOT implemented',
                                    404: 'user not found, price of edit not found',
                                    status.HTTP_200_OK: openapi.Schema(type=openapi.TYPE_OBJECT,
                                                                       properties={'photo': openapi.Schema(
                                                                       type=openapi.TYPE_FILE)},
                                                                       description="filneme is id of processed photo")
                                    },
                         request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                                     required=["username", "edits", "photo",
                                                               "json"],
                                                     properties=req)
                         )
    def post(self, request):
        if "json" not in request.data:
            return Response("Invalid request format. Json data not found.", 
                            status=status.HTTP_400_BAD_REQUEST)

        request_json = json.loads(request.data["json"])
        
        check_msg = self.check_req_form(request_json)
        if check_msg:
            return Response(f"Invalid request format. Reason: {check_msg}",
                            status=status.HTTP_400_BAD_REQUEST)

        if 'photo' not in request.data:
            return Response("A photo is required for processing", status=status.HTTP_400_BAD_REQUEST)

        result = getPhoto({'file': request.data['photo']})
        if not result[0]:
            return Response(result[1].errors, status=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE)
        path = result[1]
        photo_id = result[2]

        user = self.find_by_username(request_json['username'])
        if not user:
            return Response("invalid username", status=status.HTTP_404_NOT_FOUND)

        for edit in request_json["edits"]:
            if edit['edit'] not in self.implemented_edits:
                if edit['edit'] in self.all_edits:
                    return Response("Unsupported edit. Edit is not implemented",
                                    status=status.HTTP_501_NOT_IMPLEMENTED)
                else:
                    return Response("Unsupported edit. Edit does not exist",
                                    status=status.HTTP_404_NOT_FOUND)

        total_price = 0
        for edit in request_json["edits"]:
            price_obj = self.find_by_edit(edit['edit'])
            if not price_obj:
                return Response("unsupported edit price", status=status.HTTP_404_NOT_FOUND)
            total_price += price_obj.price

        if not self.check_user_money(user, total_price):
            return Response("not enough money on Account", status=status.HTTP_402_PAYMENT_REQUIRED)

        return self.make_operation(request_json, request, path, user, total_price, photo_id)

    def find_by_photo_id(self, _id):
        try:
            return ProcessedPhoto.objects.get(pk=_id)
        except ProcessedPhoto.DoesNotExist:
            return None

    @swagger_auto_schema(operation_summary="Return processed photo",
                         responses={400: 'request has bad format, id missing',
                                    404: 'photo not found',
                                    status.HTTP_200_OK: openapi.Schema(type=openapi.TYPE_OBJECT,
                                                                       properties={'photo': openapi.Schema(
                                                                       type=openapi.TYPE_FILE)},
                                                                    )
                                    },
                         manual_parameters=[doc_parm_id("new_request")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id and _id.isnumeric():
            photo = self.find_by_photo_id(_id)
            if not photo:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
            return FileResponse(open(photo.photo_path, 'rb'))
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
