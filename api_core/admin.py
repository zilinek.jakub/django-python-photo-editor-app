# author Abi
from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Account)
admin.site.register(Role)
admin.site.register(User)
admin.site.register(Token)
admin.site.register(BannedEdit)
admin.site.register(FlatRateType)
admin.site.register(FlatRate)
admin.site.register(Photo)
admin.site.register(Request)
admin.site.register(Output)
admin.site.register(OriginalPhoto)
admin.site.register(ProcessedPhoto)
admin.site.register(Metadata)
admin.site.register(MetadataPart)
admin.site.register(EditConfiguration)
admin.site.register(SemiprocessedPhoto)
admin.site.register(ParameterType)
admin.site.register(Edit)
admin.site.register(Price)
admin.site.register(DefinedParameter)
admin.site.register(ParameterValue)
