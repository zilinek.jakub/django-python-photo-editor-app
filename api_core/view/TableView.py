import json

from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView
from ..serializers import *

from django.db import connection


class TableView(APIView):
    _sort = False
    _order = "DESC"
    _search = ""
    _name = ""
    query_text = "SELECT  rq.fkeditconf_id id , min(rq.datetime) date, " \
                 "count(rq.id) Number_of_photos, sum(rq.price) Price, foo.Ename  Editations FROM  \
                api_core_request rq join \
                (SELECT DISTINCT ec.id conf, edit.name Ename FROM api_core_editconfiguration ec \
                 JOIN api_core_editconfiguration_parameter_values ecep ON ec.id = ecep.editconfiguration_id \
                 JOIN api_core_parametervalue pv ON ecep.parametervalue_id = pv.id \
                 JOIN api_core_edit edit ON pv.fk_edit_id = edit.name \
                 JOIN api_core_definedparameter df ON df.name = pv.fk_defined_parameter_id \
                 JOIN api_core_price price ON edit.name = price.edit_id) foo \
                 on rq.fkeditconf_id = foo.conf \
                 WHERE rq.fkuser_id=<id>\
                 GROUP BY rq.fkeditconf_id, foo.Ename"

    def find_by_username(self, _user):
        try:
            return User.objects.get(username=_user)
        except User.DoesNotExist:
            return []

    def find_by_fk_user(self, _fk):
        request = Request.objects.all()
        res = []
        for i in request:
            tmp = i.fk_UserCustom_id
            if tmp == int(_fk):
                res.append(i)
        return res

    def check_format(self, data):
        if "page" in data and "numOfItemsOnPage" in data and "username" in data:
            if "search" in data:
                _search = data["search"]
            if "sortBy" in data:
                _sort = True
            if "order" in data and (data["order"] == "ASC" or data["order"] == "DESC"):
                _order = data["order"]
            return True
        return False

    def dictfetchall(self, cursor) :
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    def QuarryTable(self):
        with connection.cursor() as cursor :
            cursor.execute(self.query_text)
            data = self.dictfetchall(cursor)
        stringified_data = json.dumps(data, indent=4, sort_keys=True, default=str)
        return HttpResponse(stringified_data, content_type="application/json")

    req = {'page': openapi.Schema(type=openapi.TYPE_NUMBER, description="number of page"),
                'numOfItemsOnPage': openapi.Schema(type=openapi.TYPE_NUMBER, description="number of items on one page"),
                'username': openapi.Schema(type=openapi.TYPE_STRING),
                'search': openapi.Schema(type=openapi.TYPE_STRING, description="something we wont search"),
                'sortBy': openapi.Schema(type=openapi.TYPE_STRING, description="name of column"),
                'order': openapi.Schema(type=openapi.TYPE_STRING, description="DESC or ASC"),
                 }

    responce = {'id': openapi.Schema(type=openapi.TYPE_NUMBER),
                'date': openapi.Schema(type=openapi.TYPE_STRING),
                'numberOfPhotos': openapi.Schema(type=openapi.TYPE_NUMBER),
                'price': openapi.Schema(type=openapi.TYPE_NUMBER),
                'editation': openapi.Schema(type=openapi.TYPE_STRING)
                }

    @swagger_auto_schema(operation_summary="Get table of request for User",
                         responses={400: ' request has bad format',
                                    status.HTTP_200_OK: openapi.Schema(type=openapi.TYPE_OBJECT,
                                                     properties=responce)},
                         request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                                     required=["username", "numOfItemsOnPage", "page"],
                                                     properties=req)
                         )
    def post(self, request):
        dat = request.data
        if not self.check_format(dat):
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

        user = self.find_by_username(dat['username'])
        if not user:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        self.query_text = self.query_text.replace('<id>', str(user.id))
        return self.QuarryTable()
