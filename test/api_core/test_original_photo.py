import os
from typing import Any
from unittest import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_photo import PhotoTestData
from test.api_core.utility import create_id_url, delete_generated_test_data, format_assert_code_mismatch_help, \
    json_similar


class OriginalPhotoCoreFuncSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        photo_test_data = PhotoTestData()
        for _ in range(0, 2):
            # Seek happens inside the function
            photo_data = photo_test_data.get_test_data("post", status.HTTP_201_CREATED)
            self.create_dependency_data(reverse('photo'), photo_data, form="multipart")

    def get_url(self) -> str:
        return reverse('original')

    def get_test_data(self) -> TestData:
        return OriginalPhotoTestData()

    @skip("Not used in view")
    def test_core_functionality_single_post_ok(self):
        pass

    def test_core_functionality_get_ok(self):
        response = self.client.get(self._url)
        self._assert_with_help(None, response, status.HTTP_200_OK)

        photo_path_field = OriginalPhotoTestData.PHOTO_PATH
        photo_pk_field = OriginalPhotoTestData.get_pk_field_name()
        for i, ent in enumerate(response.data):
            self.assertTrue(OriginalPhotoTestData.PHOTO_PATH in ent,
                            "There is no photo_path in the response data")
            self.assertTrue(os.path.isfile(ent[photo_path_field]),
                            "The response path does not point to a file")

            response_2 = self.client.get(create_id_url(self._url,
                                                       ent[photo_pk_field],
                                                       photo_pk_field))
            self._assert_with_help(None, response_2, status.HTTP_200_OK)
            self.assertTrue(json_similar(ent, response_2.data, True),
                            "Entity received from get at primary key resulted in a different entity"
                            " than received from bulk get.\n"
                            "Entity from get: {}\nEntity from get?id={}:{}"
                            .format(ent, ent[photo_pk_field], response_2.data))

        response = self.client.get(self._url+"?Pumpstcauksa pupepen")
        self._assert_with_help(None, response, status.HTTP_200_OK)

    def _assert_with_help(self, pk_used, response, expected_code):
        self.assertEqual(response.status_code, expected_code,
                         format_assert_code_mismatch_help({"primary_key": pk_used},
                                                          response.status_code,
                                                          expected_code,
                                                          response.content))

    @skip("Not used in view")
    def test_core_functionality_post_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_post_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_put_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_put_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_fail(self):
        pass

    @classmethod
    def tearDownClass(cls):
        delete_generated_test_data()


class OriginalPhotoTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        return None

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return ""

    @classmethod
    def get_pk_field_name(cls) -> str:
        return cls.PHOTO_PK

    PHOTO_PATH = "photo_path"
    PHOTO_PK = "photo"
