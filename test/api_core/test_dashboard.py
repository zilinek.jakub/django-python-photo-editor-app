from typing import Any
from unittest import skip

from test.api_core.generalized_test import GeneralizedTestSuite, TestData


@skip("View not finished yet")
class DashboardCoreTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        pass

    def get_test_data(self) -> TestData:
        pass


class DashboardTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        pass

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "DFGHJKLJNHBGZU"
