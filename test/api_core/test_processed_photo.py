from typing import Any
from unittest import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_photo import PhotoTestData


class ProcessedPhotoCoreTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        photoTestData = PhotoTestData()
        self.create_dependency_data(reverse("photo"), photoTestData.get_test_data("post", status.HTTP_201_CREATED),
                                    form="multipart")

    def get_url(self) -> str:
        return reverse("processed")

    def get_test_data(self) -> TestData:
        return ProcessedPhotoTestData()

    # Post methods do not make sense as long as we are not thinking of processing
    # the photo editing on a different server which would then pass the info back via this
    # method, but that would still require sending the entire photo, not just a path
    @skip("This view method is nonsencical at this moment")
    def test_core_functionality_post_ok(self):
        super().test_core_functionality_post_ok()

    @skip("This view method is nonsensical at this moment")
    def test_core_functionality_post_fail(self):
        super().test_core_functionality_post_fail()

    @skip("This view method is nonsensical at this moment")
    def test_core_functionality_single_post_ok(self):
        super().test_core_functionality_single_post_ok()


class ProcessedPhotoTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400_PUT,
            ("put", status.HTTP_404_NOT_FOUND): self.INVALID_DATA_404_PUT
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def get_pk_field_name(cls) -> str:
        return "photo"

    IRRELEVANT_KEY = "sadfjkhsadfkjh"

    VALID_DATA = [
        {
            "photo": 1,
            "photo_path": "./income_photos/nonsense_data1,png"
        }, {
            "photo": 2,
            "photo_path": "./income_photos/nonsense_data2,png"
        }
    ]

    VALID_DATA_PUT = [
        {
            "photo": 1,
            "photo_path": "./income_photos/nonsense_data4,png"
        }, {
            "photo": 2,
            "photo_path": "./income_photos/nonsense_data3,png",
            IRRELEVANT_KEY: "nonsense"
        }
    ]

    INVALID_DATA_400_PUT = [
        {
            "photo": 1
        }, {}, {
            "photo_path": "test"
        }, {
            "photo": "nope",
            "photo_path": "test"
        }
    ]

    INVALID_DATA_404_PUT = [
        {
            "photo": 50,
            "photo_path": "test"
        }
    ]

