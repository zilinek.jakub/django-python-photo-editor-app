import traceback

from assets import _editations
from api_core.models import *


def edit_insert():
    _created = 0
    for edit in _editations:
        try:
            if not Edit.objects.filter(name=edit).exists():
                _edit = Edit(name=edit)
                _edit.save()
                _created += 1
        except Exception as e:
            print("Edit insert failed!")
            print(traceback.format_exc())
            return False
    print("\n\nEdit insert success.\t\t Created ", _created)
    return True
