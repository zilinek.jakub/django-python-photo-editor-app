from typing import Any
from unittest.case import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.utility import create_id_url, delete_generated_test_data
from test.utility import get_path


class PhotoCoreTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse("photo")

    def get_test_data(self) -> TestData:
        return PhotoTestData()

    def create_post_data(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_201_CREATED)

        if data_to_create is None:
            return

        self.method_test_data_src(self.client.post, data_to_create, status.HTTP_201_CREATED,
                                  self.__correct_data_cmp,
                                  None,
                                  "multipart")

    def test_core_functionality_single_post_ok(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_201_CREATED)[0]
        self.method_test_single(self.client.post, data_to_create,
                                status.HTTP_201_CREATED,
                                lambda ent, resp:
                                self.std_check(PhotoResponseTestData.CORRECT_RESPONSE_DATA[0],
                                               resp),
                                None,
                                "multipart")

        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(PhotoResponseTestData.CORRECT_RESPONSE_DATA[0],
                                               resp[0]))

        id_field_name = self._test_data.get_pk_field_name()
        id_value = self._test_data.get_pk_from_data(PhotoResponseTestData.CORRECT_RESPONSE_DATA[0])
        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(PhotoResponseTestData.CORRECT_RESPONSE_DATA[0],
                                               resp),
                                create_id_url(self._url, 1 if id_value is None else id_value,
                                              id_field_name))

    def __correct_data_cmp(self, _, resp, index):
        return self._test_data.get_universal_json_compare_function()(
            PhotoResponseTestData.CORRECT_RESPONSE_DATA[index], resp, index
        )

    def test_core_functionality_post_ok(self):
        for code in range(200, 300):
            data = self._test_data.get_test_data("post", code)
            if data is None:
                continue

            self.method_test_data_src(self.client.post,
                                      data,
                                      code,
                                      self.__correct_data_cmp,
                                      None,
                                      "multipart")

    def test_core_functionality_post_fail(self):
        self.create_post_data()
        for code in range(400, 600):
            data = self._test_data.get_test_data("post", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.post, data, code)

        for code in range(400, 600):
            data = self._test_data.get_test_data("post-extra", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.post, data, code, None, None, "multipart")

    def test_core_functionality_get_ok(self):
        self._shared_suite.test_core_functionality_get_ok(self._test_data.get_irrelevant_key(),
                                                          PhotoResponseTestData.CORRECT_RESPONSE_DATA,
                                                          self.create_post_data,
                                                          self._test_data.get_pk_field_name(),
                                                          self._test_data.detect_fields)

    def test_core_functionality_get_fail(self):
        self._shared_suite.test_core_functionality_get_fail(self._test_data.get_pk_field_name(),
                                                            create_data=self.create_post_data)

    def _get_url_func(self, _, ent):
        return create_id_url(self._url, ent[self._test_data.get_pk_field_name()],
                             self._test_data.get_pk_field_name())

    def put_ok_replace_present(self):
        self.create_post_data()

        data = self._test_data.get_test_data("put", status.HTTP_200_OK)
        self.method_test_data_src(self.client.put, data, status.HTTP_200_OK,
                                  self.__correct_data_cmp,
                                  self._get_url_func, "multipart")

    @skip("This test is nonsencical until the view gets modified")
    def test_core_functionality_put_ok(self):
        # Put to this url should allow to change the photo otherwise it does not really make sense to have a put here
        super().test_core_functionality_put_ok()

    @skip("This test is nonsencical until the view gets modified")
    def test_core_functionality_put_fail(self):
        self.put_fail_replace_present()

    def test_core_functionality_delete_ok(self):
        self.create_post_data()

        id_field_name = self._test_data.get_pk_field_name()
        self.method_test_data_src(self.client.delete, PhotoResponseTestData.CORRECT_RESPONSE_DATA,
                                  status.HTTP_204_NO_CONTENT,
                                  url_get=lambda index, ent:
                                  create_id_url(self._url, ent[id_field_name]
                                                if id_field_name in ent
                                                else (index % len(PhotoResponseTestData.CORRECT_RESPONSE_DATA)) + 1,
                                                id_field_name))
        self.check_cascades()

    def test_core_functionality_delete_fail(self):
        self._shared_suite.test_core_functionality_delete_fail(self._test_data.get_pk_field_name(),
                                                               first_nf_id=1, incorrect_id=-11)

    def tearDown(self):
        delete_generated_test_data()


class PhotoTestData(TestData):
    @classmethod
    def __seek_begin(cls, data):
        for entry in data:
            if "photo" not in entry or not isinstance(entry, dict):
                continue

            entry["photo"].seek(0, 0)

    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.__VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post-extra", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_EXTRA_400,
            ("put", status.HTTP_200_OK): self.__VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
        }

        if (method, status_code) not in data:
            return None

        result = data[(method, status_code)]
        self.__seek_begin(result)

        return result

    # @classmethod
    # def close_streams(cls):
    #     for entry in cls.__VALID_DATA:
    #         entry["photo"].close()

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "ooga booga"

    __VALID_DATA = [
        {
            "photo": (open(get_path("../test_photos/legend.png", __file__), "rb"))
        }, {
            "photo": (open(get_path("../test_photos/python_logo.png", __file__), "rb"))
        },
    ]

    INVALID_DATA_400 = [
        {
            "pho"
        }, "", {}, {
            IRRELEVANT_KEY: "none"
        }
    ]

    INVALID_DATA_EXTRA_400 = [
        {
            "photo": (open(get_path("../../README.md", __file__), "rb"))
        }
    ]

    __VALID_DATA_PUT = [
        {
            "id": 1,
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb"))
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "photo"
        }, {}, {
            IRRELEVANT_KEY: "none"
        }, {
            "id": 1
        }
    ]


class PhotoResponseTestData:
    CORRECT_RESPONSE_DATA = [
        {
            "id": 1
        }, {
            "id": 2
        },
    ]
