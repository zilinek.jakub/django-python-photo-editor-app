import abc


class SaveStorageAdapter(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def save_image(self, path, image):
        pass

    @abc.abstractmethod
    def save_file(self, path, file):
        pass

    @abc.abstractmethod
    def get_image(self, path):
        pass

    @abc.abstractmethod
    def delete_file(self, path):
        pass
