from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from api_core.views import *


class Price_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=PriceSerializer(many=True))}
    res_quary = {404: 'object for this slug wasn\'t found',
                  400: '?id in quary wasn\'t specified or request has bad format',
                  200: openapi.Response(description="response",
                                        schema=PriceSerializer())}
    single = openapi.Response(description="response",
                              schema=PriceSerializer())

    def find_by_id(self, _id):
        try:
            return Price.objects.get(pk=_id)
        except Price.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Price"),
                         operation_description=doc_desc_get("Price"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("Price")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, PriceSerializer)
        price = Price.objects.all()
        serializer = PriceSerializer(price, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Price object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=PriceSerializer)
    def post(self, request):
        serializer = PriceSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Price",
                         operation_description=doc_desc_put("Price"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('Price')],
                         request_body=PriceSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, PriceSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Price",
                         operation_description=doc_desc_delete("Price"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('Price')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
