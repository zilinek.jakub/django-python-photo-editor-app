# inherits from Edit, does object detection for photo, returns json of found objects

"""
    @file watermark.py
    @author Olivie A. Franklova
    @date 22.4.2021
"""

import numpy as np
from cv2 import cv2

from .edit import Edit

"""
@brief This class represents object detection for photo.

"""


class ObjectDetection(Edit):
    def __init__(self):
        print()

    def extract_boxes_confidences_classids(self, outputs, confidence, width, height):
        boxes = []
        confidences = []
        class_ids = []

        for output in outputs:
            for detection in output:
                # Extract the scores, class_id, and the confidence of the prediction
                scores = detection[5:]
                class_id = np.argmax(scores)
                conf = scores[class_id]

                # Consider only the predictions that are above the confidence threshold
                if conf > confidence:
                    # Scale the bounding box back to the size of the image
                    box = detection[0:4] * np.array([width, height, width, height])
                    center_x, center_y, w, h = box.astype('int')

                    # Use the center coordinates, width and height
                    # to get the coordinates of the top left corner
                    x = int(center_x - (w / 2))
                    y = int(center_y - (h / 2))

                    boxes.append([x, y, int(w), int(h)])
                    confidences.append(float(conf))
                    class_ids.append(class_id)

        return boxes, confidences, class_ids

    def make_prediction(self, net, layer_names, labels, image, confidence, threshold):
        height, width = image.shape[:2]

        # Create a blob and pass it through the model
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        net.setInput(blob)
        outputs = net.forward(layer_names)

        # Extract bounding boxes, confidences and class_ids
        boxes, confidences, class_ids = self.extract_boxes_confidences_classids(outputs, confidence,
                                                                                width, height)

        # Apply Non-Max Suppression
        idxs = cv2.dnn.NMSBoxes(boxes, confidences, confidence, threshold)

        return boxes, confidences, class_ids, idxs

    def draw_bounding_boxes(self, image, boxes, confidences, class_ids, idxs, colors, labels):
        if len(idxs) > 0:
            for i in idxs.flatten():
                # extract bounding box coordinates
                x, y = boxes[i][0], boxes[i][1]
                w, h = boxes[i][2], boxes[i][3]

                # draw the bounding box and label on the image
                color = [int(c) for c in colors[class_ids[i]]]
                cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
                text = "{}: {:.4f}".format(labels[class_ids[i]], confidences[i])
                cv2.putText(image, text, (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

        return image

    def execute(self, photo_path):
        class_file = "/app/mediagraphix/src/edit/object_detection_data/coco.names"
        config = "/app/mediagraphix/src/edit/object_detection_data/yolov3.cfg"
        weights = "/app/mediagraphix/src/edit/object_detection_data/yolov3.weights"
        labels = open(class_file, encoding='utf-8').read().rstrip().rsplit('\n')
        net = cv2.dnn.readNetFromDarknet(config, weights)
        layer_names = net.getLayerNames()
        layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')

        image = cv2.imread(photo_path)

        boxes, confidences, class_ids, idxs = self.make_prediction(net, layer_names, class_file,
                                                                   image, 0.5, 0.3)
        image = self.draw_bounding_boxes(image, boxes, confidences, class_ids, idxs, colors, labels)
        cv2.imwrite("/app/mediagraphix/src/edit/object_detection_data/image_object_detection.png",
                    image)
        # with open(class_file, 'rt') as f:
        #     class_names = f.read().rstrip('\n').rsplit('\n')


print("Object_detection STARTED\n")
obj = ObjectDetection()
obj.execute("/app/mediagraphix/src/test_photos/legend.png")
