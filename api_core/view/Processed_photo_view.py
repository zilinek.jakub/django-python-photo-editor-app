from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from api_core.views import *


class Processed_photo_view(APIView):
    res_many = {404: 'object for this slug wasn\'t found',
                400: 'id is not numeric or request has bad format',
                200: openapi.Response(
                    description="response",
                    schema=ProcessedPhotoSerializer(many=True))}
    single = openapi.Response(description="response",
                              schema=ProcessedPhotoSerializer())

    @swagger_auto_schema(operation_summary=doc_sum_get("Processed photo"),
                         operation_description=doc_desc_get("Processed photo"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("processed_photo")])
    def get(self, request):
        _pk = request.GET.get('photo', '')
        if _pk != '':
            return get_by_pk(_pk, ProcessedPhoto, ProcessedPhotoSerializer)
        processed_photo = ProcessedPhoto.objects.all()
        serializer = ProcessedPhotoSerializer(processed_photo, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Processed photo object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=ProcessedPhotoSerializer)
    def post(self, request):
        serializer = ProcessedPhotoSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Processed photo",
                         operation_description=doc_desc_put("Processed photo"),
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric or id is NOT present',
                                    status.HTTP_200_OK: openapi.Response(description="response",
                              schema=ProcessedPhotoSerializer(many=False))},
                         request_body=SemiprocessedPhotoSerializer,
                         manual_parameters=[doc_parm_id('processed_photo')])
    def put(self, request):
        _pk = request.GET.get('photo', '')
        if _pk != '':
            return put_by_pk(_pk, ProcessedPhoto, ProcessedPhotoSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Processed photo",
                         operation_description=doc_desc_delete("Processed photo"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id("processed_photo")])
    def delete(self, request):
        _pk = request.GET.get('photo', '')
        if _pk != '':
            return delete_by_pk(_pk, ProcessedPhoto)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

