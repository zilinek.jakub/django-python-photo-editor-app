from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import TestData, GeneralizedTestSuite
from test.api_core.test_edit import EditTestData


class BannedEditCoreFuncSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse('ban_edit')

    def get_test_data(self) -> TestData:
        return BannedEditTestData()


class BannedEditTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "irrelevant_field"

    VALID_DATA = [
        {
            "edit_name": "Reformat",
        }, {
            "edit_name": "Watermark",
        }, {
            "edit_name": "Resize",
        }, {
            "edit_name": "Invert",
        }, {
            IRRELEVANT_KEY: 15,
            "edit_name": "AllIn1"
        }, {
            "edit_name": "CrazyEdit"
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "edit_name": "CrazyEdit"
        }, {
            "id": 2,
            "edit_name": "WatermarkExtra",
            IRRELEVANT_KEY: "something"
        }
    ]
    VALID_DATA_PUT_NEW = [
        {
            "id": 6,
            "edit_name": "TheGoodOne"
        }, {
            "id": 7,
            "edit_name": "WatermarkExtra",
            IRRELEVANT_KEY: "something"
        }
    ]
    OBJ_VALID_PUT_OK = {
        "id": 6,
        "edit_name": "TheGoodOne"
    }

    INVALID_DATA_PUT_400 = [
        {
            "id": "ooops this is not an id",
            "edit_name": "TheGoodOne"
        }, {
            "expiration": "2022-02-22T15:00:00Z"
        }, {}]
    INVALID_DATA_PUT_422 = [
        {
            "edit_name": "NonExistent"
        }, {
            "id": "5",
            "edit_name": "NonExistent"
        }]

    INVALID_DATA_400 = [
        {
            "credit": "how about some of them apples"
        }, {}, {
            "name": "Reformat",
        }
    ]
    INVALID_DATA_422 = [
        {
            "edit_name": 3000,
        }, {
            "edit_name": "Mythical edit",
        }
    ]
