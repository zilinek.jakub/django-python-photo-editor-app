import inspect
import os
from datetime import datetime
from pathlib import Path
from typing import Callable, Any, Iterable, Optional

from test.utility import get_path

ISO8601_STR = "%Y-%m-%dT%H:%M:%S%z"


def date_str_eq(val1, val2):
    return datetime.strptime(val1, ISO8601_STR) == datetime.strptime(val2, ISO8601_STR)


def int_rep(val1, val2):
    return int(val1) == int(val2)


def create_id_url(url: str, req_id: Any, id_field_name: str = "id") -> str:
    return url+"?{}={}".format(id_field_name, req_id)


def check_as_set(val1: Iterable[Any], val2: Iterable[Any]):
    return set(val1) == set(val2)


def eq(_: str, val1: Any, val2: Any) -> bool:
    return val1 == val2


def std_check(ent, resp, cmp=eq):
    return json_similar(ent, resp, True, cmp)


def json_similar_ignore_compare(irrelevant_key: str,
                                cmp: Callable[[str, Any, Any], bool] = eq) -> Callable[[Any, Any,
                                                                                        int], bool]:
    def json_similar_ignore(ent, resp, _):
        return json_similar(ent, resp, irrelevant_key not in ent, cmp)
    return json_similar_ignore


def json_similar(expected_data: dict, response_data: dict, strict: bool,
                 compare_func: Callable[[str, Any, Any], bool] =
                 lambda s, v1, v2: v1 == v2) -> bool:
    """
    Checks if all fields in expected_data match corresponding fields in
    response_data

    :param expected_data: json data which we expect to find
    :param response_data: json data we received
    :param strict: is the method supposed to return false when a field is not
    present in the response data (ie. testing whether an useless argument
    is correctly processed)
    :param compare_func: a function to use for comparison base on a string key
    """
    for key in expected_data:
        if key not in response_data:
            if strict:
                return False
            continue

        if not compare_func(key, expected_data[key], response_data[key]):
            return False
    return True


def format_assert_code_mismatch_help(ent: Any,
                                     response_code: int,
                                     expected_code: int,
                                     response_data: Any = None):
    return "\nResponse codes do not match for entity: {}\n" \
           "Expected code {}, got {} instead.\n" \
           "Body of the response looks as follows:\n {}".format(ent,
                                                                expected_code,
                                                                response_code,
                                                                response_data
                                                                if response_data is not None
                                                                else "-")


def format_assert_fail_help(ent: Any, response_data: Any, index: Optional[int],
                            cmp_function_used: Callable):
    return "Response validation failed on additional check with function:\n" \
           "{}\n" \
           "Defined at: {}.\n" \
           "{}\n" \
           "The data it received:\n" \
           "{}\n" \
           "Index passed: {}".format(inspect.getsource(cmp_function_used),
                                     inspect.getfile(cmp_function_used),
                                     "Input data the function expected:\n{}".format(ent)
                                     if ent is not None
                                     else "Unavailable - expected entity for this function is "
                                          "dictated by the function itself.",
                                     response_data,
                                     index if index is not None else "-")


def delete_generated_test_data():
    income_photos_path = get_path("../../income_photos/", __file__)
    for filename in os.listdir(income_photos_path):
        if filename.endswith(".py"):
            continue

        try:
            path = Path(income_photos_path).joinpath(filename).absolute()
            if os.path.isdir(path):
                continue
            os.remove(path)
        except PermissionError:
            print("A file removal failed, this indicates that perhaps a file handle remains active")
