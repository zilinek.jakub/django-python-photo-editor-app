from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import TestData, GeneralizedTestSuite
from test.api_core.test_defined_parameter import DefinedParameterTestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_parameter_type import ParameterTypeTestData
from test.api_core.test_parameter_value import ParameterValueTestData
from test.api_core.utility import check_as_set, json_similar_ignore_compare


class EditConfigurationCoreFuncTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)
        self.create_dependency_data(reverse('type_param'), ParameterTypeTestData.VALID_DATA)
        self.create_dependency_data(reverse('def_param'), DefinedParameterTestData.VALID_DATA)
        self.create_dependency_data(reverse('values'), ParameterValueTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse('edit_conf')

    def get_test_data(self) -> TestData:
        return EditConfigurationTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("values"), 1,
                                              ParameterValueTestData.get_pk_field_name(),
                                              [EditConfigurationTestData.VALID_DATA[0],
                                               EditConfigurationTestData.VALID_DATA[3],
                                               EditConfigurationTestData.VALID_DATA[4],
                                               EditConfigurationTestData.VALID_DATA[5],
                                               EditConfigurationTestData.VALID_DATA[6]],
                                              self.method_test_single)


class EditConfigurationTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any):
        d = {"parameter_values": check_as_set}
        if name not in d:
            return val1 == val2
        return d[name](val1, val2)

    IRRELEVANT_KEY = "irrelevant_field"
    VALID_DATA = [
        {
            "parameter_values": [1, 2]
        }, {
            "parameter_values": [3, 4]
        }, {
            "parameter_values": [1, 2, 3, 4]
        }, {
            "parameter_values": [1, 3, 4]
        }, {
            "parameter_values": [5, 1, 4]
        }, {
            "parameter_values": [1, 2, 3, 4, 5, 6, 7]
        }, {
            "parameter_values": [7]
        }, {
            "parameter_values": [4, 2, 6]
        }]
    VALID_DATA_PUT = [
        {
            "id": 3,
            "parameter_values": [3, 4, 7]
        }, {
            "id": 1,
            "parameter_values": [1, 2, 3, 4, 7],
            IRRELEVANT_KEY: "bflmpsvz"
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": 1,
            "fk_Edit": 3,
            "fk_Param_type": 1
        }, {
            "id": 2,
            "parameter_values": "1,2,3"
        }, {
            "name": "width",
            "fk_Param_type": 1
        }, {
            "id": 3,
            "parameter_values": 1
        }, "", {}]
    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "parameter_values": []
        }, {
            "id": 2,
            "parameter_values": [8, 9, 11]
        }]

    INVALID_DATA_400 = [
        {
            "name": "width",
            "fk_Param_type": 1
        }, {
            "parameter_values": "1,2,3"
        }, "", {}]
    INVALID_DATA_422 = [
        {
            "parameter_values": [8, 11]
        }, {
            "parameter_values": []
        }]
