from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_defined_parameter import DefinedParameterTestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_parameter_type import ParameterTypeTestData


class ParameterValueTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)
        self.create_dependency_data(reverse('type_param'), ParameterTypeTestData.VALID_DATA)
        self.create_dependency_data(reverse('def_param'), DefinedParameterTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse("values")

    def get_test_data(self) -> TestData:
        return ParameterValueTestData()


class ParameterValueTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            # ("put", status.HTTP_201_CREATED): self.VALID_DATA_PUT_NEW,
            # ("put_obj", status.HTTP_200_OK): self.OBJ_VALID_PUT_OK,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "asfkljsalf;kj"

    VALID_DATA = [
        {
            "value": "1080",
            "fk_defined_parameter": "width",
        }, {
            "value": "4096",
            "fk_defined_parameter": "height",
        }, {
            "value": "2048",
            "fk_defined_parameter": "width",

        }, {
            "value": "Consectetur adipiscing elit",
            "fk_defined_parameter": "description"
        }, {
            "value": "png",
            "fk_defined_parameter": "format"
        }, {
            "value": "1.18",
            "fk_defined_parameter": "size_mp"
        }, {
            "value": "true",
            "fk_defined_parameter": "make_super_awesome"
        }, {
            "value": "toto jsou testovací data",
            "fk_defined_parameter": "description"
        }, {
            "value": "další testovací data",
            "fk_defined_parameter": "description",
            IRRELEVANT_KEY: "toto by tu nemělo být"
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "value": "jiná testovací data",
            "fk_defined_parameter": "width"
        }, {
            "id": 2,
            "value": "další jiná testovací data",
            "fk_defined_parameter": "width",
            IRRELEVANT_KEY: "don't mind me here"
        }
    ]

    VALID_DATA_PUT_NEW = [
        {
            "id": 5,
            "value": "nová testovací data",
            "fk_defined_parameter": "height",
            IRRELEVANT_KEY: "I'm not here"
        }
    ]

    INVALID_DATA_400 = [
        {}, {
            "value": "test"
        }
    ]

    INVALID_DATA_422 = [
        {
            "value": "test",
            "fk_defined_parameter": "100"
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": "wait, this is not int",
            "value": "this won't work",
            "fk_defined_parameter": "width",
            IRRELEVANT_KEY: "test"
        }, {}, {
            "id": "",
            "value": "test",
            "fk_defined_parameter": "height"
        }
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "value": "test",
            "fk_defined_parameter": "100"
        }
    ]
