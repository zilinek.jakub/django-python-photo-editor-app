from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView


class Banned_edit_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=BannedEditSerializer(many=True))}
    single = openapi.Response(description="response",
                              schema=BannedEditSerializer())
    res_quary = {404: 'object for this slug wasn\'t found',
                 400: 'id in quary wasn\'t specified or request has bad format',
                 422: 'Edit does not exist',
                 200: openapi.Response(description="response",
                                       schema=BannedEditSerializer())}

    def find_by_id(self, _id):
        try:
            return BannedEdit.objects.get(pk=_id)
        except BannedEdit.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("BannedEdit"),
                         operation_description=doc_desc_get("BannedEdit"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("banned_edit")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, BannedEditSerializer)
        _all = BannedEdit.objects.all()
        serializer = BannedEditSerializer(_all, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Update BannedEdit",
                         operation_description=doc_desc_put("BannedEdit"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('banned_edit')],
                         request_body=BannedEditSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            edits = Edit.objects.all()
            edits_name = []
            for edit in edits:
                edits_name.append(edit.name)
            serializer = BannedEditSerializer(data=request.data)
            if serializer.is_valid():
                if serializer.validated_data.get('edit_name') in edits_name:
                    return put_by_id(_id, self.find_by_id, BannedEditSerializer, request)
                else:
                    return Response("Invalid banned_edit name, edit that is banned "
                                    "must exist.\n\n Existing edits: \n\n" +
                                    edits_name.__str__(), status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Add new BannedEdit object",
                         responses={400: 'request has bad format',
                                    422: 'Edit does not exist',
                                    status.HTTP_201_CREATED: single},
                         request_body=BannedEditSerializer)
    def post(self, request):
        serializer = BannedEditSerializer(data=request.data)
        # gets name of existing editations
        # this is an odd way of doing this
        edits = Edit.objects.all()
        edits_name = []
        for edit in edits:
            edits_name.append(edit.name)

        if serializer.is_valid():
            # checks if the need banned editation is valid editation from list edits
            if serializer.validated_data.get('edit_name') in edits_name:
                serializer.save()
            else:
                return Response("Invalid banned_edit name, edit that is banned "
                                "must exist.\n\n Existing edits: \n\n" +
                                edits_name.__str__(), status=status.HTTP_422_UNPROCESSABLE_ENTITY)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete BannedEdit",
                         operation_description=doc_desc_delete("BannedEdit"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('banned_edit')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
