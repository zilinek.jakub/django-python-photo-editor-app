from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView

res_many = {404: 'object for this slug wasn\'t found',
            400: 'id is not numeric or request has bad format',
            200: openapi.Response(
                description="response",
                schema=SemiprocessedPhotoSerializer(many=True))}
single = openapi.Response(description="response",
                          schema=SemiprocessedPhotoSerializer())


class Semiprocessed_photo_view(APIView):
    def find_by_fk_photo(self, _fk):
        res = SemiprocessedPhoto.objects.filter(fk_photo__exact=_fk)
        return res

    @swagger_auto_schema(operation_summary=doc_sum_get("Semiprocessed photo"),
                         operation_description="Return single Semiprocessed photo object if `fk_photo` "
                                               "in query is present "
                                               "or return all Semiprocessed photos objects",
                         responses=res_many,
                         manual_parameters=[Parameter('fk_photo', openapi.IN_QUERY,
                                                      'show only Semmiprocessed photo with this fk_photo '
                                                      '`semiprocessed_photo?fk_photo=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def get(self, request):
        _fk = request.GET.get('fk_photo', '')
        semiprocessed_photo = SemiprocessedPhoto.objects.all()
        if _fk:
            if not _fk.isnumeric():
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
            semiprocessed_photo = self.find_by_fk_photo(_fk)
            if not semiprocessed_photo:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = SemiprocessedPhotoSerializer(semiprocessed_photo, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Semiprocessed photo object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=SemiprocessedPhotoSerializer)
    def post(self, request):
        serializer = SemiprocessedPhotoSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Delete Semmiprocesed photo",
                         operation_description="Delete Semmiprocessed photo only if `fk_photo` in query is present",
                         responses=delete_res,
                         manual_parameters=[Parameter('fk_photo', openapi.IN_QUERY,
                                                      'delete Semmiprocessed photo with this fk_photo '
                                                      '`semiprocessed_photo?fk_photo=2`',
                                                      type=openapi.TYPE_NUMBER, required=True)
                                            ])
    def delete(self, request):
        _fk = request.GET.get('fk_photo', '')
        if _fk == '' or not _fk.isnumeric():
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        semiprocessed_photo = self.find_by_fk_photo(_fk)
        if not semiprocessed_photo:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        for i in semiprocessed_photo:
            i.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class Semiprocessed_photo_details(APIView):
    def find_by_photo_path(self, _photo_path):
        try:
            return SemiprocessedPhoto.objects.get(fk_Photo=_photo_path)
        except SemiprocessedPhoto.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary="Will be deleted")
    def get(self, request, _photo_path):
        semiprocessed_photo = self.find_by_photo_path(_photo_path)
        if not semiprocessed_photo:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = SemiprocessedPhotoSerializer(semiprocessed_photo)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Will be deleted")
    def put(self, request, _photo_path):
        semiprocessed_photo = self.find_by_photo_path(_photo_path)
        if not semiprocessed_photo:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = SemiprocessedPhotoSerializer(semiprocessed_photo, data=request.data)
        return serializer_validation_put(serializer)

    @swagger_auto_schema(operation_summary="Will be deleted")
    def delete(self, request, _photo_path):
        semiprocessed_photo = self.find_by_photo_path(_photo_path)
        if not semiprocessed_photo:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        semiprocessed_photo.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
