from django.contrib.auth.models import User
from assets import _super

def create_super():
    _created = 0
    for user in _super:
        if not User.objects.filter(username=user[0]).exists():
            _created += 1
            user = User.objects.create_user(user[0], password=user[1])
            user.is_superuser = True
            user.is_staff = True
            user.save()
    print("\n\nSuper users insert success.\t\t Created ", _created)
    return True
