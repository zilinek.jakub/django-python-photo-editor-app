# @author Abi
from drf_yasg.openapi import Parameter, IN_QUERY
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from api_core.views import *


class AccountView(APIView):
    res_many = {404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=AccountSerializer(many=True)
                )}
    res_quary = {404: 'object for this slug wasn\'t found',
                  400: '?id in quary wasnt specified',
                  200: openapi.Response(description="response",
                                        schema=AccountSerializer())}

    def find_by_id(self, _id):
        try:
            return Account.objects.get(pk=_id)
        except Account.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Account"),
                         operation_description=doc_desc_get("Account"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("Account")]
                         )

    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_pk(_id, Account, AccountSerializer)
        accounts = Account.objects.all()
        serializer = AccountSerializer(accounts, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Account object",
                         request_body=AccountSerializer,
                         responses=res_quary,
                        ##manual_parameters=[doc_parm_id("Account")]
                         )
    def post(self, request):
        serializer = AccountSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Account",
                         operation_description=doc_desc_put("Account"),
                         request_body=AccountSerializer,
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('Account')])
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, AccountSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Account",
                         operation_description=doc_desc_delete("Account"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('Account')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
