from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_defined_parameter import DefinedParameterTestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_edit_configuration import EditConfigurationTestData
from test.api_core.test_parameter_type import ParameterTypeTestData
from test.api_core.test_parameter_value import ParameterValueTestData
from test.api_core.test_photo import PhotoTestData
from test.api_core.test_role import RoleTestData
from test.api_core.utility import date_str_eq, delete_generated_test_data

USER_DEPENDENCY = [
    {
        "username": "abs(legend)",
        "name": "Adam Procházka",
        "email": "adam.prochazka@profifoto.cz",
        "password": "123456789",
        "free_trial": False,
        "fk_account": None,
        "roles": [2]
    }, {
        "username": "novmar7",
        "name": "Marek Novotný",
        "email": "novmar7@photofire.com",
        "password": "251251251oo",
        "free_trial": False,
        "fk_account": None,
        "roles": [1, 3]
    }, {
        "username": "PanK",
        "name": "Petr Mok",
        "email": "mokp@photofire.com",
        "password": "weeI112-.8",
        "free_trial": False,
        "fk_account": None,
        "roles": [3]
    }
]


class RequestTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('role'), RoleTestData.VALID_DATA)

        self.create_dependency_data(reverse('user'), USER_DEPENDENCY)

        self.create_dependency_data(reverse('type_param'), ParameterTypeTestData.VALID_DATA)
        self.create_dependency_data(reverse("edit"), EditTestData.VALID_DATA)
        data = DefinedParameterTestData.VALID_DATA
        for edit in data:
            edit.pop(DefinedParameterTestData.IRRELEVANT_KEY, None)
        self.create_dependency_data(reverse('def_param'), data)
        self.create_dependency_data(reverse('values'), ParameterValueTestData.VALID_DATA)
        self.create_dependency_data(reverse("edit_conf"), EditConfigurationTestData.VALID_DATA)

        photo_test_data = PhotoTestData()
        photo_data = photo_test_data.get_test_data("post", status.HTTP_201_CREATED).copy()
        photo_data.pop()
        self.create_dependency_data(reverse("photo"), photo_data, form="multipart")

    def get_url(self) -> str:
        return reverse("request")

    def get_test_data(self) -> TestData:
        return RequestTestData()

    @classmethod
    def tearDownClass(cls):
        delete_generated_test_data()


class RequestTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ('put', status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        d = {"datetime": date_str_eq}
        if name not in d:
            return val1 == val2
        return d[name](val1, val2)

    IRRELEVANT_KEY = "ladfklsjdfkjsdfvsnajnbbuh"

    VALID_DATA = [
        {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "session_id": 2,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 200,
            "state": "done",
            "fkeditconf": 2,
            "fkuser": 2,
            "photo": 1
        }, {
            "session_id": 3,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 300,
            "state": "done",
            "fkeditconf": 1,
            "fkuser": 2,
            "photo": 1,
            IRRELEVANT_KEY: "don't mind me here"
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 150,
            "state": "done",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "id": 2,
            "session_id": 2,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 250,
            "state": "done",
            "fkeditconf": 4,
            "fkuser": 3,
            "photo": 1,
            IRRELEVANT_KEY: "i'm not here"
        }
    ]

    INVALID_DATA_400 = [
        {
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "photo": 1
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": -1,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }
    ]

    INVALID_DATA_422 = [
        {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 2
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 20,
            "photo": 1
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 20,
            "fkuser": 1,
            "photo": 1
        }
    ]

    INVALID_DATA_PUT_400 = INVALID_DATA_400
    INVALID_DATA_PUT_422 = INVALID_DATA_422
