from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class Metadata_part_view(APIView):
    res_many = {400: 'id is NOT numeric or'
                     'or both id and fk_metadata is present'
                     'or request has bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=FlatRateSerializer(many=True))}

    res_quary = {404: 'object for this slug wasn\'t found',
                 400: 'id in quary wasn\'t specified or request has bad format',
                 200: openapi.Response(description="response",
                                       schema=MetadataPartSerializer())}
    single = openapi.Response(description="response",
                              schema=MetadataPartSerializer())

    def find_by_id(self, _id):
        try:
            return MetadataPart.objects.get(pk=_id)
        except MetadataPart.DoesNotExist:
            return []

    def find_by_fk_metadata(self, _fk):
        metadata_part = MetadataPart.objects.all()
        res = []
        for i in metadata_part:
            tmp = i.fk_Metadata.id
            if tmp == int(_fk):
                res.append(i)
        return res

    @swagger_auto_schema(operation_summary=doc_sum_get("MetadataPart"),
                         operation_description="Return single MetadataPart object if `id` or `fk_metadata` in "
                                               "query is present or return all MetadataPart objects",
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("metadata_part"),
                                            Parameter('fk_metadata', openapi.IN_QUERY,
                                                      'show only MetadataPart with this fk_metadata'
                                                      ' `metadata_part?fk_metadata=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def get(self, request):
        return get_id_or_fk(request, MetadataPartSerializer,
                            self.find_by_id, self.find_by_fk_metadata,
                            MetadataPart, 'fk_metadata')

    @swagger_auto_schema(operation_summary="Add new MetadataPart object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=MetadataPartSerializer)
    def post(self, request):
        serializer = MetadataPartSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update MetadataPart",
                         operation_description=doc_desc_put("MetadataPart"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('metadata_part')],
                         request_body=MetadataPartSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, MetadataPartSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete EditConfiguration",
                         operation_description=doc_desc_delete("EditConfiguration")+" or if `fk_metadata` in query is present",
                         responses={400: 'id  or fk_metadata in quary wasnt specified',
                                    404: 'object for this slug wasn\'t found',
                                    204: 'successfully deleted specified object'},
                         manual_parameters=[doc_parm_id_get('metadata_part'),
                                            Parameter('fk_metadata', openapi.IN_QUERY,
                                                      'delete MetadataPart with this fk_metadata `metadata_part?fk_metadata=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def delete(self, request):
        return delete_id_or_fk(request, self.find_by_id, self.find_by_fk_metadata, 'fk_metadata')
