from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import TestData, GeneralizedTestSuite
from test.api_core.utility import date_str_eq, int_rep


class AccountCoreFuncSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse('account')

    def get_test_data(self) -> TestData:
        return AccountTestData()


class AccountTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            # ("put", status.HTTP_201_CREATED): self.VALID_DATA_PUT_NEW,
            # ("put_obj", status.HTTP_200_OK): self.OBJ_VALID_PUT_OK,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any):
        d = {"expiration": date_str_eq,
             "credit": int_rep,
             "id": int_rep}
        if name not in d:
            return val1 == val2
        return d[name](val1, val2)

    IRRELEVANT_KEY = "some_irrelevant_data"

    VALID_DATA = [
        {
            "credit": 3000,
            "expiration": "2024-01-22T14:00:00Z"
        }, {
            "credit": 1500,
            "expiration": "2025-08-22T15:30:00Z"
        }, {
            "credit": 2700,
            "expiration": "2022-02-22T15:00:00Z"
        }, {
            "credit": "3800",
            "expiration": "2024-01-22T14:00:00Z"
        }, {
            "credit": "3800",
            "expiration": "2024-01-22T14:00:00Z",
            IRRELEVANT_KEY: [11, 25, 16, 2]
        }
    ]
    VALID_DATA_SC = [
        {
            "id": "This is not an id you fool",
            "credit": 3001,
            "expiration": "2025-08-22T15:00:00Z"
        }, {
            "id": 1,
            "credit": 3002,
            "expiration": "2025-08-22T15:00:00Z"
        }
    ]
    VALID_DATA_PUT = [
        {
            "id": 1,
            "credit": 2500,
            "expiration": "2024-01-22T14:00:00Z"
        }, {
            "id": 3,
            "credit": 3500,
            "expiration": "2030-08-22T15:30:00Z"
        }, {
            "id": "5",
            "credit": 2700,
            "expiration": "2022-02-22T15:00:00Z"
        }, {
            "id": 2,
            "credit": "3800",
            "expiration": "2024-01-22T14:00:00Z",
            IRRELEVANT_KEY: [11, 25, 16, 2]
        }, {
            "id": "5",
            "credit": "3800",
            "expiration": "2024-01-22T14:00:00Z"
        }
    ]
    VALID_DATA_PUT_NEW = [
        {
            "id": 6,
            "credit": 1920,
            "expiration": "2024-01-22T14:00:00Z"
        }, {
            "id": 255555565656565655656,
            "credit": 2700,
            "expiration": "2022-02-22T15:00:00Z"
        }, {
            "id": -1,
            "credit": "3800",
            "expiration": "2024-01-22T14:00:00Z",
            IRRELEVANT_KEY: [11, 25, 16, 2]
        }, {
            "id": "5",
            "credit": "3800",
            "expiration": "2024-01-22T14:00:00Z"
        }
    ]
    OBJ_VALID_PUT_OK = {
        "id": 6,
        "credit": 1215,
        "expiration": "2080-08-22T15:30:00Z"
    }

    INVALID_DATA_400 = [
        {
            "credit": "how about some of them apples"
        }, {
            "credit": 3000
        }, {
            "credit": "300O",
            "expiration": "2024-01-22T14:00:00Z"
        }, {
            "credit": 3000,
            "expiration": ""
        }, {}, {
            "credit": 5,
            "expiration": "'SELECT * FROM api_core_account"
        }, {"This is not a request, this is a threat"}
    ]
    INVALID_DATA_PUT_400 = [
        {
            "id": "ooops this is not an id",
            "credit": 2500,
            "expiration": "2024-01-22T14:00:00Z"
        }, {
            "credit": 3500,
        }, {
            "expiration": "2022-02-22T15:00:00Z"
        }, {}, {
            "id": "5",
            "hmmm": "yes",
            "a test case": "mmmm"
        }
    ]
