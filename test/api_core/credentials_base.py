from typing import Callable, Any, Iterable, Dict, Optional

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
import base64

from test.api_core.utility import format_assert_code_mismatch_help, format_assert_fail_help


def encode_credentials(username: str, password: str):
    base64.encodebytes("{}:{}".format(username, password).encode('utf-8'))


class CredentialsTestBase(APITestCase):
    ROOT_USER = "root"
    ROOT_PASS = "root"

    def setUp(self):
        # once auth is done we should setup and tear down using our super user
        # self._log_in(self.ROOT_USER, self.ROOT_PASS)
        try:
            self._url
        except AttributeError:
            self._url = None
        return super().setUp()

    def _log_in(self, username, password):
        """
        This method should authenticate using a basic auth
        (a method which requires username and password) and acquires a token.
        After that, the token overload is called to authenticate further requests
        """
        self.client.credentials(HTTP_AUTHORIZATION="Basic {}".format(
            encode_credentials(username, password)
        ))

        token_gen = reverse('token')
        response = self.client.get(token_gen, format="json")
        assert (response.status_code == status.HTTP_200_OK or
                response.status_code == status.HTTP_201_CREATED)
        self._log_in_token(response)

    def _log_in_token(self, token):
        self.client.credentials(HTTP_AUTHORIZATION="Token {}".format(token))

        # login_bounce = reverse('login')
        # response = self.client.get(login_bounce, format="json")
        # assert (response.status_code == status.HTTP_200_OK)

    def method_test_single(self, method: Callable, data: Any, expected_code: int,
                           additional_check_func: Optional[Callable[[Any, Any], bool]] = None,
                           url: str = None,
                           form: str = "json") -> None:
        """
        Calls a method, which should be set to one of the client methods, passes data as an argument
        and asserts whether the correct response code has arrived, after that it runs the results
        through the check function
        :param method: should be one of a self.client.{post,get,put,patch,delete}
        :param data: request body
        :param expected_code: HTTP response code
        :param additional_check_func: function which will be supplied as
        additional_check_func(data, response.data)
        :param url: URL of the request, if not set equal to self._url
        :param form: format which should be used as interpretation
        :return: None
        """
        if url is None:
            url = self._url
        response = method(url, data, format=form)
        self.assertEquals(response.status_code, expected_code,
                          format_assert_code_mismatch_help(data, response.status_code,
                                                           expected_code, response.content))
        if additional_check_func is not None:
            self.assertTrue(additional_check_func(data, response.data),
                            format_assert_fail_help(data, response.data, None,
                                                    additional_check_func))

    def method_test_data_src(self, method: Callable, data: Iterable[Any],
                             expected_code: int,
                             additional_check_func: Optional[Callable[[Any, Any, int], bool]] =
                             None,
                             url_get: Optional[Callable[[int, Dict], str]] = None,
                             form: str = "json") -> None:
        """
        Calls a method, which should be set to one of the client methods, iterates through the data
        collection and passes each element to the method as a request body then asserts whether
        the correct response code has arrived, and then it runs the results through the check
        function
        :param method: should be one of a self.client.{post,get,put,patch,delete}
        :param data: A collection of request bodies
        :param expected_code: HTTP response code
        :param additional_check_func: function which will be supplied as
        additional_check_func(data, response.data, index_in_data)
        :param url_get: URL of the request, is called as url_get(index, ent), if not set equal
        to self._url for each
        :param form: format which should be used as interpretation
        :return: None
        """
        if url_get is None:
            def url_get(*_): return self._url

        for index, ent in enumerate(data):
            response = method(url_get(index, ent), ent, format=form)

            formatted_debug_msg = format_assert_code_mismatch_help(ent,
                                                                   response.status_code,
                                                                   expected_code,
                                                                   response.content)

            self.assertEquals(response.status_code, expected_code, formatted_debug_msg)
            if additional_check_func is None:
                continue
            self.assertTrue(additional_check_func(ent, response.data, index),
                            format_assert_fail_help(ent, response.data, index,
                                                    additional_check_func))
