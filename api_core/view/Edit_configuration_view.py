from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class Edit_configuration_view(APIView):
    res_many = {400: 'id is NOT numeric or'
                     'or both id and fk_req is present'
                     'or request has bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=FlatRateSerializer(many=True))}

    res_quary = {404: 'object for this slug wasn\'t found',
                 400: 'id in quary wasn\'t specified or request has bad format',
                 200: openapi.Response(description="response",
                                       schema=EditConfigurationSerializer())}
    single = openapi.Response(description="response",
                              schema=EditConfigurationSerializer())

    def find_by_id(self, _id):
        try:
            return EditConfiguration.objects.get(pk=_id)
        except EditConfiguration.DoesNotExist:
            return []

    def find_by_fk_Request(self, _fk):
        edit_configuration = EditConfiguration.objects.all()
        res = []
        for i in edit_configuration:
            tmp = i.fk_Request.id
            if tmp == _fk:
                res.append(i)
        return res

    @swagger_auto_schema(operation_summary=doc_sum_get("EditConfiguration"),
                         operation_description="Return single EditConfiguration object if `id` or `fk_req` in "
                                               "query is present or return all EditConfiguration objects",
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("edit_configuration"),
                                            Parameter('fk_req', openapi.IN_QUERY,
                                                      'show only EditConfiguration with this fk_req `edit_configuration?fk_req=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def get(self, request):
        return get_id_or_fk(request, EditConfigurationSerializer, self.find_by_id,
                            self.find_by_fk_Request, EditConfiguration, 'fk_req')

    @swagger_auto_schema(operation_summary="Add new EditConfiguration object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=EditConfigurationSerializer)
    def post(self, request):
        serializer = EditConfigurationSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update EditConfiguration",
                         operation_description=doc_desc_put("EditConfiguration"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('edit_configuration')],
                         request_body=EditConfigurationSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, EditConfigurationSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete EditConfiguration",
                         operation_description=doc_desc_delete("EditConfiguration")+" or if `fk_req` in query is present",
                         responses={400: 'id  or fk_req in quary wasnt specified',
                                    404: 'object for this slug wasn\'t found',
                                    204: 'successfully deleted specified object'},
                         manual_parameters=[doc_parm_id_get('edit_configuration'),
                                            Parameter('fk_req', openapi.IN_QUERY,
                                                      'delete EditConfiguration with this fk_req'
                                                      ' `edit_configuration?fk_req=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def delete(self, request):
        return delete_id_or_fk(request, self.find_by_id, self.find_by_fk_Request, 'fk_req')
