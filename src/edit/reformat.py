# inherits from Edit, changes format of photo
"""
    @file reformat.py
    @author Olivie A. Franklova
    @date 22.4.2021
"""
import os.path

from .edit import Edit
from PIL import Image
"""
@brief This class represents edit which reformat photo.
"""


class Reformat(Edit):
    """
    @brief This is constructor for class Reformat.

    @param format has type string
    """

    def __init__(self, format, name):
        super().__init__()
        self.format = format
        self.name = os.path.splitext(name)[0]

    @property
    def format(self):
        return self._format

    @format.setter
    def format(self, format):
        if format == "png" or format == "PNG":
            self._format = "PNG"
        elif format == "jpg" or format == "JPG" or format == "JPEG" or format == "jpeg":
            self._format = "JPEG"
        else:
            raise ValueError("This format is not supported")

    def get_full_name(self):
        return self.name + "." + self._format

    """
    @brief Reformat photo.

    @param photo has type image
    @return reformated photo.
    """
    def execute(self, photo):
        full_path = self.name + "." + self._format
        photo.save(full_path) # """TODO make this to rewrite format posix"""
        new_photo = Image.open(full_path)
        return new_photo

    """
    @brief Resize photo and save it.

    @param photo has type image.
    @param name is old name of photo, has type string.
    @param m_format is format
    @return resized photo.
    """
    def execute_and_save(self, photo, name, m_format):
        new_photo = self.execute(photo)
        name = name + "_reformat_" + m_format
        new_photo.save(name)
        return new_photo