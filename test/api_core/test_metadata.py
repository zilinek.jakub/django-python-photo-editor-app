from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_photo import PhotoTestData
from test.api_core.test_processed_photo import ProcessedPhotoTestData
from test.api_core.utility import delete_generated_test_data

PROCESSED_PHOTO_DEPENDENCY = [
    {
        "photo": 1,
        "photo_path": "./income_photos/nonsense_data1,png"
    }, {
        "photo": 2,
        "photo_path": "./income_photos/nonsense_data2,png"
    }, {
        "photo": 3,
        "photo_path": "./income_photos/nonsense_data3,png"
    }, {
        "photo": 4,
        "photo_path": "./income_photos/nonsense_data4,png"
    }
]

class MetadataCoreTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        photo_test_data = PhotoTestData()
        for _ in range(0, 2):
            # Seek happens inside the function
            photo_data = photo_test_data.get_test_data("post", status.HTTP_201_CREATED)
            self.create_dependency_data(reverse("photo"), photo_data, form="multipart")

        self.create_dependency_data(reverse("processed"), PROCESSED_PHOTO_DEPENDENCY)

    def get_url(self) -> str:
        return reverse("metadata")

    def get_test_data(self) -> TestData:
        return MetadataTestData()

    @classmethod
    def tearDownClass(cls):
        delete_generated_test_data()


class MetadataTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_409,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_409_CONFLICT): self.INVALID_DATA_PUT_409,
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    VALID_DATA = [
        {
            "processed_photo": 1
        }, {
            "processed_photo": 2
        }, {
            "processed_photo": 3
        }
    ]

    INVALID_DATA_400 = [
        {
            "processed": 1
        }, {}, "", {
            "processed_photo": "nope"
        }
    ]

    INVALID_DATA_409 = [
        VALID_DATA[0]
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "processed_photo": 4
        }, {
            "id": 2,
            "processed_photo": 1
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "processed": 1
        }, {}, "", {
            "processed_photo": "nope"
        }
    ]

    INVALID_DATA_PUT_409 = [
        {
            "id": 1,
            "processed_photo": 3
        }
    ]

    IRRELEVANT_KEY = "fsdjkhgskjhgkshfk"
