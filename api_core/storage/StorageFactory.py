from django.conf import settings

from api_core.storage.storage_type.LocalStorage import LocalStorage


class StorageFactory:
    __storages = {
        'local': LocalStorage()
    }

    @classmethod
    def get_storage(cls, storage_name):
        return cls.__storages.get(storage_name, None)

    @classmethod
    def get_default_storage(cls):
        return cls.get_storage(settings.DEFAULT_STORAGE)
