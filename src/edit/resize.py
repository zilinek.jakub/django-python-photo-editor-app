# inherits from edit, resizes photo
"""
    @file resize.py
    @author Olivie A. Franklova
    @date 22.4.2021
"""
from .edit import Edit

"""
@brief This class represents edit which resize photo.
"""


class Resize(Edit):
    """
    @brief This is constructor for class Resize.

    @param width has type int
    @param height has type int
    """

    def __init__(self, width, height):
        super().__init__()
        self.width = width
        self.height = height

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, width):
        if width < 0:
            raise ValueError("Width below 0 is not possible.")
        self._width = width

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, height):
        if height < 0:
            raise ValueError("Height below 0 is not possible.")
        self._height = height

    """
    @brief Resize photo.

    @param photo has type image
    @return resized photo.
    """
    def execute(self, photo):
        new_photo = photo.resize((self.width, self.height))
        return new_photo

    """
    @brief Resize photo and save it.

    @param photo has type image.
    @param name is old name of photo, has type string.
    @param m_format is format
    @return resized photo.
    """
    def execute_and_save(self, photo, name, m_format):
        new_photo = self.execute(photo)
        name = name + "_resize_" + self.width + "_" + self.height + m_format
        new_photo.save(name)
        return new_photo
