from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class Metadata_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=MetadataSerializer(many=True))}
    res_quary = {404: 'object for this slug wasn\'t found',
                  400: '?id in quary wasn\'t specified or request has bad format',
                  200: openapi.Response(description="response",
                                        schema=MetadataSerializer())}
    single = openapi.Response(description="response",
                              schema=MetadataSerializer())


    def find_by_id(self, _id):
        try:
            return Metadata.objects.get(pk=_id)
        except Metadata.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Metadata"),
                         operation_description=doc_desc_get("Metadata"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("metadata")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, MetadataSerializer)
        metadata = Metadata.objects.all()
        serializer = MetadataSerializer(metadata, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Metadata object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=MetadataSerializer)
    def post(self, request):
        serializer = MetadataSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Metadata",
                         operation_description=doc_desc_put("Metadata"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('metadata')],
                         request_body=MetadataSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, MetadataSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Metadata",
                         operation_description=doc_desc_delete("Metadata"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('metadata')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
