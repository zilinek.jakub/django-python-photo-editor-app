from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView
from ..serializers import *


class LoginView(APIView):
    def find_by_username(self, _user):
        try:
            return User.objects.get(username=_user)
        except User.DoesNotExist:
            return []

    def checkFormat(self, data):
        if "username" in data and "password" in data:
            return True
        return False

    @swagger_auto_schema(operation_summary="Verified that username and password are correct ",
                         responses={
                             200: 'login success',
                             401: 'login failed(bad username or password)',
                             400: 'bad format of request',
                         },
                         required=["username", "password"],
                         request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                                     properties={
                                    'username': openapi.Schema(type=openapi.TYPE_STRING, description='title: username'),
                                    'password': openapi.Schema(type=openapi.TYPE_STRING, description='title: password'),
                                })
                         )
    def post(self, request):
        dat = request.data
        if not self.checkFormat(dat):
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        user = self.find_by_username(dat['username'])
        if not user:
            return HttpResponse(status=status.HTTP_401_UNAUTHORIZED)

        if user.password == dat['password']:
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_401_UNAUTHORIZED)
