from typing import Any, Dict, List

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_banned_edit import BannedEditTestData
from test.api_core.test_edit import EditTestData
from test.api_core.utility import create_id_url, check_as_set, json_similar_ignore_compare


class FlatRateTypeCoreFuncSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)
        self.create_dependency_data(reverse('ban_edit'), BannedEditTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse('FR_type')

    def get_test_data(self) -> TestData:
        return FlatRateTypeTestData()

    def check_cascades(self):
        self.create_post_data()
        self.method_test_single(self.client.delete, None, status.HTTP_204_NO_CONTENT,
                                url=create_id_url(reverse('ban_edit'), 2))
        response = self.client.get(self._url)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        for ent in response.data:
            assert(2 not in ent["banned_edits"])


class FlatRateTypeTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ('put', status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any):
        d = {"banned_edits": check_as_set}
        if name not in d:
            return val1 == val2
        return d[name](val1, val2)

    IRRELEVANT_KEY = "irrelevant_field"
    VALID_DATA = [
        {
            "daily_limit": 100,
            "name": "Basic",
            "price": 200,
            "banned_edits": [2]
        }, {
            "daily_limit": 200,
            "name": "Advanced",
            "price": 300,
            "banned_edits": [2, 1, 3]
        }, {
            "daily_limit": 300,
            "name": "Standard",
            "price": 500,
            IRRELEVANT_KEY: 15
        }, {
            "name": "Pro",
            "price": 1000,
            "banned_edits": [5, 4]
        }
    ]
    VALID_DATA_PUT = [
        {
            "id": 2,
            "daily_limit": 153,
            "name": "Advanced",
            "price": 100,
            "banned_edits": [2, 3]
        }, {
            "id": 3,
            "daily_limit": 300,
            "name": "Standard-",
            "price": 400,
            "banned_edits": [1],
            IRRELEVANT_KEY: 15
        }, {
            "id": 4,
            "daily_limit": 1000,
            "name": "Pro",
            "price": 940,
            "banned_edits": []
        }
    ]
    VALID_DATA_PUT_NEW = [
        {
            "id": 9,
            "daily_limit": 200,
            "name": "Advanced+",
            "price": 350,
            "banned_edits": []
        }, {
            "id": 10,
            "name": "uuuh",
            "price": 100,
            "banned_edits": [3, 4],
            IRRELEVANT_KEY: "something"
        }
    ]
    OBJ_VALID_PUT_OK = {
        "id": 4,
        "daily_limit": None,
        "name": "Pro",
        "price": 999,
        "banned_edits": []
    }

    INVALID_DATA_PUT_400 = [
        {
            "id": 4,
            "name": "Pro",
            "banned_edits": []
        }, {
            "id": 3,
            "daily_limit": 153,
            "price": 100,
        }, {}, {
            "id": 4,
            "name": "Pro",
            "banned_edits": [15]
        },  {
            "id": 4,
            "name": "Pro",
            "daily_limit": 153,
            "banned_edits": [15, 4]
        }]
    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "name": "Pro",
            "price": 251,
            "banned_edits": [15]
        },  {
            "id": 4,
            "name": "Pro",
            "price": 553,
            "daily_limit": 153,
            "banned_edits": [15, 4]
        }
    ]

    INVALID_DATA_400 = [
        {
            "price": 999,
            "banned_edits": []
        }, {
            "daily_limit": 153,
            "price": 100,
        }, {
            "name": "Pho",
            "banned_edits": [2, 5]
        }, {}, "", {
            "banned_edits": [1, 15, 4],
            "name": "Extra",
            "daily_limit": 153
        }
    ]
    INVALID_DATA_422 = [
        {
            "name": "Basic",
            "price": 153,
            "banned_edits": [3, 4, 10]
        }, {
            "banned_edits": [1, 15, 4],
            "name": "Extra",
            "daily_limit": 153,
            "price": 251251251
        }
    ]
