# Class that contains photo, edits to do, semiprocessed income_photos
"""
    @file photo.py
    @author Olivie A. Franklova
    @date 21.4.2021
"""
from PIL import Image

from api_core.storage.StorageFactory import StorageFactory
from src.edit.reformat import Reformat


class Photo:
    """
    @brief This is constructor for class Photo.

    Photo has photo_path, array of edits and photo which is image load from photo_path
    @param photo_path has type string.
    @param edits is array of edits.
    """

    def __init__(self, photo_path, edits):
        # This can be moved to the setter however we do need this value
        # if the first read operation failed otherwise an attribute error is raised
        self._photo = None
        self.photo_path = photo_path
        self.edits = edits

    def __del__(self):
        if self._photo is None:
            return
        self._photo.close()

    @property
    def photo_path(self):
        return self._photo_path

    """
    @brief Setter photo_path also sets photo

    """

    @photo_path.setter
    def photo_path(self, photo_path):
        self._photo_path = photo_path
        self._photo = Image.open(self.photo_path).convert('RGB')

    @property
    def edits(self):
        return self._edits

    """
    @brief Setter edits also sets photo for all edits.

    """

    @edits.setter
    def edits(self, edits):
        self._edits = edits

    """
    @brief Execute all edits.

    """

    def execute_all_edits(self):
        for edit in self.edits:
            self._photo = edit.execute(self._photo)

            # Don't blame me. Blame the design
            if isinstance(edit, Reformat):
                self.photo_path = edit.get_full_name()

    def save(self, path):
        StorageFactory.get_default_storage().save_image(path, self._photo)

    # def save(self, path, format):
    #     self._photo.save(path, format)

# https://note.nkmk.me/en/python-pillow-paste/
# https://www.codementor.io/@saptaks/python-image-library-convert-type-and-quality-of-image
# -8h499tksz
# https://auth0.com/blog/image-processing-in-python-with-pillow/
# https://www.programiz.com/python-programming/property
