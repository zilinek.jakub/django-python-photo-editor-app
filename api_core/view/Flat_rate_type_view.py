from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class Flat_rate_type_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=FlatRateTypeSerializer(many=True))}
    res_quary = {404: 'object for this slug wasn\'t found',
                  400: '?id in quary wasn\'t specified or request has bad format',
                  200: openapi.Response(description="response",
                                        schema=FlatRateTypeSerializer())}
    single = openapi.Response(description="response",
                              schema=FlatRateTypeSerializer())

    def find_by_id(self, _id):
        try:
            return FlatRateType.objects.get(pk=_id)
        except FlatRateType.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("FlatRateType"),
                         operation_description=doc_desc_get("FlatRateType"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("flat_rate_type")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, FlatRateTypeSerializer)
        flat_rate_type = FlatRateType.objects.all()
        serializer = FlatRateTypeSerializer(flat_rate_type, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new FlatRateType object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=FlatRateTypeSerializer)
    def post(self, request):
        serializer = FlatRateTypeSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update FlatRateType",
                         operation_description=doc_desc_put("FlatRateType"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('flat_rate_type')],
                         request_body=FlatRateTypeSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, FlatRateTypeSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete FlatRateType",
                         operation_description=doc_desc_delete("FlatRateType"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('flat_rate_type')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
