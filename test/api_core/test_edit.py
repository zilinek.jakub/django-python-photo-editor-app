from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import TestData, GeneralizedTestSuite
from test.api_core.utility import create_id_url, format_assert_code_mismatch_help


class EditCoreFuncSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse('edit')

    def get_test_data(self) -> TestData:
        return EditTestData()

    def test_core_functionality_get_fail(self):
        response = self.client.delete(create_id_url(self._url,
                                                    "UgaBunga",
                                                    EditTestData.get_pk_field_name()))
        self.assertEqual(response.status_code,
                         status.HTTP_404_NOT_FOUND,
                         format_assert_code_mismatch_help({"primary_key": "UgaBunga"},
                                                          response.status_code,
                                                          status.HTTP_404_NOT_FOUND,
                                                          response.content))

    def __create_url_func(self, created_data, id_field_name):
        def url_func(index, _):
            index_mod = index % len(created_data)
            _ent = created_data[index_mod]
            return create_id_url(self._url, _ent[id_field_name]
                                 if id_field_name in _ent else index_mod,
                                 id_field_name)
        return url_func

    def __put_ranges(self, _from, _to):
        self.create_post_data()
        created_data = self._test_data.get_test_data("post", status.HTTP_201_CREATED)

        id_field_name = self._test_data.get_pk_field_name()
        for code in range(_from, _to):
            data = self._test_data.get_test_data("put", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.put, data, code,
                                      url_get=self.__create_url_func(created_data, id_field_name))

    def put_ok_replace_present(self):
        self.__put_ranges(200, 400)

    def put_fail_replace_present(self):
        self.__put_ranges(400, 600)

    def test_core_functionality_delete_fail(self):
        self._shared_suite.test_core_functionality_delete_fail(
            EditTestData.get_pk_field_name(),
            "TheWeirdOne",
            ""
        )


class EditTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            # ("put", status.HTTP_201_CREATED): self.VALID_DATA_PUT_NEW,
            # ("put_obj", status.HTTP_200_OK): self.OBJ_VALID_PUT_OK,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_409_CONFLICT): self.INVALID_DATA_PUT_409,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def get_pk_field_name(cls) -> str:
        return "name"

    IRRELEVANT_KEY = "irrelevant_field"
    VALID_DATA = [
        {
            "name": "Reformat",
        }, {
            "name": "Watermark",
        }, {
            "name": "Resize",
        }, {
            "name": "Invert",
        }, {
            "name": "AllIn1"
        }, {
            "name": "CrazyEdit"
        }, {
            "name": "WatermarkExtra",
        }, {
            "name": "TheGoodOne",
        }]
    VALID_DATA_PUT = [
        {
            "name": "ReformatButDifferent"
        }, {
            "name": "WatermarkExtra+",
            IRRELEVANT_KEY: "something"
        }
    ]
    VALID_DATA_PUT_NEW = [
        {
            "name": "TheGoodOne"
        }, {
            "name": "WatermarkExtra",
            IRRELEVANT_KEY: "something"
        }
    ]
    OBJ_VALID_PUT_OK = {
        "name": "TheGoodOne"
    }

    INVALID_DATA_PUT_400 = [
        {
            "expiration": "2022-02-22T15:00:00Z"
        }, {}, {
            "name": None
        }]

    INVALID_DATA_PUT_409 = [
        {
            "name": "AllIn1"
        }]

    INVALID_DATA_400 = [
        {
            "credit": "how about some of them apples"
        }, {}, {
            "name": None,
        }
    ]
