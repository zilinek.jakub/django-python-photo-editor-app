from typing import Any
from unittest import skip

from django.urls import reverse
from rest_framework import status

from test.api_core import utility
from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_defined_parameter import DefinedParameterTestData
from test.api_core.test_edit import EditTestData
from test.api_core.test_edit_configuration import EditConfigurationTestData
from test.api_core.test_parameter_type import ParameterTypeTestData
from test.api_core.test_parameter_value import ParameterValueTestData
from test.api_core.test_photo import PhotoTestData
from test.api_core.utility import create_id_url, delete_generated_test_data


class SemiprocessedPhotoTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('edit'), EditTestData.VALID_DATA)
        self.create_dependency_data(reverse('type_param'), ParameterTypeTestData.VALID_DATA)
        self.create_dependency_data(reverse('def_param'), DefinedParameterTestData.VALID_DATA)
        self.create_dependency_data(reverse('values'), ParameterValueTestData.VALID_DATA)
        self.create_dependency_data(reverse("edit_conf"), EditConfigurationTestData.VALID_DATA)
        photoTestData = PhotoTestData()
        self.create_dependency_data(reverse("photo"),
                                    photoTestData.get_test_data("post", status.HTTP_201_CREATED),
                                    form="multipart")

    def get_url(self) -> str:
        return reverse("semiprocessed_photo")

    def get_test_data(self) -> TestData:
        return SemiprocessedPhotoTestData()

    @classmethod
    def tearDownClass(cls):
        delete_generated_test_data()

    def test_core_functionality_single_post_ok(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_201_CREATED)[0]
        self.method_test_single(self.client.post, data_to_create,
                                status.HTTP_201_CREATED, self.std_check)

        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(data_to_create, resp[0]))

        id_field_name = self._test_data.get_pk_field_name()
        id_value = self._test_data.get_pk_from_data(data_to_create)
        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(data_to_create, resp[0]),
                                create_id_url(self._url, 1 if id_value is None else id_value,
                                              id_field_name))

    @skip("Put is not used in this view.")
    def test_core_functionality_put_ok(self):
        pass

    @skip("Put is not used in this view.")
    def test_core_functionality_put_fail(self):
        pass


class SemiprocessedPhotoTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_409
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        data = {
            "latest": utility.date_str_eq
        }
        if name not in data:
            return utility.eq("none", val1, val2)
        return data[name](val1, val2)

    @classmethod
    def get_pk_field_name(cls) -> str:
        return "fk_photo"

    IRRELEVANT_KEY = "klasjflkjasfjkahsfdj"

    VALID_DATA = [
        {
            "photo_path": "../test_photos/legend.png",
            "latest": "2021-11-10T15:10:00+00:00",
            "fk_photo": 1,
            "edit_configuration": 1
        }, {
            "photo_path": "../test_photos/python_logo.png",
            "latest": "2021-11-10T15:10:00+00:00",
            "fk_photo": 2,
            "edit_configuration": 2,
            IRRELEVANT_KEY: "test"
        }
    ]

    INVALID_DATA_409 = [
        {
            "photo_path": "../test_photos/legend.png",
            "latest": "2021-11-8T15:10:00+00:00",
            "fk_photo": 1,
            "edit_configuration": 1
        }
    ]
