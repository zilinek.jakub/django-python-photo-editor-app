from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_role import RoleTestData
from test.api_core.test_user import UserTestData
from test.api_core.utility import eq

USER_DEPENDENCY = [
    {
        "username": "abs(legend)",
        "name": "Adam Procházka",
        "email": "adam.prochazka@profifoto.cz",
        "password": "123456789",
        "free_trial": False,
        "fk_account": None,
        "roles": [2]
    }, {
        "username": "novmar7",
        "name": "Marek Novotný",
        "email": "novmar7@photofire.com",
        "password": "251251251oo",
        "free_trial": False,
        "fk_account": None,
        "roles": [1, 3]
    }, {
        "username": "PanK",
        "name": "Petr Mok",
        "email": "mokp@photofire.com",
        "password": "weeI112-.8",
        "free_trial": False,
        "fk_account": None,
        "roles": [3]
    }
]


class TokenTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('role'), RoleTestData.VALID_DATA)
        self.create_dependency_data(reverse('user'), USER_DEPENDENCY)

    def get_url(self) -> str:
        return reverse("token")

    def get_test_data(self) -> TestData:
        return TokenTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("user"), 1,
                                              UserTestData.get_pk_field_name(),
                                              [TokenTestData.VALID_DATA[1],
                                               TokenTestData.VALID_DATA[2],
                                               TokenTestData.VALID_DATA[3]],
                                              self.method_test_single)


class TokenTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_POST_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_POST_422,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_POST_400,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_POST_422
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @staticmethod
    def __check_active_bool(val1: Any, val2: Any) -> bool:
        if val1.lower() == "true":
            return val2
        if val1.lower() == "false":
            return not val2
        raise Exception("This should not happen. This means source data is not 'true'"
                        "nor 'false' string (case insensitive)")

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        data = {
            "active": cls.__check_active_bool
        }
        if name not in data:
            return eq("none", val1, val2)
        return data[name](val1, val2)

    IRRELEVANT_KEY = "kjlsadhafjkhasdjklfh"

    VALID_DATA = [
        {
            "active": "True",
            "hash": "toto je hash",
            "fk_user": 1
        }, {
            "active": "True",
            "hash": "toto je jiný hash",
            "fk_user": 1
        }, {

            "active": "False",
            "hash": "neplatný hash",
            "fk_user": 1
        }, {
            "active": "True",
            "hash": "hash jiného uživatele",
            "fk_user": 2
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "active": "False",
            "hash": "toto je hash",
            "fk_user": 1
        }, {
            "id": 2,
            "active": "True",
            "hash": "hash",
            "fk_user": 1
        }
    ]

    INVALID_DATA_POST_400 = [
        {
            "active": "toto není bool",
            "hash": "test",
            "fk_user": 1
        }, {}, {
            "hash": "test",
            "fk_user": 1
        }, {
            "active": "True",
            "fk_user": 1
        }, {
            "active": "True",
            "hash": "test"
        }
    ]

    INVALID_DATA_POST_422 = [
        {
            "active": "True",
            "hash": "test",
            "fk_user": 100
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": 1,
            "active": "toto není bool",
            "hash": "test",
            "fk_user": 1
        }, {}, {
            "id": 1,
            "hash": "test",
            "fk_user": 1
        }, {
            "id": 1,
            "active": "True",
            "fk_user": 1
        }, {
            "id": 1,
            "active": "True",
            "hash": "test"
        }
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "active": "True",
            "hash": "test",
            "fk_user": 100
        }
    ]
