import os.path

from PIL import Image

from api_core.storage.SaveStorageAdapter import SaveStorageAdapter


class LocalStorage(SaveStorageAdapter):
    def get_image(self, path):
        if not os.path.isfile(path):
            raise FileNotFoundError("File does not exist. Path: " + path)
        return Image.open(path)

    def save_image(self, path, image):
        image.save(path)

    def save_file(self, path, file):
        with open(path, "wb") as f:
            f.write(file.read())

        f.flush()
        f.close()

    def delete_file(self, path):
        if not os.path.isfile(path):
            return
        os.remove(path)