from unittest.case import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.utility import *


class RegisterTest(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse("register")

    def get_test_data(self) -> TestData:
        return RegisterTestData()

    def test_core_functionality_single_post_ok(self):
        data_to_create = self.get_test_data().get_test_data("post", status.HTTP_201_CREATED)[0]
        self.method_test_single(self.client.post, data_to_create, status.HTTP_201_CREATED,
                                self.std_check)

    @skip("Not used in view")
    def test_core_functionality_put_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_put_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_get_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_get_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_ok(self):
        pass


class RegisterTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_UNIQ_409
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @staticmethod
    def __check_free_trial_bool(val1: Any, val2: Any) -> bool:
        if val1.lower() == "true":
            return val2
        if val1.lower() == "false":
            return not val2
        raise Exception("This should not happen. This means source data is not 'true'"
                        "nor 'false' string (case insensitive)")

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        data = {
            "free_trial": cls.__check_free_trial_bool
        }
        if name not in data:
            return eq("none", val1, val2)
        return data[name](val1, val2)

    IRRELEVANT_KEY = "adklsjdfkl;dsajfl"

    VALID_DATA = [
        {
            "username": "Tomato",
            "name": "Tomáš",
            "email": "glazrtom@fit.cvut.cz",
            "password": "$2y$13$FBSgS2wjHnXLY3xJRHqnM.28HHlQq6Ayhod/IZ3LTEX3WaU/wOBMS",
            "free_trial": "True"
        }, {
            "username": "Linus",
            "name": "Linus Sebastian",
            "email": "linus@gmail.com",
            "password": "$2y$13$Po/pWh0e9IUpCXoOyeFCy.5pTwrTJin0ZxumosKB31S6sZ02pOQ..",
            "free_trial": "False"
        }
    ]

    INVALID_DATA = [
        {
            "Username": "Kuba",
            "name": "Kuba",
            "email": "zilinjak",
            "password": "le hash",
            "free_trial": "True"
        }, {
            "name": "Test",
            "email": "test",
            "password": "test",
            "free_trial": "False"
        }, {
            "username": "Test",
            "name": "test",
            "password": "test",
            "free_trial": "False"
        }, {
            "username": "Test",
            "name": "test",
            "email": "test",
            "free_trial": "False"
        }, {
            "username": "This is too long usernae and therefore this will not pass",
            "name": "test",
            "email": "test",
            "password": "top secret hash",
            "free_trial": "False"
        }, {
            "username": "Test",
            "name": "This is too long name which will not pass",
            "email": "test",
            "password": "test",
            "free_trial": "False"
        }, {
            "username": "Tomato",
            "name": "Jiný Tomáš",
            "email": "unknown",
            "password": "heslo",
            "free_trial": "True"
        }
    ]

    # Test distinctive username
    INVALID_DATA_UNIQ_409 = [
        {
            "username": "Tomato",
            "name": "Jiný Tomáš",
            "email": "correct@email.cz",
            "password": "heslo",
            "free_trial": "True"
        },
        # {
        #     "username": "Kuba",
        #     "name": "Kuba",
        #     "email": "linus@gmail.com",
        #     "password": "heslo",
        #     "free_trial": "True"
        # }
    ]
