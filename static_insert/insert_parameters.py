import os
import traceback

from assets import _parameter
import requests

def getURI():
    __deploy = os.environ.get('DJANGO_DEPLOY_VARIABLE', '')
    if __deploy == 'deploy':
        return 'https://mediagraphix.herokuapp.com/'
    else:
        return 'http://localhost:8000/'


def parameter_insert():
    _created = 0
    URI = getURI()
    try:
        for param in _parameter:
            _data = {"name": param[0], "fk_parameter_type" : param[2], "fk_edit": param[1]}
            response = requests.post(URI+'def_param/', data=_data)
            if response.status_code != 201 and response.status_code != 409:
                print("Price insert failed!")
                return False
            if response.status_code == 201:
                _created += 1
    except Exception as e:
        print("Price insert failed!")
        print(traceback.format_exc())
        return False
    print("Parameter insert success.\t Created ", _created)
    return True
