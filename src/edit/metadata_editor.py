# inherits from Edit, edits and adds values to photo metadata
"""
    @file Metadata_editor.py
    @author Olivie A. Franklova
    @date 4.5.2021
"""
# from .Edit import Edit
from PIL import Image
from PIL.ExifTags import TAGS
import piexif

"""
@brief This class represents edit which edit metadata.

"""


class MetadataEditor:
    """
    @brief Constructor for class Metadata

    @param param has type json and include data for edit
    """
    # def __init__(self, param):
    #     self.param = param

    """
    @brief Execute all changes to metadata

    @param photo has type Image
    """
    def execute(self, photo):
        exif_dict = piexif.load(photo.info['exif'])
        print(exif_dict)

        # {'0th': {271: b'NIKON CORPORATION', 272: b'NIKON D3200', 274: 1, 282: (300, 1),
        # 283: (300, 1), 296: 2, 305: b'Ver.1.00 ',
        # 306: b'2017:10:12 12:35:19', 531: 2, 34665: 228, 34853: 38580},

        # 'Exif': {33434: (10, 600), 33437: (56, 10), 34850: 0, 34855: 200,
        # 34864: 2, 36864: b'0230', 36867: b'2017:10:12 12:35:19',
        # 36868: b'2017:10:12 12:35:19', 37121: b'\x01\x02\x03\x00',
        # 37122: (4, 1), 37380: (0, 6), 37381: (48, 10),
        # 37383: 5, 37384: 0, 37385: 16, 37386: (400, 10), 37500: b'Nikon\}

        print("\n\n")
        exif_data = photo.getexif()
        for i in exif_data:
            tag_name = TAGS.get(i, i)

            # passing the tagid to get its respective value
            value = exif_data.get(i)
            if tag_name not in self.param:
                continue

            # if tagname in toChack:
            if tag_name != "MakerNote":
                # As long as our minimal requirement is python < 3.6 we cannot use fstring
                # print(f"{tag_name:25}: {value}")
                print("{tag_name:25}: {value}".format(tag_name=tag_name, value=value))
        # if photo.has_exif:
        #     #print(photo.list_all())
        #     print(photo.exif_version)
        #     print(photo.model)
        #     print(photo.flash)
        #     print(photo.datetime)
        #     print(photo.user_comment)
        #     # image_members = []
        #     # image_members.append(dir(photo))
        #     # for i in enumerate(image_members):
        #     #     print(i)
        #     #     print("\n")
        #     #print(photo.gps_longitude)
        #
        #     print(f"Lens model: {photo.get('lens_model', 'Unknown')}")
        #     #print(photo.gps_longitude)
        #
        # else:
        #     print("does not contain any EXIF information.")


# myJason = {"ExifVersion": "0230",
#            "ComponentsConfiguration": "b'\x01\x02\x03\x00'",
#            "CompressedBitsPerPixel": "4.0",
#            "DateTimeOriginal": "2017:10:12 12:35:19",
#            "DateTimeDigitized": "2017:10:12 12:35:19",
#            "ExposureBiasValue": "0.0",
#            "MaxApertureValue": "4.8",
#            "MeteringMode": "5",
#            "LightSource": "0",
#            "Flash": "16",
#            "FocalLength": "40.0",
#            "UserComment": "b'ASCII\x00\x00\x00                                    '",
#            "ColorSpace": "1",
#            "ExifImageWidth": "6016",
#            "ExifInteroperabilityOffset": "38548",
#            "SceneCaptureType": "0",
#            "SubsecTime": "80",
#            "SubsecTimeOriginal": "80",
#            "SubsecTimeDigitized": "80",
#            "ExifImageHeight": "4000",
#            "SubjectDistanceRange": "0",
#            "Make": "NIKON CORPORATION",
#            "Model": "NIKON D3200",
#            "SensingMethod": "2",
#            "Orientation": "1",
#            "FileSource": "b'\x03'",
#            "ExposureTime": "0.016666666666666666",
#            "XResolution": "300.0",
#            "YCbCrPositioning": "2",
#            "FNumber": "5.6",
#            "SceneType": "b'\x01'",
#            "YResolution": "300.0",
#            "ExposureProgram": "0",
#            "CFAPattern": "b'\x00\x02\x00\x02\x00\x01\x01\x02'",
#            "GPSInfo": "{0: b'\x02\x03\x00\x00'}",
#            "CustomRendered": "0",
#            "ISOSpeedRatings": "200",
#            "ResolutionUnit": "2",
#            "ExposureMode": "0",
#            "FlashPixVersion": "b'0100'",
#            "WhiteBalance": "0",
#            "Software": "Ver.1.00 ",
#            "DateTime": "2017:10: 12 12: 35:19",
#            "DigitalZoomRatio": "1.0",
#            "FocalLengthIn35mmFilm": "60",
#            "GainControl": "0",
#            "Contrast": "0",
#            "Saturation": "0",
#            "Sharpness": "0",
#            "ExifOffset": "228",
#            "MakerNote": "..."
#            }
# met = MetadataEditor(myJason)
# img = Image.open("/Users/olivie/Documents/FIT/SP/implementace/"
#                  "bi-si_mediagraphix/src/test_photos/listi.JPG")
# met.execute(img)
# with open('/Users/olivie/Documents/FIT/SP/implementace/bi-si_mediagraphix/
#            src/test_photos/listi.JPG',
#            'rb') as palm_1_file:
#     img = Image(palm_1_file)
#     met.execute(img)
