[comment]: <> (@author Kuba)  


# BI-SI_Mediagraphix  
- DB models: https://gitlab.fit.cvut.cz/frankoli/rozsireni-sluzby-prodeje-fotografii-pro-fotografy
- Backend source code: https://gitlab.fit.cvut.cz/zilinjak/bi-si_mediagraphix
- Frontend source code: https://gitlab.fit.cvut.cz/belovdmi/mediagraphix-fe
## Project maintenance

### DockerHub and maintaining Dockerfile
In this section I'll show you how to maintain DockerHub Image that we are using in tests.
The easiest way is as follows:
 1. Build the container with ```docker build -t mediagraphix/testenv:<tag>```, tag is usually *latest*
 2. Now just push the built image to docker hub ```docker push mediagraphix/testenv:<same tag>```

The original documentation for docker hub is [here](https://docs.docker.com/docker-hub/repos/).

### BuildX
Nowadays OS are using different CPU architectures (Intel/AMD - amd64, Apple M1 - arm64). The problem you need to solve is that if you are working on new Apple M1 you will build images for ARCH amr64 and this image wont be able to run on AMD64 machines.

To solve this problem you can use too named **BuildX**. This tool is part of Docker and uses virtualization to build your image for multiple architectures in parallel and on any machine architecture. 

Firstly you need to create configuration for your buildX

### How to

- _Go info folder where your docker file is_
- Login into docker - `docker login`
- In case you have already used buildX your can list and then set configuration to already created one. This will list saved configurations - `docker buildx ls`
- Create BuildX builder - this builder will contain all configuration for your buildX - `docker buildx create --name <name>`
- Set BuildX to use your new configuration - `docker buildX use <name>`
- You can ispect your builder - `docker buildx inspect --bootstrap`
- Build and push your dockerfile for multiple archs - `docker buildx build --platform <architectures sepearated by comma without space> -t <remote image name:tag> --push .`
- Usage in our project - `docker buildx build --platform linux/arm64,linux/amd64,linux/arm/v7,linux/arm/v6 -t mediagraphix/testenv:latest --push .`
 
 > The build for multiple architectures is using QEMU virtualization. Build can take *VERY LONG TIME*. On M1 mac air took +- 20 mins

## How to start the project?  
  
While developing this project we are using **Docker** containers, so we all have the same environment. All used docker containers are setup with routing in **Docker-compose.yaml**.

## Docker compose  
- Env container => Linux distribution which contains the app itself with all dependencies installed.
- Postgres_dev container => Contains the main database in PostgreSQL.
- Postgres_test container => Contains the test database in PostgreSQL, only when tests are running.  
  
#### Docker compose setup in IDE  
Right upper dropdown menu, right next to run button. Click on **Edit configurations...**  
Then click on + button, select **Docker** and then **Docker-compose**.  
Into "compose-files" textfield write **"./docker/docker-compose.yaml; "**. 
> Now you can select Docker-compose next to run button, this will run your docker containers and app automatically. 
    
### What to know about Docker ?  
  
What is an image? Template of a container.   
What is a container? Instance of an image.
  
### Useful commands for Docker or Docker-compose 
This section will cover few useful commands that you may want to use. 

**Docker-compose.yaml**  
```docker-compose up -d``` causes a container to start up, *-d* makes it run in the background. This command needs to be run in the same folder as docker-compose.yaml.  

**Dockerfile**  
```docker run <cont_id or tag>``` runs an already built docker **container**.  
```docker exec -it <cont_id> <command>``` executes a command inside the <cont_id>.  
```docker run -it <container> bash``` to temporally create a container and run bash.  

**Docker**  
```docker build -t <tag of the image> <location>``` builds a container from an image.  
```docker container ls``` lists all containers currently running.  
```docker image ls``` lists all used or published images.  
> tags: -a shows all images/containers, defaults shows only running containers and default hides intermediate images
 
## Run with docker-compose  
 1. Install Docker
 2. Run command ```docker-compose up``` inside the **docker** folder
 3. App starts along with starting all databases and sets port forwarding 8000 to 8000  

## Run without Docker 
Mediagraphix app uses python3 (download can be found [**here**](https://www.python.org/downloads/)) and multiple libraries. All libraries can be found in **requirements.txt** file in the root of git repository.

 1) Download and install python 3 (with a package manager)
    - \[Recommended optional] [Download, setup, and activate VirtualEnvironment](#virtual-environment-tutorial) (*similar to docker container*)
 2) Download all required libraries
 3) Run command ```python3 manage.py runserver 0.0.0.0:<port>``` inside of the project root folder.

#### Virtual environment tutorial

In this blog you will follow few commands that will setup your environment, so it is able to run Mediagraphix.

- Install virtual environment
```
pip install virtualenv
```
- create virtual environment 
```
python3 -m venv <name/folder of your virtual enviroment>
```  
- Activate the virtual environment
```
(linux) > source <folder_of_virtual_env>/bin/activate
(windows) > .\<folder_of_virtual_env>\Scripts\activate.bat
```
- Install all requirements
```
pip install -r requirements.txt
```
- Now all should be set, and you can run our app
```
python3 manage.py runserver 0.0.0.0:8000
```

### Run app without IDE
Make sure you have installed all libraries specified in **requirements.txt**. If you did so with the use of virtual environment, make sure to activate it.  

Simply run command: ```python3 manage.py runserver 0.0.0.0:<port>```

The output should look similar to this one:
> Performing system checks...
> 
> System check identified no issues (0 silenced).  
> June 17, 2021 - 12:22:12  
> Django version 2.0.13, using settings 'api.settings'  
> Starting development server at http://0.0.0.0:8000/  
> Quit the server with CONTROL-C.

## Testing

After you are done changing the project python code, you should make sure you have not broken anything on accident. 

Each branch gets tested with two sets of tests:
- Integration tests of API.
- Unit tests of photo editing features.

You should run both of these sets to verify that your changes do not have perceivable negative impact.

### Testing with command line

Django executes the tests in accordance with **api/settings.py**. Among other things this sets up a testing database.
In order to run following commands, you have to use the python version which has all the dependencies (specified in **requirements.txt**) installed.

To run the set of integration tests, use: ```python3 manage.py test test.api_core```  
To run the set of photo unit tests, use: ```python3 -m unittest test.photo_test```  

Both of these commands try to find all test cases in modules test.api_core (test/api_core/*) and test.photo_test (test/photo_test.py). As you can see, not only can you specify which test suite to run, you can also specify which specific test case to run.

So for example:  
```
python3 manage.py test test.api_core.test_account.AccountCoreFuncSuite.test_core_functionality_get_ok
```
would take a look into the package ***test.api_core.test_account*** (file test/api_core/test_account.py), then it would find the class ***AccountCoreFuncSuite*** and run test method ***test_core_functionality_get_ok***

Unittest and all testing frameworks built atop of it have test case recognition, any method prefixed with ***test_*** will be ran as a test case except the ones with a ***@skip*** annotation. Methods without the correct prefix will not be used as test methods unless explicitly specified via the command similar to the previous one.

### Testing with IDE

To take advantage of IDE's testing features you need to set up django dependency. In IntelliJ based IDEs you need to locate the following screen and setup the location of settings.py:  
![window with Django settings](./documentation_img/django_settings.jpg "Settings for Django")  
- In IntelliJ Idea, you can find this window under File > Project Structure > Modules > Django  
If the Django module is not present under Mediagraphix project nor in the facet list, navigate yourself to **/.idea/** in the project root and in a file **bi-si_mediagraphix.iml** change the line:  
    ```
    <module type="JAVA_MODULE" version="4">
    ```
    to:
    ```
    <module type="PYTHON_MODULE" version="4">
    ```
- In PyCharm this screen can be found under File > Settings > Languages & Frameworks > Django

You can run both sets by selecting run on the **/test/** subdirectory.

![running all tests](./documentation_img/running_all_tests.jpg "Running all tests")  

Similarly, you can run all tests in just a single module, which are usually just separate suites.  
As with the command line, there is a way to run only a specific test suites and methods.  

![running_specific_tests](./documentation_img/running_individual_tests.jpg "Running specific tests")

You can see the results of your tests in the *Run* tab of your IDE.

![results](./documentation_img/test_results.jpg "Test results") 

On the left you can see a brief summary of whether the tests failed or succeed or whether they were skipped completely. On the right you may see a traceback of the currently selected test.  
You may notice four indicators:  
- Check mark => these tests have passed.
- Grey no entry sign => these tests got skipped.
- Orange cross => these tests failed an assertion. If you see one of these, you might have made a mistake that you should correct.
- Red exclamation => these tests failed on a critical failure, and resulted in a thrown exception. These should be fixed ASAP.  

Also, please note that *Run tab* has a checked "checkmark" button under the *Run:* at the top left of the previous image. This allows you to see all tests that have passed.

### Writing tests, test cases and test data

In order to keep this file as short as possible, please visit the page on [testing](./test/TESTING.md) for more information.

## Test data  
  
Show introduction how to start Mediagraphix and insert data via our API more about API documentation can be found [**HERE**]([api_docs]).
  
### Database setup

 1. Delete the local database (if you have one, it will be the **db.sqlite3** file)
 2. Generate migrations with ```python3 manage.py makemigrations```
    > **What are migrations?**  
    > In our app we define database entities with the use of python classes. In order to be able to convert these classes to many database languages, we take advantage of migrations. These migrations are different for each database language. The migration language target can be set in **api/settings.py** in the DATABASES field. Databases switch based on an environment variable, the default for debugging and testing purposes is django.db.backends.sqlite3.
 3. Create database entities according to migrations. This can be done with: ```python3 manage.py migrate --run-syncdb```

### Example of API requests

In folder server_scripts can be found set of  http files which are tests requests. These tests can be run whenever while the main app is running.  
  
Example request:  
```  
POST http://localhost:8000/param_type/  
Content-Type: application/json  
{  
 "name" : "int"
}  
```
Example response:
```
POST http://localhost:8000/param_type/  
Content-Type: application/json  
Status (201): Created
{  
 "id" : 1,
 "name" : "int"
}  
```

## Static analysis

In development, everyone can have a bit of a different idea how to stylize code. This is fine as long as you don't mind inconsistencies all over your codebase. IDEs do generally a good job at keeping your code consistent with the language recommendations, but these checks are, but a tip of an iceberg. IDEs are not capable of checking file naming, imports and generally any error which would require a compilation in any non-interpreted language.
> Even though we may want to ignore part of the import checks, sometimes it is possible to reimport classes multiple times, and those are the errors which should be rectified. 

Fortunately pylint and flake8 are exactly the tools we need to check for these problems.  
> For a comprehensive list of what pylint checks, you can in turn check out [pylint-errors](https://vald-phoenix.github.io/pylint-errors/). Or for a less comprehensive one - [pylint's documentation](https://docs.pylint.org/en/1.6.0/features.html#pylint-checkers-options-and-switches).  
> List of flake8 error codes is [here](https://flake8.pycqa.org/en/4.0.1/user/error-codes.html) + [additional violations](https://github.com/rocioar/flake8-django#list-of-rules).  

Since we are using two checkers and pylint can be more pedantic, there is a higher tolerance of errors in a pylint check (any grade above and equal to 8). There is a countermeasure to this tolerance - a result of a flake8 analysis should be squeaky clean.

### Running static analysis

To check your code with the analyzers, run these two commands in the root of Mediagraphix:  
```
# Pylint
# Note that for pylint it is not enough to pass '.' as a directory, doing so will check only the top level
(linux) pylint --output-format=text --rcfile=.pylintrc $(pwd) > ./pylint/pylint.log 2>&1
(windows) pylint --output-format=text --rcfile=.pylintrc %cd% > ./pylint/pylint.log 2>&1

# Flake8
flake8 --config=./.flake8rc . > ./flake8/flake8.log
```
The output is stored in the appropriate directories. You may notice, that both commands accept a running configuration. These settings are in the root folder and are used for the CI. Both of these tools use plugins for django, these configurations add these plugins to the execution and will get us rid of some unwarranted errors. If you want to make your code really shine, you may use a pedantic version of these config files which do not ignore anything.

If the contribution fails only at the gitlab CI, the artifacts include the same files that you can create with the commands above, so you may download them if you do not like the console output.

# DevOps
To store source code of Mediagraphix we are using GitLab. To manage continuous integration and continuous delivery we are using GitLab CI/CD.

All settings for GitLab CI/CD can be found in **.gitlab-ci.yml** file.
Here are stages and descriptions to each stage for out CI/CD.
1. Static Analysis
   - [Flake8](https://flake8.pycqa.org/en/latest/)
   - [Pylint](https://pylint.org/)
> Both of these applications are running static analysis on mediagraphix source code. All results can be found in artifacts on GitLab.
2. Dynamic testing
   - API: We are currently testing API with [Django rest testing framework](https://www.django-rest-framework.org/api-guide/testing/).
   - Editing features are tested with the use of [unittest](https://docs.python.org/3/library/unittest.html).
   - More complex mechanisms use [Django testing framework](https://docs.djangoproject.com/en/3.2/topics/testing/) based on unittest.
3. Coverage
   - We are testing our code coverage with python [coverage lib](https://pypi.org/project/coverage/).
4. Deployment 
   - Needs to install few dependencies like node.js, curl and git
   - Removes githooks so gitlfs doesn't throw an error
   - Sets git config
   - Creates .netrc file with a username and API key from environment variables
   - Creates remote to heroku git
   - Pushes current folder revision to Heroku master branch
   - Heroku itself, after the push initialization, installs all dependencies in requirements.txt
   - Heroku has a process file -- **.procfile** -- that is managing main processes after a project is pulled and dependencies installed.
   - Our project has one process, its called **web**, we check if database is ready and then run server using python script, later we need to change this to gunicorn
> Deployment and hosting is provided by [HerokuApp](https://www.heroku.com/).
> Hosting is providing WebApp hosting, database in postgreSQL and git repository.

> We faced an issue with accessing Heroku API from [HerokuCLI](https://devcenter.heroku.com/categories/command-line).
> We needed to access git repository and Heroku API. With Heroku API key in an environment variable it was not enough to connect to GitLab.
> So we needed to add login parameters to .netrc file to the root directory. We have created env variables for a username and password.
> These variables contain API key, and username respectively and can be found in GitLab CI/CD settings. 


# Django Settings.py

Mediagraphix project uses environment variables to check if App should start as deployed version.

If App is supposed to run as deployed version, os has to have environment variable DJANGO_DEPLOY_VARIABLE=deploy.
For any other values including unset the app will run with test settings.

### What's the difference ?

In the testing/development environment app uses local SQLite3 database. The version deployed is using postgreSQL database.   
Also, with the testing environment, the app is running in api.settings.DEBUG=TRUE, this way, if an error occurs, error trace will be printed with more info to the command line. 

# Insert Static data to DB

When developing the app we may want to create some objects that will be in database all the time. 
These objects are set and may not needed to be changed.

The script in in main_insert, all inserting functions are run and checked for return value. **If some fails scripts returns 1 otherwise 0**. If 0 is returned CI/CD will be set as failed

> Example : In our project we have decided that for now we only need few Editations, Parameters, Types and prices for parameters.

> All of these objects are set and admin dont need to create them after every deploy. If we want to add some values to these object all can be found in static_insert/assest.py.

### How to add more static data ?
 1. Create static data in assets.py for model you want to add
 2. Create function, may be in separated file, that adds data to models and save them. Function should return True on success or False on fail. This will change behaviour of result of inserting static data.
 3. Add function name to _functions variable.
 4. DONE ! Add new added models will be run after deployment


# Useful commands

```heroku pg:reset -a mediagraphix --confirm mediagraphix; heroku run -a mediagraphix ./create_new_database.sh``` -> resets database to default, empty rows

```python3 manage.py graph_models -e -g -l dot -o DB_MODEL.png api_core``` -> generates database model in png
> Dependencies : ```pip install pydotplus```


