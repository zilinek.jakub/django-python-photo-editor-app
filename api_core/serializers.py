# @author Abi
from drf_yasg import openapi
from rest_framework import serializers
from .models import *
from .validators import EndDateValidator, validate_positive


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'


class RoleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Role
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = '__all__'


class BannedEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = BannedEdit
        fields = '__all__'


class FlatRateTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlatRateType
        fields = '__all__'


class FlatRateSerializer(serializers.ModelSerializer):
    class Meta:
        model = FlatRate
        fields = '__all__'
        extra_kwargs = {
            'valid_to': {'validators': [EndDateValidator('valid_from')]},
        }


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = '__all__'


class PhotoFileSerializer(serializers.Serializer):
    file = serializers.ImageField()


class RequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Request
        fields = '__all__'
        extra_kwargs = {
            'price': {'validators': [validate_positive]}
        }


class OutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = Output
        fields = '__all__'


class OriginalPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = OriginalPhoto
        fields = '__all__'


class ProcessedPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProcessedPhoto
        fields = '__all__'


class MetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Metadata
        fields = '__all__'


class MetadataPartSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetadataPart
        fields = '__all__'


class EditConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = EditConfiguration
        fields = '__all__'


class SemiprocessedPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SemiprocessedPhoto
        fields = '__all__'


class ParameterTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParameterType
        fields = '__all__'


class EditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Edit
        fields = '__all__'


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = '__all__'
        extra_kwargs = {
            'price': {'validators': [validate_positive]}
        }


class DefinedParameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = DefinedParameter
        fields = '__all__'


class ParameterValuesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ParameterValue
        fields = '__all__'


class UserRequestSerializer(serializers.ModelSerializer):
    request = RequestSerializer(many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'name', 'request']
