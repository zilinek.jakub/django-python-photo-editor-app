from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class Output_view(APIView):
    res_many = {400: 'id is NOT numeric or'
                     'or both id and fk_user is present'
                     'or request has bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=FlatRateSerializer(many=True))}
    single = openapi.Response(description="response",
                              schema=FlatRateSerializer())

    def find_by_id(self, _id):
        try:
            return Output.objects.get(pk=_id)
        except Output.DoesNotExist:
            return []

    def find_by_fk_user(self, _fk):
        output = Output.objects.all()
        res = []
        for i in output:
            tmp = i.fk_UserCustom_id
            if tmp == int(_fk):
                res.append(i)
        return res

    @swagger_auto_schema(operation_summary=doc_sum_get("Output"),
                         operation_description="Return single Output object if `id` or `fk_user` in "
                                               "query is present or return all Output objects",
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("output"),
                                            Parameter('fk_user', openapi.IN_QUERY,
                                                      'show only output with this fk_user `output?fk_user=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def get(self, request):
        return get_Id_or_User(request, OutputSerializer, self.find_by_id, self.find_by_fk_user, Output)

    @swagger_auto_schema(operation_summary="Add new Output object",
                         responses={400: 'request has bad format or'
                                         ' request or fk_user doesnt exist',
                                    status.HTTP_201_CREATED: single},
                         request_body=OutputSerializer)
    def post(self, request):
        serializer = OutputSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Output",
                         operation_description=doc_desc_put("Output"),
                         responses={400: 'request has bad format or'
                                         ' request or fk_user doesnt exist'
                                         ' or id is NOT present',
                                    status.HTTP_201_CREATED: single},
                         manual_parameters=[doc_parm_id('output')],
                         request_body=OutputSerializer
                         )
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, OutputSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Output",
                         operation_description=doc_desc_delete("Output")+" or if `fk_user` in query is present",
                         responses={
                             400: 'id or user in quary wasnt specified',
                             404: 'object for this slug wasn\'t found',
                             204: 'successfully deleted specified object'},
                         manual_parameters=[doc_parm_id_get('output'),
                                            Parameter('fk_user', openapi.IN_QUERY,
                                                      'delete Output with this fk_user'
                                                      ' `output?fk_user=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)

                                            ])
    def delete(self, request):
        return delete_id_or_user(request, self.find_by_id, self.find_by_fk_user)
