# @author Abi
from django.http import *
from drf_yasg.openapi import Parameter
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from .models import *
from .serializers import *


def get_codes_for_422():
    return {'does_not_exist', 'empty', 'logic_error'}


def get_codes_for_409():
    return {'unique'}


def validate_get_error_list(serializer):
    valid = True
    error_list = []
    try:
        serializer.is_valid(raise_exception=True)
    except ValidationError as ve:
        valid = False
        error_list = [item for sublist in list(ve.args[0].values()) for item in sublist]

    return valid, error_list


def get_status(err_lst):
    err_lst_codes = set(map(lambda x: x.code, err_lst))

    err_lst_codes = err_lst_codes.difference(get_codes_for_422())
    if len(err_lst_codes) == 0:
        return status.HTTP_422_UNPROCESSABLE_ENTITY
    err_lst_codes = err_lst_codes.difference(get_codes_for_409())
    if len(err_lst_codes) == 0:
        return status.HTTP_409_CONFLICT
    return status.HTTP_400_BAD_REQUEST


def serializer_validation(serializer):
    valid, err_lst = validate_get_error_list(serializer)
    if valid:
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=get_status(err_lst))


# The function above could be extended to cover puts
def serializer_validation_put(serializer):
    valid, err_lst = validate_get_error_list(serializer)
    if valid:
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors, status=get_status(err_lst))


def get_obj_by_pk(_pk, object_type):
    try:
        return object_type.objects.get(pk=_pk)
    except object_type.DoesNotExist:
        return None


def generic_op_by_pk(_pk, object_type, validation_function, operation):
    return generic_op_by_many(validation_function,
                              lambda pk: get_obj_by_pk(pk, object_type),
                              operation, pk=_pk)


def generic_op_by_many(validation_function, find_function, operation, **kwargs):
    if validation_function is not None and not validation_function(**kwargs):
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    obj = find_function(**kwargs)
    if not obj:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    return operation(obj)


def get_by_pk(_pk, object_type, serializer, validation_function=lambda pk: pk.isnumeric()):
    def serializer_operation(obj):
        ser = serializer(obj)
        return Response(ser.data)

    return generic_op_by_pk(_pk, object_type, validation_function, serializer_operation)


def get_by_many(find_function, serializer, validation_function=lambda x: x.isnumeric(), **kwargs):
    def serializer_operation(obj):
        ser = serializer(obj, many=True)
        return Response(ser.data)

    return generic_op_by_many(validation_function, find_function, serializer_operation, **kwargs)


def get_by_id(_id, find_function, serializer):
    if not _id.isnumeric():
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    obj = find_function(_id)
    if not obj:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    ser = serializer(obj)
    return Response(ser.data)


def get_by_id_many(_id, find_function, serializer):
    if not _id.isnumeric():
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    obj = find_function(_id)
    if not obj:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    ser = serializer(obj, many=True)
    return Response(ser.data)


def delete_by_pk(_pk, object_type, validation_function=lambda pk: pk.isnumeric()):
    def delete_operation(obj):
        obj.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return generic_op_by_pk(_pk, object_type, validation_function, delete_operation)


def delete_by_id(_id, find_function):
    if not _id.isnumeric():
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    obj = find_function(_id)
    if not obj:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    obj.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


def delete_by_id_many(_id, find_function):
    if not _id.isnumeric():
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    obj = find_function(_id)
    if not obj:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    for i in obj:
        i.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


def put_by_pk(_pk, object_type, serializer, request, validation_function=lambda pk: pk.isnumeric()):
    def serializer_operation(obj):
        ser = serializer(obj, data=request.data)
        return serializer_validation_put(ser)
    return generic_op_by_pk(_pk, object_type, validation_function, serializer_operation)


def put_by_id(_id, find_function, serializer, request):
    if not _id.isnumeric():
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    obj = find_function(_id)
    if not obj:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)
    ser = serializer(obj, data=request.data)
    return serializer_validation_put(ser)


def get_Id_or_User(request, serializer, find_by_id, find_by_fk_user, obj):
    _id = request.GET.get('id', '')
    _fk_user = request.GET.get('fk_user', '')
    if _id != '' and _fk_user != '':
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    if _id != '':
        return get_by_id(_id, find_by_id, serializer)
    if _fk_user != '':
        return get_by_id_many(_fk_user, find_by_fk_user, serializer)
    req = obj.objects.all()
    ser = serializer(req, many=True)
    return Response(ser.data)


def get_id_or_fk(request, serializer, find_by_id, find_by_fk, obj, fk):
    _id = request.GET.get('id', '')
    _fk = request.GET.get(fk, '')
    if _id != '' and _fk != '':
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    if _id != '':
        return get_by_id(_id, find_by_id, serializer)
    if _fk != '':
        return get_by_id_many(_fk, find_by_fk, serializer)
    req = obj.objects.all()
    ser = serializer(req, many=True)
    return Response(ser.data)


def delete_id_or_user(request, find_by_id, find_by_fk_user):
    _id = request.GET.get('id', '')
    _fk_user = request.GET.get('fk_user', '')
    if _id != '' and _fk_user != '':
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    if _id != '':
        return delete_by_id(_id, find_by_id)
    if _fk_user != '':
        return delete_by_id_many(_fk_user, find_by_fk_user)
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


def delete_id_or_fk(request, find_by_id, find_by_fk, fk):
    _id = request.GET.get('id', '')
    _fk = request.GET.get(fk, '')
    if _id != '' and _fk != '':
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
    if _id != '':
        return delete_by_id(_id, find_by_id)
    if _fk != '':
        return delete_by_id_many(_fk, find_by_fk)
    return HttpResponse(status=status.HTTP_400_BAD_REQUEST)


def doc_parm_id_get(typ):
    return Parameter('id', openapi.IN_QUERY,
                     'show only ' + typ + ' with this id `' + typ + '?id=2`',
                     type=openapi.TYPE_NUMBER, required=False)


def doc_parm_username_get(typ):
    return Parameter('username', openapi.IN_QUERY,
                     'show only  ' + typ + '  with this username `' + typ + '?username=Abi` ',
                     type=openapi.TYPE_STRING, required=False)


def doc_parm_id(typ):
    return Parameter('id', openapi.IN_QUERY,
                     'make operation with  ' + typ + ' with this id `' + typ + '?id=2`',
                     type=openapi.TYPE_NUMBER, required=True)


def doc_parm_username(typ):
    return Parameter('username', openapi.IN_QUERY,
                     'make operation with  ' + typ + ' with this username `' + typ + '?username=Abi` ',
                     type=openapi.TYPE_STRING, required=True)


delete_res = {400: 'id in quary wasnt specified',
              404: 'object for this slug wasn\'t found',
              204: 'successfully deleted specified object'}


def doc_sum_get(typ):
    return "Retrieve " + typ + " object"


def doc_desc_get(typ):
    return "Return single " + typ + "  object if `id` in query is present or return all " + typ + "s  objects"


def doc_desc_put(typ):
    return "Update " + typ + " only if `id` in query is present"


def doc_desc_delete(typ):
    return "Delete " + typ + " only if `id` in query is present"


