from drf_yasg.openapi import IN_QUERY
from drf_yasg.utils import swagger_auto_schema
import json
from rest_framework.views import APIView
from api_core.views import *


class DefinedParameterView(APIView):
    # __FK_EDIT_FIELD_NAME__ = 'fk_edit'
    __NAME_FIELD_NAME__ = 'name'

    # def has_same_name(self, data):
    #
    #     if self.__FK_EDIT_FIELD_NAME__ not in data or self.__NAME_FIELD_NAME__ not in data:
    #         return False
    #
    #     try:
    #         DefinedParameter.objects.get(fk_edit=data[self.__FK_EDIT_FIELD_NAME__],
    #                                      name=data[self.__NAME_FIELD_NAME__])
    #     except DefinedParameter.DoesNotExist:
    #         return False
    #     return True

    @swagger_auto_schema(operation_summary=doc_sum_get("DefinedParameter"),
                         operation_description=doc_desc_get("DefinedParameter"),
                         responses={404: 'slug not found',
                                    status.HTTP_200_OK: openapi.Response(
                                        description="response for get without id",
                                        schema=DefinedParameterSerializer(many=True)
                                    )
                                    },
                         manual_parameters=[
                             Parameter('name', IN_QUERY,
                                       'show only defined parameter with this name'
                                       '`def_param?name=`',
                                       type='number', required=False)])
    def get(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        if _pk:
            return get_by_pk(_pk, DefinedParameter, DefinedParameterSerializer, None)
        defined_param = DefinedParameter.objects.all()
        serializer = DefinedParameterSerializer(defined_param, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Defined parameter object",
                         responses={400: 'bad request',
                                    409: 'when object has same name and '
                                         'fk_edit as another',
                                    status.HTTP_201_CREATED : openapi.Response(
                                        description="response",
                                        schema=DefinedParameterSerializer()
                                    )},
                         request_body=DefinedParameterSerializer)
    def post(self, request):
        # if self.has_same_name(request.data):
        #     response_data = {
        #         "name": ["this parameter name is already in use, for fk_Edit = "
        #                  + str(request.data[__FK_EDIT_FIELD_NAME__])]
        #     }
        #     return HttpResponse(json.dumps(response_data), status=status.HTTP_409_CONFLICT)
        serializer = DefinedParameterSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Defined parameter",
                         operation_description=doc_desc_put("Defined parameter"),
                         responses={400: 'when name is not present',
                                    404: 'slug not found',
                                    409: 'when object has same name and fk_edit as another',
                                    status.HTTP_200_OK: openapi.Response(
                                        description="response",
                                        schema=DefinedParameterSerializer(many=False)
                                    )
                                    },
                         manual_parameters=[
                             Parameter('id', IN_QUERY,
                                       'put data to defined parameter with id `def_param?id=2`',
                                       type='number', required=True)],
                         request_body=DefinedParameterSerializer)
    def put(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        if _pk:
            return put_by_pk(_pk, DefinedParameter, DefinedParameterSerializer, request, None)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Defined Parameter",
                         operation_description=doc_desc_delete("Defined Parameter"),
                         responses={404: 'slug not found', 400: 'name is not present or empty'},
                         manual_parameters=[
                             Parameter('id', IN_QUERY,
                                       'delete data from defined parameter with id '
                                       '`def_param?id=2`',
                                       type='number', required=True)])
    def delete(self, request):
        _pk = request.GET.get(self.__NAME_FIELD_NAME__, '')
        if _pk:
            return delete_by_pk(_pk, DefinedParameter, None)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
