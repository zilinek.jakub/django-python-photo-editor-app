import markdown

markdown.markdownFromFile(
    input='../README.md',
    output='../README.html',
    encoding='utf8',
)

print("README.md \t> README.html")