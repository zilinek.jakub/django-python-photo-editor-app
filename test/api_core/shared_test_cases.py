from typing import Callable, Any, Sequence
from unittest import TestCase

from django.test import Client
from rest_framework import status

from test.api_core.utility import json_similar, eq, create_id_url
from test.api_core.utility import format_assert_code_mismatch_help, format_assert_fail_help


class SharedTestSuite:
    def __init__(self, client: Client, url):
        self._client = client
        self._url = url
        self._tc = TestCase()

    def test_core_functionality_get_fail(self,
                                         id_field_name: str = "id",
                                         first_nf_id: Any = 15,
                                         incorrect_id: Any = "iota",
                                         create_data: Callable = lambda: None):
        """
        A function which tests whether the get does fail on an id equal to first_nf_id
        (404 Expected) and then tests whether the incorrect_id as a url fails
        :param first_nf_id: First ID that will not be found in the posted entries
        :param incorrect_id: Id which is plainly incorrect
        :param create_data: Function which creates data on the endpoint
        :param id_field_name: Name of the primary key attribute
        """
        create_data()
        response = self._client.get(create_id_url(self._url, first_nf_id, id_field_name))
        self._assert_with_help(first_nf_id, response, status.HTTP_404_NOT_FOUND)
        response = self._client.get(create_id_url(self._url, incorrect_id, id_field_name))
        self._assert_with_help(incorrect_id, response, status.HTTP_400_BAD_REQUEST)
        # This is an edge case scenario where we have so many records we have
        # surpassed intmax, in that case this id should be valid, however
        # for the time of being we will leave it as invalid
        response = self._client.get(create_id_url(self._url, -5, id_field_name))
        self._assert_with_help(-5, response, status.HTTP_400_BAD_REQUEST)

    def test_core_functionality_get_ok(self,
                                       irrelevant_key: str,
                                       data: Sequence[Any],
                                       create_data: Callable,
                                       id_field_name: str = "id",
                                       field_comparison: Callable[[str, Any, Any], bool] = eq,
                                       irrelevant_get_field: str = "airjgsi=fshfiuwg"):
        create_data()
        response = self._client.get(self._url)
        self._assert_with_help(None, response, status.HTTP_200_OK)
        for i, ent in enumerate(response.data):
            if i >= len(data):
                break
            self._tc.assertTrue(json_similar(data[i], ent, irrelevant_key not in data[i],
                                field_comparison),
                                format_assert_fail_help(data[i], ent, i, json_similar))
            self._assert_with_help(None,
                                   self._client.get(create_id_url(self._url,
                                                                  ent[id_field_name],
                                                                  id_field_name)),
                                   status.HTTP_200_OK)
        response = self._client.get(self._url+"?"+irrelevant_get_field)
        self._assert_with_help(None, response, status.HTTP_200_OK)

    def test_core_functionality_delete_fail(self,
                                            id_field_name: str = "id",
                                            first_nf_id: Any = 15,
                                            incorrect_id: Any = "iota",
                                            create_data: Callable = lambda: None):
        create_data()
        response = self._client.delete(create_id_url(self._url, first_nf_id, id_field_name))
        self._assert_with_help(first_nf_id, response, status.HTTP_404_NOT_FOUND)
        response = self._client.delete(create_id_url(self._url, incorrect_id, id_field_name))
        self._assert_with_help(incorrect_id, response, status.HTTP_400_BAD_REQUEST)

    def _assert_with_help(self, pk_used, response, expected_code):
        self._tc.assertEqual(response.status_code, expected_code,
                             format_assert_code_mismatch_help({"primary_key": pk_used},
                                                              response.status_code,
                                                              expected_code,
                                                              response.content))

    def check_type_cascade(self,
                           dependency_url,
                           dependency_pk,
                           dependency_pk_name,
                           cascaded_entries,
                           delete_single_method):
        delete_single_method(self._client.delete, None, status.HTTP_204_NO_CONTENT,
                             url=create_id_url(dependency_url, dependency_pk, dependency_pk_name))

        response = self._client.get(self._url)
        self._assert_with_help(None, response, status.HTTP_200_OK)
        for ent in response.data:
            self._tc.assertTrue(ent not in cascaded_entries, "\nEntity: {} found but should"
                                                             "have been cascaded".format(ent))
