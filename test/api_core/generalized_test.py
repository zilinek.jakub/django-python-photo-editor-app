import abc
import unittest
from typing import Any, Callable, Optional

from rest_framework import status

from test.api_core.credentials_base import CredentialsTestBase
from test.api_core.shared_test_cases import SharedTestSuite
from test.api_core.utility import eq, json_similar, json_similar_ignore_compare, create_id_url


class TestData:
    @abc.abstractmethod
    def get_test_data(self, method: str, status_code: int) -> Any:
        pass

    @classmethod
    @abc.abstractmethod
    def get_irrelevant_key(cls) -> str:
        pass

    @classmethod
    def get_pk_field_name(cls) -> str:
        return cls.__DEFAULT_PK__

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        return eq(name, val1, val2)

    @classmethod
    def get_universal_json_compare_function(cls) -> Optional[Callable[[Any, Any, int], bool]]:
        return json_similar_ignore_compare(cls.get_irrelevant_key(), cls.detect_fields)

    @classmethod
    def get_pk_from_data(cls, data) -> Optional[str]:
        pk_field_name = cls.get_pk_field_name()
        return data[pk_field_name] if pk_field_name in data else None

    __DEFAULT_PK__ = 'id'


class GeneralizedTestSuite(CredentialsTestBase):
    @abc.abstractmethod
    def setup_dependency(self):
        """
        Use this method to set up your dependencies if you have some
        """
        pass

    @abc.abstractmethod
    def get_url(self) -> str:
        """
        This method should return the root url of all requests
        :return: root ul of all request
        """
        pass

    @abc.abstractmethod
    def get_test_data(self) -> TestData:
        """
        This method should return the appropriate test data instance which contains relevant test
        data.
        :return: TestData interface
        """
        pass

    def create_dependency_data(self, url, data,
                               additional_check_func: Optional[
                                   Callable[[Any, Any, int], bool]
                               ] = None,
                               form="json"):
        """
        Iterates through data and posts individual requests on the url, after the post performs
        an additional check defined by the additional_check_func (defauls to
        json_similar(en, resp, True, self.detect_fields)) to see if the dependency data was posted
        correctly, expects the servers response to be success.

        Is called during setUp method.

        :param url: the url where all post data will be sent
        :param data: an array of request bodies
        :param additional_check_func: function to check response validity
        :param form: format which should be used for encoding
        """

        self.method_test_data_src(self.client.post, data,
                                  status.HTTP_201_CREATED,
                                  additional_check_func,
                                  lambda x, y: url,
                                  form)

    @classmethod
    def setUpClass(cls):
        if cls is GeneralizedTestSuite:
            raise unittest.SkipTest("Base class")
        super(GeneralizedTestSuite, cls).setUpClass()

    def setUp(self):
        super().setUp()

        self._url = self.get_url()
        self._shared_suite = SharedTestSuite(self.client, self._url)
        self._test_data = self.get_test_data()
        self.setup_dependency()

    def std_check(self, ent, resp, cmp=None):
        if cmp is None:
            cmp = self._test_data.detect_fields
        return json_similar(ent, resp, True, cmp)

    def test_core_functionality_single_post_ok(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_201_CREATED)[0]
        self.method_test_single(self.client.post, data_to_create,
                                status.HTTP_201_CREATED, self.std_check)

        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(data_to_create, resp[0]))

        id_field_name = self._test_data.get_pk_field_name()
        id_value = self._test_data.get_pk_from_data(data_to_create)
        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(data_to_create, resp),
                                create_id_url(self._url, 1 if id_value is None else id_value,
                                              id_field_name))

    def create_post_data(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_201_CREATED)

        if data_to_create is None:
            return

        self.method_test_data_src(self.client.post, data_to_create, status.HTTP_201_CREATED,
                                  self._test_data.get_universal_json_compare_function())

    def test_core_functionality_post_ok(self):
        for code in range(200, 300):
            data = self._test_data.get_test_data("post", code)
            if data is None:
                continue

            cmp_func = self._test_data.get_universal_json_compare_function()
            self.method_test_data_src(self.client.post, data, code, cmp_func)

    def test_core_functionality_post_fail(self):
        self.create_post_data()
        for code in range(400, 600):
            data = self._test_data.get_test_data("post", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.post, data, code)

    def test_core_functionality_get_ok(self):
        created_data = self._test_data.get_test_data("post", status.HTTP_201_CREATED)
        self._shared_suite.test_core_functionality_get_ok(self._test_data.get_irrelevant_key(),
                                                          created_data, self.create_post_data,
                                                          self._test_data.get_pk_field_name(),
                                                          self._test_data.detect_fields)

    def test_core_functionality_get_fail(self):
        self._shared_suite.test_core_functionality_get_fail(self._test_data.get_pk_field_name(),
                                                            create_data=self.create_post_data)

    def _get_url_func(self, _, ent):
        return create_id_url(self._url, ent[self._test_data.get_pk_field_name()],
                             self._test_data.get_pk_field_name())

    def put_ok_replace_present(self):
        self.create_post_data()

        data = self._test_data.get_test_data("put", status.HTTP_200_OK)
        if data is None:
            return
        self.method_test_data_src(self.client.put, data, status.HTTP_200_OK,
                                  self._test_data.get_universal_json_compare_function(),
                                  self._get_url_func)

    def put_ok_create_new(self):
        data = self._test_data.get_test_data("put", status.HTTP_201_CREATED)

        if data is None:
            return

        self.method_test_data_src(self.client.put, data, status.HTTP_201_CREATED,
                                  self._test_data.get_universal_json_compare_function(),
                                  self._get_url_func)

        extra_ent = self._test_data.get_test_data("put_obj", status.HTTP_200_OK)
        id_field_name = self._test_data.get_pk_field_name()
        self.method_test_single(self.client.put, extra_ent, status.HTTP_200_OK,
                                lambda ent, resp, i:
                                json_similar(ent, resp, True, self._test_data.detect_fields),
                                create_id_url(self._url, extra_ent[id_field_name], id_field_name))

    def put_fail_replace_present(self):
        self.create_post_data()
        created_data = self._test_data.get_test_data("post", status.HTTP_201_CREATED)

        if created_data is None:
            return

        id_field_name = self._test_data.get_pk_field_name()
        for code in range(400, 600):
            data = self._test_data.get_test_data("put", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.put, data, code,
                                      url_get=lambda index, ent:
                                      create_id_url(self._url, ent[id_field_name]
                                                    if id_field_name in ent
                                                    else (index % len(created_data)) + 1,
                                                    id_field_name))

    def test_core_functionality_put_ok(self):
        self.put_ok_create_new()
        self.put_ok_replace_present()

    def test_core_functionality_put_fail(self):
        self.put_fail_replace_present()

    def check_cascades(self):
        return

    def test_core_functionality_delete_ok(self):
        self.create_post_data()

        created_data = self._test_data.get_test_data("post", status.HTTP_201_CREATED)
        id_field_name = self._test_data.get_pk_field_name()
        self.method_test_data_src(self.client.delete, created_data, status.HTTP_204_NO_CONTENT,
                                  url_get=lambda index, ent:
                                  create_id_url(self._url, ent[id_field_name]
                                                if id_field_name in ent
                                                else (index % len(created_data)) + 1,
                                                id_field_name))
        self.check_cascades()

    def test_core_functionality_delete_fail(self):
        self._shared_suite.test_core_functionality_delete_fail(self._test_data.get_pk_field_name(),
                                                               first_nf_id=1, incorrect_id=-11)
