from assets import _functions
import sys


# runs all insert functions inside _functions
def main():
    for func in _functions:
        if not func():
            sys.exit(66)
    sys.exit(0)


if __name__ == "__main__":
    main()
