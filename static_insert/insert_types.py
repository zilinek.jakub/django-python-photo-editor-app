import traceback

from assets import _types
from api_core.models import *



def type_insert():
    _created = 0
    for types in _types:
        try:
            if not ParameterType.objects.filter(name=types).exists():
                _type = ParameterType(name=types)
                _type.save()
                _created += 1
        except Exception as e:
            print("Price insert failed!")
            print(traceback.format_exc())
            return False
    print("Type insert success.\t\t Created ", _created)
    return True
