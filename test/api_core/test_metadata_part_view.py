from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_metadata import MetadataTestData, PROCESSED_PHOTO_DEPENDENCY
from test.api_core.test_photo import PhotoTestData
from test.api_core.utility import delete_generated_test_data


class MetadataPartCoreTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        photo_test_data = PhotoTestData()
        for _ in range(0, 2):
            # Seek happens inside the function
            photo_data = photo_test_data.get_test_data("post", status.HTTP_201_CREATED)
            self.create_dependency_data(reverse("photo"), photo_data, form="multipart")

        self.create_dependency_data(reverse("processed"), PROCESSED_PHOTO_DEPENDENCY)
        self.create_dependency_data(reverse("metadata"), MetadataTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse("metadata_part")

    def get_test_data(self) -> TestData:
        return MetadataPartTestData()

    @classmethod
    def tearDownClass(cls):
        delete_generated_test_data()


class MetadataPartTestData(TestData):
    IRRELEVANT_KEY = "fsdjkhgskjhgkshfk"

    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422,
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    VALID_DATA = [
        {
            "desc": "some serious metadata",
            "name": "edit description",
            "fk_metadata": 1
        }, {
            "desc": "M. E.",
            "name": "authored by",
            "fk_metadata": 1
        }, {
            "desc": "1 min 2s",
            "name": "time taken",
            "fk_metadata": 1
        }, {
            "desc": "1 min 2s",
            "name": "time taken",
            "fk_metadata": 2,
            IRRELEVANT_KEY: "Amogus"
        }
    ]

    INVALID_DATA_400 = [
        {
            "processed": 1
        }, {}, "", {
            "desc": "1 min 2s",
            "name": "time taken",
        }, {
            "desc": "1 min 2s",
            "fk_metadata": 2
        }, {
            "desc": "1 min 2s"
        }
    ]

    INVALID_DATA_422 = [
        {
            "desc": "some serious metadata",
            "name": "edit description",
            "fk_metadata": 15
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "desc": "1s",
            "name": "time taken",
            "fk_metadata": 3,
        }, {
            "id": 2,
            "desc": "oh yeh",
            "name": "idk bruv",
            "fk_metadata": 1,
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "processed": 1
        }, {}, "", {
            "processed_photo": "nope"
        }, {
            "id": 1,
            "desc": "1 min 2s",
            "name": "time taken",
        }, {
            "id": 2,
            "desc": "1 min 2s",
            "fk_metadata": 2
        }, {
            "id": 1,
            "desc": "1 min 2s"
        }
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 2,
            "desc": "some serious metadata",
            "name": "edit description",
            "fk_metadata": 21
        }
    ]

