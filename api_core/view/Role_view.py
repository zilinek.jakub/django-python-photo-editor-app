from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView


class Role_view(APIView):
    single = openapi.Response(description="response",
                              schema=RoleSerializer())
    res_quary = {404 : 'object for this slug wasn\'t found',
                 400 : '?id in quary wasn\'t specified or name is NOT unique '
                       'or request has bad format',
                 200 : openapi.Response(description="response",
                                        schema=RoleSerializer())}
    res_many = {400 : 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=RoleSerializer(many=True))}

    def find_by_id(self, _id):
        try:
            return Role.objects.get(pk=_id)
        except Role.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Role"),
                         operation_description=doc_desc_get("Role"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("role")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, RoleSerializer)
        roles = Role.objects.all()
        serializer = RoleSerializer(roles, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Update Role",
                         operation_description=doc_desc_put("Role"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('role')],
                         request_body=RoleSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, RoleSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Role",
                         operation_description=doc_desc_delete("Role"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('role')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Add new Role object",
                         responses={400: 'name is NOT unique or request has bad format',
                                    status.HTTP_201_CREATED : single },
                         request_body=RoleSerializer)
    def post(self, request):
        serializer = RoleSerializer(data=request.data)
        return serializer_validation(serializer)
