from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView


class Token_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=TokenSerializer(many=True))}
    res_quary = {404: 'object for this slug wasn\'t found',
                  400: 'id in quary wasn\'t specified or request has bad format',
                  200: openapi.Response(description="response",
                                        schema=TokenSerializer())}
    single = openapi.Response(description="response",
                              schema=TokenSerializer())

    def find_by_id(self, _id):
        try:
            return Token.objects.get(pk=_id)
        except Token.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Token"),
                         operation_description=doc_desc_get("Token"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("token")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, TokenSerializer)
        token = Token.objects.all()
        serializer = TokenSerializer(token, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Token object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED : single },
                         request_body=TokenSerializer)
    def post(self, request):
        serializer = TokenSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Token",
                         operation_description=doc_desc_put("Token"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('token')],
                         request_body=TokenSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, TokenSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Token",
                         operation_description=doc_desc_delete("Token"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('token')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
