from typing import Any
from unittest import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import TestData, GeneralizedTestSuite
from test.api_core.test_role import RoleTestData
from test.api_core.test_account import AccountTestData


class UserTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse("role"),
                                    RoleTestData().get_test_data("post", status.HTTP_201_CREATED))
        acc = AccountTestData()
        self.create_dependency_data(reverse("account"),
                                    acc.get_test_data("post", status.HTTP_201_CREATED))

    def get_url(self) -> str:
        return reverse("user")

    def get_test_data(self) -> TestData:
        return UserTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("account"), 1,
                                              AccountTestData.get_pk_field_name(),
                                              [UserTestData.VALID_DATA[1]],
                                              self.method_test_single)
        self._shared_suite.check_type_cascade(reverse("account"), 2,
                                              AccountTestData.get_pk_field_name(),
                                              [UserTestData.VALID_DATA[2],
                                               UserTestData.VALID_DATA[3]],
                                              self.method_test_single)

    def test_core_functionality_delete_fail(self):
        self._shared_suite.test_core_functionality_delete_fail(
            UserTestData.get_pk_field_name(),
            15,
            "iota"
        )


class UserTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_POST_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_POST_422,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_404_NOT_FOUND): self.INVALID_DATA_PUT_404,
            ("put", status.HTTP_409_CONFLICT): self.INVALID_DATA_PUT_409,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "dajkshfkjasdhf"

    VALID_DATA = [
        {
            "username": "abs(legend)",
            "name": "Adam Procházka",
            "email": "adam.prochazka@profifoto.cz",
            "password": "123456789",
            "free_trial": False,
            "fk_account": 1,
            "roles": [2]
        }, {
            "username": "novmar7",
            "name": "Marek Novotný",
            "email": "novmar7@photofire.com",
            "password": "251251251oo",
            "free_trial": False,
            "fk_account": 2,
            "roles": [1, 3]
        }, {
            "username": "PanK",
            "name": "Petr Mok",
            "email": "mokp@photofire.com",
            "password": "weeI112-.8",
            "free_trial": False,
            "fk_account": 2,
            "roles": [3]
        }, {
            "username": "Imaroz",
            "name": "Jiří Lebeda",
            "email": "jiri.lebeda@email.cz",
            "password": "as-pa78.ER",
            "free_trial": False,
            "fk_account": None,
            "roles": [2]
        }, {
            "username": "Test min",
            "name": "Jmeno",
            "email": "jmeno.lebeda@email.cz",
            "password": "as-pa78.ER",
            "free_trial": False,
            "roles": []
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "username": "abs",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }
    ]

    INVALID_DATA_POST_400 = [
        {
            "username": "příliš dlouhý username na zpracování",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }, {
            "username": "Hello",
            "name": "Toto je moc dlouhé jméno",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }
    ]

    INVALID_DATA_POST_422 = [
        {
            "username": "abs",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 100,
            "roles": []
        }, {
            "username": "abs",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 1,
            "roles": [100]
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": 1,
            "username": "příliš dlouhý username na zpracování",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }, {
            "id": 1,
            "username": "Hello",
            "name": "Toto je moc dlouhé jméno",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }
    ]

    INVALID_DATA_PUT_409 = [
        {
            "id": 2,
            "username": "abs(legend)",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }, {
            "id": 1,
            "username": "abs",
            "name": "Adam",
            "email": "novmar7@photofire.com",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 2,
            "roles": []
        }
    ]

    INVALID_DATA_PUT_404 = [
        {
            "id": 100,
            "username": "abs",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 1,
            "roles": [1]
        }
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 1,
            "username": "abs",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 100,
            "roles": []
        }, {
            "id": 1,
            "username": "abs",
            "name": "Adam",
            "email": "adam2.prochazka@profifoto.cz",
            "password": "12345678",
            "free_trial": True,
            "fk_account": 1,
            "roles": [100]
        }
    ]


