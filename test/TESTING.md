# Testing and you

At this point you should be familiar how to use tests, if you aren't, please refer to [the readme section](../README.md#testing). Having tests at your disposal is great as it reduces time needed to deploy features. However, these tests first need to be written, and that is why you are here.

## Unit testing  

As prefaced by the initial document, all tests are build atop of unittest module. Although these tests should be the most numerous part of the testing framework, it is not always possible. Currently, plain old unittests are used to test photo editing feature. As for the bussiness logic, at the current stage, is pretty much non-existent, these classes should and will be unit tested when ready, but until then, the integration testing is all we have.

In order for your test cases to run out of the box, you need to make sure they are prefixed with ***test_***. Bellow you can see an example of a unit test.

```python
import unittest
from unittest.case import skip

from src.feature.example1 import BlackBox
from src.feature.example2 import RadixSort


class ExampleOfTestSuite(unittest.TestCase):
    def test_black_box_output(self):
        black_box = BlackBox()
        
        # Assertion 1
        assert(black_box.return_same_bool(True))
        # Assertion 2
        self.assertFalse(black_box.return_same_bool(False))
        # Assertion 3
        self.assertEqual(black_box.get_http_response_code_name(500),
                         "Internal Server Error",
                         "A method does not know the correct name of http response code 500")

    def method(self):
        pass

    @skip("Underlying class is not fully implemented")
    def test_radix_sort_impl(self):
        radix_sort = RadixSort()
        result = radix_sort.sort([1,2,3,8,5,6])
        self.assertEqual(result, [1,2,3,5,6,8], "Radix sort failed to sort correctly")


@skip("Test suite unfinished")
class ExampleOfTestSuite1(unittest.TestCase):
    def test_example2(self):
        pass

    def test_example3(self):
        pass

```
In the code above, first thing you should notice are the three test assertions in the method ***test_black_box_output***. These illustrate the ways to assert correctness of the return statement. From these three ways, the **first assertion is the one not recommended**. It all comes down to the way other developers will see this assertion when it fails. The case #1 would show only the following message:
```
Failure
Traceback (most recent call last):
  File "\Python\Python37\lib\unittest\case.py", line 59, in testPartExecutor
    yield
  File "\Python\Python37\lib\unittest\case.py", line 628, in run
    testMethod()
  File "\test\example_test.py", line 13, in test_black_box_output
    assert(black_box.return_same_bool(True))
AssertionError
```
Although it does have the very useful traceback, it is hard to pinpoint what exactly failed. If you are writing some code and the tests aren't passing, the last thing you want to do is to debug the test code to find where is the problem. Ideally you want to see the expectation vs. response and maybe the arguments passed to said function. That is exactly what the case #2 does, any methods which unittest has for assertion (you can identify these as calls to self because they aren't functions). Now the output will look something like this 
```
Failure
Traceback (most recent call last):
  File "\Python\Python37\lib\unittest\case.py", line 59, in testPartExecutor
    yield
  File "\Python\Python37\lib\unittest\case.py", line 628, in run
    testMethod()
  File "\test\example_test.py", line 15, in test_black_box_output
    self.assertFalse(black_box.return_same_bool(False))
  File "\Python\Python37\lib\unittest\case.py", line 699, in assertFalse
    raise self.failureException(msg)
AssertionError: True is not false
```
Now, would you look at that, we are getting to the crux of things. So this is better, and sometimes, this suffices, but we can do one better. I present to you case #3:
```
Traceback (most recent call last):
  File "\Python\Python37\lib\unittest\case.py", line 59, in testPartExecutor
    yield
  File "\Python\Python37\lib\unittest\case.py", line 628, in run
    testMethod()
  File "\test\example_test.py", line 17, in test_black_box_output
    "A method does not know the correct name of http response code 500")
AssertionError: 'External, Non-client, Error' != 'Internal Server Error'
- External, Non-client, Error
+ Internal Server Error
 : A method does not know the correct name of http response code 500
```
This is as far as we can get, in terms of clarity, of course, we could make it even better with customized message which would give us summary of input arguments, but that would be required only in some more complicated functions.
The test above also showcases how skips are handled. Any test case or test suite annotated as such will be skipped and so will all extending classes, be mindful of that.  
Unittest also includes methods which are run before each test case and before each test suite, these are:  

- setUp
- tearDown  

and class methods:

- setUpClass
- tearDownClass

Method ***setUp*** is run before each test case and ***tearDown*** after each test case. Methods ***setUpClass*** and ***tearDownClass*** are used before respectively after all test cases in a test suite.  

## Integration testing
Now, that you have reminded yourself how to do unit testing, it is time for the real deal. Testing in general requires a lot of repetition, this is done, so we do not have to fix bugs in unittests. Some may even say that if you need a unit test to test your tests, you are doing something wrong. Well, that is all nice and dandy but even in unit tests a use of for cycle can be valuable and decrease unnecessary repetition. In an ideal scenario, all a tester needs to do in a black box testing scenario is to think of all the inputs and appropriate outputs. Adding any other agency may result in code mistakes. Ideally, the only mistakes we want to accept are the ones in the entered data, as fixing these mistakes is trivial and not hardcoded. But let's not get too ahead of ourselves, we are talking ideals, and this is not an ideal situation.

With typical unit tests, there is too much granularity to achieve our ideal. A testing framework which would allow you to specify just I/O data would sooner or later fall short as it would be like killing a fly with a longsword. Integration tests are however a bit different, responses are clearcut, you always know what you have to get, and the server needs to provide you with a response. Such response should be deterministic, and format of this response doesn't change too often. What may also be useful is sending multiple requests in tandem, but once again all these things can be more or less automatized. Without further or do, I present to you - our integration testing framework.

### Humble beginnings
First class you need to be aware of is **CredentialsTestBase** this class extends ApiTestCase from Django's REST testing framework. It provides any child class with following convenient methods:  

```_log_in(username, password)``` Attempts to log user in with specified password (this should create a token)  
```_log_in_token(token)``` Logs user in with a specified token (using a token authentization)
```
method_test_single(method,
                   data,
                   expected_code,
                   additional_check_func = None,
                   url: str = None,
                   form: str = "json")
```
Sends the contents of data using the passed method, expects *expected_code* (for any other response assertion fails)
then any data which might have been received tests against the *additional_check_func*tion.
If *url* is not specified, it uses a class protected attribute *_url*.
Argument *form* specifies which content type is sent to the server.
```
method_test_data_src(method,
                     data,
                     expected_code,
                     additional_check_func = None,
                     url_get = None,
                     form = "json")
```

Similar to the previous method, the key difference being data - now expcted to be iterable
url_get - a function which returns a string based on passed index (the index passed is the same as
the one of the current entity of iterated data), defaults to _url atribute

This class also ensures that protected member _url exists during tests case execution as long as you call super when overriding setUp.

### Shared Test Suite

We are already in some murky water, what we are doing here has inheritance. Most of the time inheritance is not needed, as solutions via composition can be more flexible and make logic less obfuscated. That is why a select portion of these methods is packed in its own class. **SharedTestSuite** provides following methods. the class itself needs to be initialized with a client and a URL:
```
test_core_functionality_get_fail(first_nf_id = 15,
                                 incorrect_id = "iota",
                                 create_data = lambda: None)
```
Sends two ***GET*** requests at the url defined in the constructor.  
It doesn't expect to find an entity with the *first_nd_id* (expects HTTP 404 response)  
It tries to find an entity with malformed id *incorrect_id* (expects HTTP 400 response)  
And expects a function which creates some data.
```
test_core_functionality_get_ok(irrelevant_key,
                               data,
                               create_data,
                               field_comparison = eq,
                               irrelevant_get_field = "airjgsi=fshfiuwg")
```
Sends a ***GET*** request at the url, then validates each response against data argument.  
Argument *create_data* is a function which should be ran before the get is attempted.  
Argument *field comparison* is a function which will run for each response key value pair.  
It also sends *irrelevant_get_field* as a part of the get request to see if it is properly ignored.

```
test_core_functionality_delete_fail(first_nf_id = 15,
                                    incorrect_id = "iota",
                                    create_data = lambda: None)
```
Sends two ***DELETE*** requests at the url defined in the constructor.  
It doesn't expect to find an entity with the *first_nd_id* (expects HTTP 404 response)  
It tries to find an entity with malformed id *incorrect_id* (expects HTTP 400 response)  
And expects a function which creates some data.

```
check_type_cascade(dependency_url,
                   dependency_pk,
                   dependency_pk_name,
                   cascaded_entries,
                   delete_single_method)
```
Sends a ***DELETE*** request (uses *delete_single_method*) at the {*dependency_url*}?*{dependency_pk_name*}={*dependency_pk*}.  
Then sends a ***GET*** request at the protected *_url* and check whether there are any *cascaded_entries* present, assertion fails any of these entities is found. 

### Generalizing

So, we have defined some often used methods, but we still haven't touched on the meat, **GeneralizedTestSuite** provides us with test case for HTTP CRUD methods (except for patch, we are not using it in any view, but adding it would be simple enough). Each method has two cases associated, one which should succeed with a code from the 2xx family, and the other which expect failures. This is done by going through all response codes from 400-600 and getting appropriate test data from the defined test class.

Now rather than showing you the code of GeneralizedTestSuite (if you are really interested in the code you can click [here](./api_core/generalized_test.py)) I will rather show you which methods you may want to override in your tests.  
```get_url(self) -> str``` Returns an endpoint which the test should send its requests to. Body of this method usually contains a call to django's reverse method. **You have to override this method**  
```get_test_data(self) -> TestData``` Returns a companion class which returns all the required test data. **You have to override this method**  
```setup_dependency(self)``` This method gets called inside the overriden *setUp* method and allows you to set up any entities which your tests may require at the endpoint. **You have to override this method**, even if you are not using any dependency (this is done to explicitly to tell anyone that this method exists). To create said dependency data, you may use the following method:
```
create_dependency_data(url,
                       data,
                       additional_check_func = None,
                       form="json")
```
Sends a post at the provided *url* containing data where data provided in *form* format. You may choose to validate the response using the *additional_check_func* but this option is unused by default, as most of the time, this is a job of other test classes.  
```check_cascades(self)``` This method allows you to check whether the data correctly cascaded after an outside delete operation (this method is called inside the success delete-test case). To test each dependency cascade use method *check_type_cascade* mentioned above.

#### Single post test case

There is a special single post scenario, this test case checks whether the posted data is in accordance with the data received. Then, it calls to get to check whether only one entry is present and matches data sent, and finally tries to get the entry directly to verify that the match is consistent even through direct fetch. To test whether the data sent is the data received this case uses ```std_check(ent, resp, cmp=None)```. Argument *ent* is the expectation, *resp* the response and the cmp is a comparison function which check equality for each key-value pair. This method calls function *json_similar* in the *test/utility.py*, but what separates it from the utility function is that it is strict by default - meaning if the data received is not exact it will raise an assertion error. You can change this behaviour by an overriding this method.  

#### TestData interface 

As mentioned in one of the previous chapters we want to separate logic from data, this makes our code more readable and ideally allows us to write less potentially buggy code. That is why each integration test has a companion TestData class (TestData being the prefix and an interface). This interface requires you to override the following methods:  
```
get_test_data(self, method: str, status_code: int) -> Any:
```
Returns data or None for a given pair of (["post","put","put_obj"], *status_code*). Semantics being that this is the data which should return the specified *status_code*. 
```
@classmethod
get_irrelevant_key(cls) -> str:
```
This method returns a key which is for all intents and purposes irrelevant, it is used to check that a field with unimportant data iis correctly ignored.
```
@classmethod
get_pk_field_name(cls) -> str:
```
This method returns the name of the primary key of the method, normally the primary key name is **id**, however some entities use a string as a primary key. In such instances the appropriate primary key is not always names id, when such a situation occurs, this method should be overridden.
```
@classmethod
detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
```
When a json entity is checked against its origin, this method is evaluated for each corresponding field. Argument *name* gets passed the key of the json dictionary, arg1 and arg2 are the values of the two entities. Most of the time, checking for equality is what we want, but for example when dealing with dates or any other special situations, this quality won't do, in these cases override this method.  
```
@classmethod
get_universal_json_compare_function(cls) -> Optional[Callable[[str, Any, Any], bool]]:
    return json_similar_ignore_compare(cls.get_irrelevant_key(), cls.detect_fields)
```
This method is in its entire form (aside of the def), override this method when you do want to be strict or when you do not want to do any checking of the response data.

To finish this chapter, an example of a written test case would be appropriate, however I have something better [a link to an example](./api_core/test_account.py). Note that in this example test suite itself is just few lines, the main part is the test data.

### What if generalized test is not applicable?

Sure, sometimes you may want to do something extra, test a method no one has ever used which is special to one specific view only. This is another point where our framework differs from the ideal, our data is still hardcoded, but it allows us to create test cases which are different from the presets. All you need to do is to override or skip non-applicable tests. See bellow how these tests can look like:
```python
class RegisterTest(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse("register")

    def get_test_data(self) -> TestData:
        return RegisterTestData()

    def test_core_functionality_single_post_ok(self):
        data_to_create = self.get_test_data().get_test_data("post", status.HTTP_201_CREATED)[0]
        self.method_test_single(self.client.post, data_to_create, status.HTTP_201_CREATED,
                                self.std_check)

    @skip("Not used in view")
    def test_core_functionality_put_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_put_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_get_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_get_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_ok(self):
        pass
```