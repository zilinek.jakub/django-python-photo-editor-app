"""
@author Abi
api URL Configuration

The `urlpatterns` list routes URLs to view. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function view
    1. Add an import:  from my_app import view
    2. Add a URL to urlpatterns:  path('', view.home, name='home')
Class-based view
    1. Add an import:  from other_app.view import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))

"""
from django.conf import settings
from django.contrib import admin

from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from api_core.view import *
from django.conf.urls import url
from django.conf.urls.static import static
from api_core.view import *
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
   openapi.Info(
      title="Mediagraphix API Documentation v1",
      default_version='v1',
      description="Documentation of project https://gitlab.fit.cvut.cz/zilinjak/bi-si_mediagraphix",
      terms_of_service="https://gitlab.fit.cvut.cz/zilinjak/bi-si_mediagraphix",
      contact=openapi.Contact(email="zilinjak@fit.cvut.cz"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    #Docs
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    # test

    #FE requests
    path('user/register/', RegisterView.as_view(), name='register'),
    path('user/login/', LoginView.as_view(), name='login'),
    path('user/dashboard/', DashboardView.as_view(), name='user-dash'),
    path('user/table/', TableView.as_view(), name='user-table'),

    #URLS
    path('admin/', admin.site.urls, name='admin'),

    path('account/', AccountView.as_view(), name='account'),  # ?id=1 or without param

    path('user/', User_view.as_view(), name='user'),  # ?id=1 || ?username=Admin || without param
    path('user/requests/', User_requests.as_view(), name='user-request'),
    path('user/fk_account/', User_fk.as_view()),

    path('role/', Role_view.as_view(), name='role'),  # ?id=1 or without param

    path('request/', Request_view.as_view(), name='request'),  # ?id=1 or without param
    path('new_request/', New_Request_view.as_view(), name='new_request'),

    # ?id=1 or without param
    path('token/', Token_view.as_view(), name='token'),

    #TODO make an quary expression
    path('ban_edit/', Banned_edit_view.as_view(), name='ban_edit'),

    # ?id=1 or without param
    path('flat_rate_type/', Flat_rate_type_view.as_view(), name="FR_type"),

    # ?id=1 or ?fk_user=1 or without param
    path('flat_rate/', Flat_rate_view.as_view(), name="FR"),

    # ?id=1 or without param
    path('photo/', Photo_view.as_view(), name="photo"),

    # ?id=1 or ?fk_user=1 or without param TODO FIX
    # path('request/', Request_view.as_view(), name="request"),

    # ?id=1 or ?fk_user=1 or without param
    path('output/', Output_view.as_view(), name="output"),

    # ?id=1 or  without param
    path('original_photo/', OriginalPhotoView.as_view(), name="original"),

    # ?id=1 or  without param
    path('processed_photo/', Processed_photo_view.as_view(), name="processed"),

    # ?id=1 or  without param
    path('metadata/', Metadata_view.as_view(), name="metadata"),

    # ?id=1 or ?fk_metadata=1 or without param
    path('metadata_part/', Metadata_part_view.as_view(), name="metadata_part"),

    # ?id=1 or ?fk_req=1 or without param
    path('edit_configuration/', Edit_configuration_view.as_view(), name="edit_conf"),

    path('semiprocessed_photo/', Semiprocessed_photo_view.as_view(), name="semiprocessed_photo"), #?fk_photo=1
    path('semiprocessed_photo/_photo_path/<str:_photo_path>/', Semiprocessed_photo_details.as_view(),
         name="semiprocessed_photo_detail"),


    # ?id=1 or  without param
    path('param_type/', ParameterTypeView.as_view(), name="type_param"),

    # ?id=1 or  without param
    path('edit/', EditView.as_view(), name="edit"),

    # ?id=1 or  without param
    path('price/', Price_view.as_view(), name="price"),

    # ?id=1 or  without param
    path('def_param/', DefinedParameterView.as_view(), name="def_param"),

    # ?id=1 or  without param
    path('param_values/', Param_values_view.as_view(), name="values"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += staticfiles_urlpatterns()
