from api_core.storage.storage_type.LocalStorage import LocalStorage


class StorageFactory:
    @classmethod
    def get_default_storage(cls):
        return LocalStorage()
