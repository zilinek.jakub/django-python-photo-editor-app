from unittest.case import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_account import AccountTestData
from test.api_core.test_role import RoleTestData
from test.api_core.test_user import UserTestData
from test.api_core.utility import *


def acc_detect_fields(name: str, val1: Any, val2: Any) -> bool:
    d = {"expiration": date_str_eq,
         "credit": int_rep,
         "id": int_rep}
    if name not in d:
        return val1 == val2
    return d[name](val1, val2)


class LoginTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse("account"), AccountTestData.VALID_DATA)

        self.create_dependency_data(reverse("role"), RoleTestData.VALID_DATA)
        self.create_dependency_data(reverse("user"), UserTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse("login")

    def get_test_data(self) -> TestData:
        return LoginTestData()

    @staticmethod
    def __check_free_trial_bool(val1: Any, val2: Any) -> bool:
        if val1.lower() == "true":
            return val2
        if val1.lower() == "false":
            return not val2
        raise Exception("This should not happen. This means source data is not 'true'"
                        "nor 'false' string (case insensitive)")

    def test_core_functionality_single_post_ok(self):
        data_to_create = self.get_test_data().get_test_data("post", status.HTTP_200_OK)[0]
        self.method_test_single(self.client.post, data_to_create, status.HTTP_200_OK)

    @skip("Not used in view")
    def test_core_functionality_put_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_put_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_get_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_get_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_ok(self):
        pass


class LoginTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_200_OK): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_401_UNAUTHORIZED): self.INVALID_DATA_401
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    # No fields should be present in response
    @classmethod
    def get_universal_json_compare_function(cls) -> Optional[Callable[[str, Any, Any], bool]]:
        return None

    IRRELEVANT_KEY = "adklsjdfkl;dsajfl"

    VALID_DATA = [
        {
            "username": "abs(legend)",
            "password": "123456789"
        }, {
            "username": "novmar7",
            "password": "251251251oo"
        }, {
            "username": "PanK",
            "password": "weeI112-.8"
        }, {
            "username": "Imaroz",
            "password": "as-pa78.ER"
        }
    ]

    INVALID_DATA_400 = [
        {
            "password": "$2y$13$FBSgS2wjHnXLY3xJRHqnM.28HHlQq6Ayhod/IZ3LTEX3WaU/wOBMS"
        }, {
            "username": "Linus"
        }, {}, "", {
            IRRELEVANT_KEY: "dffda"
        }
    ]

    # 404 Should never be returned for non existing users as it is a severe security vulnerability
    INVALID_DATA_401 = [
        {
            "username": "Tomato",
            "password": "$2y$13$FBSgS2wjHnXLY3xJRHqnM.28HHlQq6Ayhod/IZ3LTEX3WaU/wOBMS"
        }, {
            "username": "Linus",
            "password": "$2y$13$Po/pWh0e9IUpCXoOyeFCy.5pTwrTJin0ZxumosKB31S6sZ02pOQ.."
        }, {
            "username": "",
            "password": "$2y$13$FBSgS2wjHnXLY3xJRHqnM.28HHlQq6Ayhod/IZ3LTEX3WaU/wOBMS"
        }, {
            "username": "Tomato",
            "password": ""
        }, {
            "username": "",
            "password": ""
        }
    ]
