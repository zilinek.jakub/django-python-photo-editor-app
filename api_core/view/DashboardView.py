from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView
from ..serializers import *


class DashboardView(APIView):
    def find_by_username(self, _user):
        try:
            return User.objects.get(username=_user)
        except User.DoesNotExist:
            return []

    def find_by_account_id(self, _id):
        try:
            return Account.objects.get(pk=_id)
        except Account.DoesNotExist:
            return []

    responce= {'userInfo': {
"username": "string",
"name": "string",
"surname": "string",
"email": "string",
"creditCard": "number",
"billingAddress": "string",
"companyName": "string",
},"userAccount": {
    "payAccount": "string",
    "numberOfConnectedUsers": "number",
},"userPhotos":{
"photoEdited": "number",
    "numOfPhotosLastEdit": "number"
},
                "userStorage": {
    "numberOfConnectedSources": "number",
    "lastConnectedSource": "string"
},"userPlan": {
    "planName": "string",
    "planExpiration": "date"
}}
    req = { "username": "string", "token": "string"}


    @swagger_auto_schema(operation_summary="Get user dashboard",
                         operation_description="TO be done in future",
                         responses={400: ' request has bad format',
                                    status.HTTP_200_OK: openapi.Schema(type=openapi.TYPE_OBJECT,
                                                                       properties=responce)},
                         request_body=openapi.Schema(type=openapi.TYPE_OBJECT,
                                                     required=["username", "numOfItemsOnPage", "page"],
                                                     properties=req)
                         )
    def post(self, request):
        ##TODO We should verify token...
        dat = request.data
        user = self.find_by_username(dat['username'])
        if not user:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        account = self.find_by_account_id(user.fk_account)
        if not account:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = UserSerializer(user)
        return Response(serializer.data)
