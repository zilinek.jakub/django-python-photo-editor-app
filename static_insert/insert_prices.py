import traceback
from api_core.models import *
from assets import _prices


def insert_prices():
    _created = 0
    for price in _prices:
        try:
            if not Price.objects.filter(edit=price[0]).exists():
                _price = Price(edit=Edit(price[0]),
                               price=price[1])
                _price.save()
                _created += 1
        except Exception as e:
            print("Price insert failed!")
            print(traceback.format_exc())
            return False
    print("Price insert success.\t\t Created ", _created)
    return True
