from .Account_view import *
from .Defined_parameter_view import *
from .Edit_configuration_view import *
from .Edit_view import *
from .Flat_rate_type_view import *
from .Flat_rate_view import *
from .Metadata_part_view import *
from .Metadata_view import *
from .Original_photo_view import *
from .Output_view import *
from .Param_type_view import *
from .Param_values_view import *
from .Photo_view import *
from .Price_view import *
from .Processed_photo_view import *
from .Request_view import *
from .Role_view import *
from .Semiprocessed_photo_view import *
from .Token_view import *
from .User_view import *
from .Banned_edit import *
from .Register_view import *
from .DashboardView import *
from .LoginView import *
from .TableView import *
from .New_Request_view import *
from ..models import *
