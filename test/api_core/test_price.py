from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_edit import EditTestData


class PriceTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse("edit"), EditTestData.VALID_DATA)

    def get_url(self) -> str:
        return reverse("price")

    def get_test_data(self) -> TestData:
        return PriceTestData()

    def check_cascades(self):
        self.create_post_data()
        self._shared_suite.check_type_cascade(reverse("edit"), "Reformat",
                                              EditTestData.get_pk_field_name(),
                                              [PriceTestData.VALID_DATA[0]],
                                              self.method_test_single)
        self._shared_suite.check_type_cascade(reverse("edit"), "Watermark",
                                              EditTestData.get_pk_field_name(),
                                              [PriceTestData.VALID_DATA[1]],
                                              self.method_test_single)


class PriceTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("post", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_422,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_409,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_409_CONFLICT): self.INVALID_DATA_PUT_409,
            ("put", status.HTTP_422_UNPROCESSABLE_ENTITY): self.INVALID_DATA_PUT_422
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "sdfkjasfgvcxkjnv"

    VALID_DATA = [
        {
            "price": 100,
            "edit": "Reformat"
        }, {
            "price": 200,
            "edit": "Watermark",
            IRRELEVANT_KEY: "This is not the key you are looking for"
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "price": 150,
            "edit": "Reformat"
        }, {
            "id": 2,
            "price": 200,
            "edit": "Resize",
            IRRELEVANT_KEY: "I'm not here"
        }
    ]

    INVALID_DATA_409 = [
        {
            "price": 112,
            "edit": "Reformat"
        }, {
            "price": 520,
            "edit": "Watermark"
        }
    ]

    INVALID_DATA_400 = [
        {
            "price": -100,
            "edit": "Invert"
        }, {
            "price": 100,
        }, {
            "edit": "Invert"
        }, {}, {
            "price": "toto není int",
            "edit": "AllIn1"
        }
    ]

    INVALID_DATA_422 = [
        {
            "price": 100,
            "edit": "100"
        }, {
            "price": 100,
            "edit": 1
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": 1,
            "price": -100,
            "edit": "Reformat"
        }, {
            "id": "",
            "price": 100,
            "edit": "Reformat"
        }, {
            "id": 1,
            "price": 100
        }, {
            "id": 1,
            "edit": "Reformat"
        }, {
            "id": 2
        }, {
            "id": 1,
            "price": "you",
            "edit": "Reformat"
        }
    ]

    INVALID_DATA_PUT_409 = [
        {
            "id": 1,
            "price": 186,
            "edit": "Watermark"
        }, {
            "id": 2,
            "price": 112,
            "edit": "Reformat"
        }
    ]

    INVALID_DATA_PUT_422 = [
        {
            "id": 2,
            "price": 200,
            "edit": "100"
        }
    ]
