# inherits from Edit, adds watermark to photo
"""
    @file watermark.py
    @author Olivie A. Franklova
    @date 22.4.2021
"""
import math

from .edit import Edit
from .resize import Resize

"""
@brief This class represents edit which add watermark to photo.
"""


class Watermark(Edit):
    """
    @brief This is constructor for class Watermark.

    @param width has type int
    @param height has type int
    @param watermark has type Image
    @param x_coordinate in upper left has type int
    @param y_coordinate in upper left has type int
    @param transparency has type int is % of transparency
    """

    def __init__(self, watermark, x_coordinate, y_coordinate, width, height, transparency=0):
        self.width = width
        self.height = height
        self._x_middle = False
        self._y_middle = False
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.watermark = watermark
        self.transparency = transparency

    @property
    def transparency(self):
        return self._transparency

    @transparency.setter
    def transparency(self, transparency):
        self._transparency = self._recount(transparency)

    @property
    def watermark(self):
        return self._watermark

    @watermark.setter
    def watermark(self, watermark):
        self._watermark = watermark

    @property
    def y_coordinate(self):
        return self._y_coordinate

    @y_coordinate.setter
    def y_coordinate(self, y_coordinate):
        if y_coordinate == 'middle':
            self._y_middle = True
        elif y_coordinate < 0:
            raise ValueError("Width below 0 is not possible.")
        self._y_coordinate = y_coordinate

    @property
    def x_coordinate(self):
        return self._x_coordinate

    @x_coordinate.setter
    def x_coordinate(self, x_coordinate):
        if x_coordinate == 'middle':
            self._x_middle = True
        elif x_coordinate < 0:
            raise ValueError("Width below 0 is not possible.")
        self._x_coordinate = x_coordinate

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, width):
        if width < 0:
            raise ValueError("Width below 0 is not possible.")
        self._width = width

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, height):
        if height < 0:
            raise ValueError("Height below 0 is not possible.")
        self._height = height

    def _recount(self, value):
        return int(math.ceil(2.55 * (100 - value)))

    # 0 = 100% transparency 255 = 0%

    def count_middle(self, shape):
        h, w = shape
        return int((w - self.width) / 2), int((h - self.height) / 2)

    def check_size(self, shape):
        h, w = shape
        if self._width > w:
            self.width = w
        if self._height > h:
            self.height = h

    """
    @brief This paste watermark to photo.

    At first we resize watermark image and change transparency.
    We make copy of photo. We paste watermark.
    @param photo has type Image
    @return image with watermark
    """

    def execute(self, photo):
        if self._transparency == 0:
            return photo
        self.check_size(photo.size)
        res = Resize(self.width, self.height)
        self._watermark = res.execute(self._watermark)
        mask = self._watermark.copy()
        mask.putalpha(self._transparency)
        self._watermark.paste(mask, self._watermark)
        if self._x_middle:
            self._x_coordinate = self.count_middle(photo.size)[0]
        if self._y_middle:
            self._y_coordinate = self.count_middle(photo.size)[1]
        back_im = photo.copy()
        back_im.paste(self._watermark, (self.x_coordinate, self.y_coordinate), self._watermark)

        return back_im

    """
        @brief Paste watermark to photo and save it.

        @param photo has type image.
        @param name is old name of photo, has type string.
        @param m_format is format
        @return image with watermark
        """

    def execute_and_save(self, photo, name, m_format):
        new_photo = self.execute(photo)
        name = name + "_watermark" + m_format
        new_photo.save(name)
        return new_photo
