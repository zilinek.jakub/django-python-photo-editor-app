from rest_framework.serializers import ValidationError


class EndDateValidator:
    def __init__(self, start_date_field):
        self.start_date_field = start_date_field
        self.serializer_field = None

    def set_context(self, serializer_field):
        self.serializer_field = serializer_field

    def __call__(self, value):
        end_date = value
        serializer = self.serializer_field.parent
        if self.start_date_field not in serializer.initial_data:
            return
        raw_start_date = serializer.initial_data[self.start_date_field]

        try:
            start_date = serializer.fields[self.start_date_field].run_validation(raw_start_date)
        except ValidationError:
            return  # if start_date is incorrect we will omit validating range

        if start_date and end_date and end_date < start_date:
            raise ValidationError('{} cannot be less than {}'.format(
                self.serializer_field.field_name, self.start_date_field), code='logic_error')


def validate_positive(value):
    if value >= 0:
        return
    raise ValidationError("This field can not be a negative number", code="invalid")
