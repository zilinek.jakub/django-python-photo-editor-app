# @author Simon
from django.db import models
from django.utils import timezone


class Account(models.Model):
    credit = models.IntegerField()
    expiration = models.DateTimeField()

    def __str__(self):
        field_values = []
        for field in self._meta.get_fields():
            field_values.append(str(getattr(self, field.name, '')))
        return ', '.join(field_values)


class Role(models.Model):
    name = models.CharField(max_length=20, unique=True)
    type = models.CharField(max_length=20)

    def __str__(self):
        field_values = []
        for field in self._meta.get_fields():
            field_values.append(str(getattr(self, field.name, '')))
        return ', '.join(field_values)


class User(models.Model):
    username = models.CharField(max_length=20, unique=True)
    name = models.CharField(max_length=20)
    email = models.EmailField(unique=True)
    password = models.TextField()  # MUST use hash function to store hashed passwords here
    birth_date = models.DateTimeField(null=True, blank=True)
    free_trial = models.BooleanField()
    fk_account = models.ForeignKey(Account, on_delete=models.CASCADE, null=True)
    roles = models.ManyToManyField(Role, blank=True)

    def __str__(self):
        field_values = []
        for field in self._meta.get_fields():
            field_values.append(str(getattr(self, field.name, '')))
        return ', '.join(field_values)


class Token(models.Model):
    active = models.BooleanField()
    hash = models.TextField()
    fk_user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class BannedEdit(models.Model):
    edit_name = models.CharField(max_length=20)

    def __str__(self):
        return self.edit_name


class FlatRateType(models.Model):
    daily_limit = models.PositiveIntegerField(null=True, blank=True)
    name = models.CharField(max_length=20)
    price = models.PositiveIntegerField()
    banned_edits = models.ManyToManyField(BannedEdit, blank=True)

    def __str__(self):
        return self.name


class FlatRate(models.Model):
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField()
    fk_user = models.ForeignKey(User, on_delete=models.CASCADE)
    fk_flat_rate_type = models.ForeignKey(FlatRateType, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class Photo(models.Model):
    def __str__(self):
        return str(self.id)


class OriginalPhoto(models.Model):
    photo_path = models.TextField(unique=True)
    photo = models.OneToOneField(Photo, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.photo_path


class ProcessedPhoto(models.Model):
    photo_path = models.TextField()
    photo = models.OneToOneField(Photo, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return self.photo_path


class Metadata(models.Model):
    processed_photo = models.OneToOneField(
        ProcessedPhoto,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.id)


class MetadataPart(models.Model):
    desc = models.TextField()
    name = models.CharField(max_length=30)
    fk_metadata = models.ForeignKey(Metadata, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ParameterType(models.Model):
    name = models.CharField(max_length=30, primary_key=True)

    def __str__(self):
        return self.name


class Edit(models.Model):
    name = models.CharField(max_length=30, primary_key=True)

    def __str__(self):
        return self.name


class Price(models.Model):
    price = models.IntegerField()
    edit = models.OneToOneField(Edit, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.price) + " : " + self.edit.name


class DefinedParameter(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    fk_parameter_type = models.ForeignKey(ParameterType, on_delete=models.CASCADE)
    fk_edit = models.ManyToManyField(Edit)

    def __str__(self):
        return self.name + " " + self.fk_parameter_type.name + " " + self.fk_edit.name


class ParameterValue(models.Model):
    value = models.TextField()
    fk_defined_parameter = models.ForeignKey(DefinedParameter, on_delete=models.CASCADE)

    def __str__(self):
        return self.value


class EditConfiguration(models.Model):
    parameter_values = models.ManyToManyField(ParameterValue, blank=False)

    def __str__(self):
        return str(self.id)


class Request(models.Model):
    session_id = models.IntegerField(serialize=True)
    datetime = models.DateTimeField(default=timezone.now)
    price = models.IntegerField()
    state = models.CharField(max_length=20)
    fkeditconf = models.ForeignKey(EditConfiguration, null=False, blank=False, on_delete=models.CASCADE)
    fkuser = models.ForeignKey(User, on_delete=models.CASCADE)
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class Output(models.Model):
    datetime = models.DateTimeField(default=timezone.now)
    fk_user = models.ForeignKey(User, on_delete=models.CASCADE)
    request = models.OneToOneField(Request, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class SemiprocessedPhoto(models.Model):
    photo_path = models.TextField(primary_key=True)
    latest = models.DateTimeField(default=timezone.now)  # not sure if correct attribute type
    fk_photo = models.ForeignKey(Photo, on_delete=models.CASCADE)
    edit_configuration = models.OneToOneField(EditConfiguration, on_delete=models.CASCADE)

    def __str__(self):
        return self.photo_path
