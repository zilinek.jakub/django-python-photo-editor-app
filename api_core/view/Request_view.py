from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView


class Request_view(APIView):
    res_many = {400 : 'id is NOT numeric or'
                      'or both id and fk_user is present'
                      ' or request has bad format',
                404 : 'object for this slug wasn\'t found',
                200 : openapi.Response(
                    description="response for get without id",
                    schema=RequestSerializer(many=True))}
    single = openapi.Response(description="response",
                              schema=RequestSerializer())

    def find_by_fk_user(self, _fk):
        request = Request.objects.all()
        res = []
        for i in request:
            tmp = i.fk_UserCustom_id
            if tmp == int(_fk):
                res.append(i)
        return res

    def find_by_id(self, _id):
        try:
            return Request.objects.get(pk=_id)
        except Request.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Request"),
                         operation_description="Return single Request object if `id` or `fk_user` in "
                                               "query is present or return all Request objects",
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("request"),
                                            Parameter('fk_user', openapi.IN_QUERY,
                                                      'show only request with this fk_user `request?fk_user=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def get(self, request):
        return get_Id_or_User(request, RequestSerializer, self.find_by_id, self.find_by_fk_user, Request)

    @swagger_auto_schema(operation_summary="Add new Request object",
                         responses={400 : 'request has bad format or'
                                          ' fk_editconf or fk_user doesnt exist',
                                    status.HTTP_201_CREATED : single},
                         request_body=RequestSerializer)
    def post(self, request):
        serializer = RequestSerializer(data=request.data)
        return serializer_validation(serializer)

    @swagger_auto_schema(operation_summary="Update Request",
                         operation_description=doc_desc_put("Request"),
                         responses={400 : 'request has bad format or'
                                          ' fk_editconf or fk_user doesnt exist'
                                          ' or id is NOT present',
                                    status.HTTP_201_CREATED : single},
                         manual_parameters=[doc_parm_id('request')],
                         request_body=RequestSerializer
                         )
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, RequestSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Request",
                         operation_description=doc_desc_delete("Request")+" or if `fk_user` in query is present",
                         responses={
                                    400: 'id or user in quary wasnt specified',
                                    404: 'object for this slug wasn\'t found',
                                    204: 'successfully deleted specified object'},
                         manual_parameters=[doc_parm_id_get('request'),
                                            Parameter('fk_user', openapi.IN_QUERY,
                                                      'delete Request with this fk_user'
                                                      ' `request?fk_user=2`',
                                                      type=openapi.TYPE_NUMBER, required=False)
                                            ])
    def delete(self, request):
        return delete_id_or_user(request, self.find_by_id, self.find_by_fk_user)
