from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import TestData, GeneralizedTestSuite
from test.api_core.utility import format_assert_code_mismatch_help, create_id_url


class ParameterTypeTest(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse('type_param')

    def get_test_data(self) -> TestData:
        return ParameterTypeTestData()

    def test_core_functionality_delete_fail(self):
        self._shared_suite.test_core_functionality_delete_fail(
            ParameterTypeTestData.get_pk_field_name(),
            "bubble",
            ""
        )

    def test_core_functionality_get_fail(self):
        response = self.client.delete(create_id_url(self._url,
                                                    "widthv2",
                                                    ParameterTypeTestData.get_pk_field_name()))
        self.assertEqual(response.status_code,
                         status.HTTP_404_NOT_FOUND,
                         format_assert_code_mismatch_help({"primary_key": "widthv2"},
                                                          response.status_code,
                                                          status.HTTP_404_NOT_FOUND,
                                                          response.content))

    def __create_url_func(self, created_data, id_field_name):
        def url_func(index, _):
            index_mod = index % len(created_data)
            _ent = created_data[index_mod]
            return create_id_url(self._url, _ent[id_field_name]
                                 if id_field_name in _ent else index_mod,
                                 id_field_name)
        return url_func

    def __put_ranges(self, _from, _to):
        self.create_post_data()
        created_data = self._test_data.get_test_data("post", status.HTTP_201_CREATED)

        id_field_name = self._test_data.get_pk_field_name()
        for code in range(_from, _to):
            data = self._test_data.get_test_data("put", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.put, data, code,
                                      url_get=self.__create_url_func(created_data, id_field_name))

    def put_ok_replace_present(self):
        self.__put_ranges(200, 400)

    def put_fail_replace_present(self):
        self.__put_ranges(400, 600)


class ParameterTypeTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        d = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_400,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
        }

        if (method, status_code) not in d:
            return None
        return d[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def get_pk_field_name(cls) -> str:
        return "name"

    IRRELEVANT_KEY = "aksjhfajklshf"

    VALID_DATA = [
        {
            "name": "int"
        }, {
            "name": "string",
            IRRELEVANT_KEY: "test"
        }, {
            "name": "bool",
        }, {
            "name": "float",
        }, {
            "name": "double"
        }
    ]

    VALID_DATA_PUT = [
        {
            "name": "decimal"
        }
    ]

    VALID_DATA_PUT_NEW = [
        {
            "name": "float"
        }
    ]

    INVALID_DATA_400 = [
        {}
    ]

    INVALID_DATA_PUT_400 = [
        {
            "nam": "int"
        }, {}, {
            "name": ""
        }
    ]
