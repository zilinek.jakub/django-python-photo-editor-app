# @author Abi
import os
import unittest

from PIL import Image

# If we REALLY NEED TO run this straight from the filename, then uncommenting
# the next line is a way to make src available
# sys.path.append(Path(os.path.dirname(__file__)).parent.absolute().__str__())
from src.edit.reformat import Reformat
from src.edit.resize import Resize
from src.edit.watermark import Watermark
from src.photo import Photo
from test.utility import get_path


class PhotoEditingTestSuite(unittest.TestCase):
    legend_path_orig = get_path("test_photos/legend.png", __file__)
    legend_path_res = get_path("test_photos/output/legend_resize.png", __file__)
    legend_path_output_jpg = get_path("test_photos/output/legend_resize.jpeg", __file__)
    watermark_path = get_path("test_photos/watermark.png", __file__)
    marked_path = get_path("test_photos/output/watermark70.png", __file__)
    wmiddle_path = get_path("test_photos/output/watermark_middle.png", __file__)
    python_path = get_path("test_photos/python_logo.png", __file__)
    pythoned_path = get_path("test_photos/output/watermark_python.png", __file__)

    def setUp(self) -> None:
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'api.settings')

    def test_resize(self):
        editations = list()
        ed = Resize(500, 500)
        editations.append(ed)

        photo = Photo(self.legend_path_orig, editations)
        photo.execute_all_edits()

        photo.save(self.legend_path_res)
        img = Image.open(self.legend_path_res)
        self.assertEqual(img.size, (500, 500))
        img.close()

    def test_watermark(self):
        editations = list()

        water = Image.open(self.watermark_path)
        ed = Watermark(water, 0, 0, 180, 180, 70)
        editations.append(ed)

        photo = Photo(self.legend_path_res, editations)
        photo.execute_all_edits()

        photo.save(self.marked_path)

        editations.clear()
        ed = Watermark(water, 'middle', 'middle', 100, 100, 10)
        editations.append(ed)

        photo = Photo(self.legend_path_res, editations)
        photo.execute_all_edits()

        photo.save(self.wmiddle_path)

        editations.clear()
        water = Image.open(self.python_path)
        ed = Watermark(water, 'middle', 'middle', 100, 100, 50)
        editations.append(ed)

        photo = Photo(self.legend_path_res, editations)
        photo.execute_all_edits()

        photo.save(self.pythoned_path)

    def test_reformat(self):
        editations = [Reformat("jpeg", self.legend_path_orig)]

        photo = Photo(self.legend_path_orig, editations)
        photo.execute_all_edits()
        photo.save(self.legend_path_output_jpg)

        self.assertEqual(os.path.isfile(self.legend_path_output_jpg), True)

        editations = [Reformat("png", self.legend_path_output_jpg)]

        photo = Photo(self.legend_path_output_jpg, editations)
        photo.execute_all_edits()
        photo.save(self.legend_path_res)

        self.assertEqual(os.path.isfile(self.legend_path_res), True)

    def test_more_edits(self):
        editations = list()
        ed = Resize(500, 500)
        editations.append(ed)

        water = Image.open(self.watermark_path)
        ed = Watermark(water, 0, 0, 180, 180)
        editations.append(ed)

        ed = Resize(200, 200)
        editations.append(ed)

        photo = Photo(self.legend_path_orig, editations)
        photo.execute_all_edits()

        photo.save(self.legend_path_res)
        img = Image.open(self.legend_path_res)
        self.assertEqual(img.size, (200, 200))
        water.close()
        img.close()

    # def test_object_detection(self):
    #     editations = list()
    #     ed = Object_detection()
    #     editations.append(ed)
    #     photo = Photo("/app/mediagraphix/src/test_photos/legend.png", editations)
    #     photo.execute_all_edits()
    #     photo.save("/app/mediagraphix/src/test_photos/legend_obj_dete.png")
    #     print("END test_object_detection\n")


if __name__ == '__main__':
    unittest.main()
