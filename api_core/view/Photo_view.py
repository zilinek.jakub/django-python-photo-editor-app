import uuid
import json

from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
import base64
import mimetypes

from api_core.views import *

##TODO
class Photo_view(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=PhotoSerializer(many=True))}
    res_quary = {404: 'object for this slug wasn\'t found',
                 400: 'id in quary wasn\'t specified or request has bad format',
                 200: openapi.Response(description="response",
                                       schema=PhotoSerializer())}
    single = openapi.Response(description="response",
                              schema=PhotoSerializer())

    def find_by_id(self, _id):
        try:
            return Photo.objects.get(pk=_id)
        except Photo.DoesNotExist:
            return []

    @swagger_auto_schema(operation_summary=doc_sum_get("Photo"),
                         operation_description=doc_desc_get("Photo"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("photo")])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return get_by_id(_id, self.find_by_id, PhotoSerializer)
        photo = Photo.objects.all()
        serializer = PhotoSerializer(photo, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Add new Photo object",
                         responses={400: 'request has bad format',
                                    status.HTTP_201_CREATED: single},
                         request_body=PhotoSerializer)
    def post(self, request):
        data = {'file': request.data['photo'] if 'photo' in request.data else None}
        serializer = PhotoFileSerializer(data=data)

        if serializer.is_valid():
            file = serializer.validated_data.get("file")
            mime_type = mimetypes.guess_extension(file.content_type)
            file_name = './income_photos/' + uuid.uuid4().hex + mime_type
            with open(file_name, "wb") as f:
                f.write(file.read())
            file.close()
            photo2 = Photo.objects.create()
            OriginalPhoto(photo_path=file_name, photo=photo2).save()
            return Response(PhotoSerializer(photo2).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Update Photo",
                         operation_description=doc_desc_put("Photo"),
                         responses=res_quary,
                         manual_parameters=[doc_parm_id('photo')],
                         request_body=PhotoSerializer)
    def put(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return put_by_id(_id, self.find_by_id, PhotoSerializer, request)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(operation_summary="Delete Photo",
                         operation_description=doc_desc_delete("Photo"),
                         responses=delete_res,
                         manual_parameters=[doc_parm_id('photo')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id != '':
            return delete_by_id(_id, self.find_by_id)
        return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
