var toTextile = require('to-textile');

const fs = require("fs");
const buffer = fs.readFileSync("../README.html");
const fileContent = buffer.toString();

fs.writeFile('../README.textile', toTextile(fileContent), function (err) {
  if (err) return console.log(err);
  console.log('README.html \t> README.textile');
});