from typing import Any

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData


class RoleTestSuite(GeneralizedTestSuite):
    def setup_dependency(self):
        pass

    def get_url(self) -> str:
        return reverse("role")

    def get_test_data(self) -> TestData:
        return RoleTestData()


class RoleTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_201_CREATED): self.VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_POST_400,
            ("post", status.HTTP_409_CONFLICT): self.INVALID_DATA_POST_409,
            ("put", status.HTTP_200_OK): self.VALID_DATA_PUT,
            ("put", status.HTTP_400_BAD_REQUEST): self.INVALID_DATA_PUT_400,
            ("put", status.HTTP_409_CONFLICT): self.INVALID_DATA_PUT_409
        }

        if (method, status_code) not in data:
            return None
        return data[(method, status_code)]

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    IRRELEVANT_KEY = "askjdhfkjsadhfkjdhsaf"

    VALID_DATA = [
        {
            "name": "Operator",
            "type": "op1.photofire.com"
        }, {
            "name": "Admin",
            "type": "adm.profifoto.cz"
        }, {
            "name": "UserPF",
            "type": "user.photofire.com",
            IRRELEVANT_KEY: "test"
        }
    ]

    VALID_DATA_PUT = [
        {
            "id": 1,
            "name": "OP",
            "type": "op.photofire.com"
        }, {
            "id": 2,
            "name": "Administrator",
            "type": "admin.profifoto.cz",
            IRRELEVANT_KEY: "test"
        }
    ]

    INVALID_DATA_PUT_409 = [
        {
            "id": 2,
            "name": "Operator",
            "type": "adm.profifoto.cz"
        }
    ]

    INVALID_DATA_PUT_400 = [
        {
            "id": "",
            "name": "test",
            "type": "test"
        }, {}, {
            "id": 1,
            "name": "Příliš dlouhý název role",
            "type": "test"
        }, {
            "id": 1,
            "name": "Operator",
            "type": "Příliš dlouhy typ, který vyvolá chybu"
        }
    ]

    INVALID_DATA_POST_400 = [
        {
            "name": "test"
        }, {
            "type": "test"
        }, {}, {
            "name": "Příliš dlouhý název role",
            "type": "test"
        }, {
            "name": "test",
            "type": "Příliš dlouhý typ, který vyhodí chybu"
        }
    ]

    INVALID_DATA_POST_409 = [
        {
            "name": "Operator",
            "type": "test"
        }
    ]
