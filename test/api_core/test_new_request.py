import io
import json
from typing import Any
from unittest import skip

from django.urls import reverse
from rest_framework import status

from test.api_core.generalized_test import GeneralizedTestSuite, TestData
from test.api_core.test_account import AccountTestData
from test.api_core.test_role import RoleTestData
from test.api_core.test_user import UserTestData
from test.api_core.utility import create_id_url, date_str_eq, delete_generated_test_data, \
    format_assert_code_mismatch_help, \
    format_assert_fail_help
from test.utility import get_path

EDIT_DEPENDENCY = [
    {
        "name": "reformat",
    }, {
        "name": "watermark",
    }, {
        "name": "resize",
    }, {
        "name": "invert"
    }]

PRICE_DEPENDENCY = [
    {
        "price": 100,
        "edit": "reformat"
    }, {
        "price": 200,
        "edit": "watermark"
    }, {
        "price": 110,
        "edit": "resize"
    }, {
        "price": 110,
        "edit": "invert"
    }
]


class NewRequestTest(GeneralizedTestSuite):
    def setup_dependency(self):
        self.create_dependency_data(reverse('role'), RoleTestData.VALID_DATA)
        self.create_dependency_data(reverse('account'), AccountTestData.VALID_DATA)
        self.create_dependency_data(reverse('user'), UserTestData.VALID_DATA)

        self.create_dependency_data(reverse('edit'), EDIT_DEPENDENCY)
        self.create_dependency_data(reverse('price'), PRICE_DEPENDENCY)

    def get_url(self) -> str:
        return reverse("new_request")

    def get_test_data(self) -> TestData:
        return NewRequestTestData()

    def create_post_data(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_200_OK)

        if data_to_create is None:
            return

        self.method_test_data_src(self.client.post, data_to_create, status.HTTP_200_OK,
                                  self._test_data.get_universal_json_compare_function(),
                                  None,
                                  "multipart")

    def std_check(self, ent, resp, cmp=None):
        return resp.get("Content-Disposition") is not None

    def test_core_functionality_single_post_ok(self):
        data_to_create = self._test_data.get_test_data("post", status.HTTP_200_OK)[0]
        self.method_test_single(self.client.post, data_to_create,
                                status.HTTP_200_OK, self.std_check, None, "multipart")

        id_field_name = self._test_data.get_pk_field_name()
        id_value = self._test_data.get_pk_from_data(data_to_create)
        self.method_test_single(self.client.get, None, status.HTTP_200_OK,
                                lambda ent, resp:
                                self.std_check(data_to_create, resp),
                                create_id_url(self._url, 1 if id_value is None else id_value,
                                              id_field_name))

    def test_core_functionality_post_ok(self):
        for code in range(200, 300):
            data = self._test_data.get_test_data("post", code)
            if data is None:
                continue

            cmp_func = self._test_data.get_universal_json_compare_function()
            self.method_test_data_src(self.client.post, data, code, cmp_func, None, "multipart")

    def test_core_functionality_post_fail(self):
        self.create_post_data()
        for code in range(400, 600):
            data = self._test_data.get_test_data("post", code)
            if data is None:
                continue
            self.method_test_data_src(self.client.post, data, code, None, None, "multipart")

    def test_core_functionality_get_ok(self):
        self.create_post_data()

        data = self._test_data.get_test_data("post", status.HTTP_200_OK)

        for i, ent in enumerate(data):
            response = self.client.get(create_id_url(self._url, i + 1))
            self.assertTrue(self.std_check(ent, response),
                            format_assert_fail_help(ent, response, i + 1, self.std_check))
            self._assert_with_help(i, response, status.HTTP_200_OK)
        response = self.client.get(self._url+"?id=1&dl=fsghksghsoighroitjhjrhk")
        self._assert_with_help(None, response, status.HTTP_200_OK)

    @skip("Not used in view")
    def test_core_functionality_put_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_put_fail(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_ok(self):
        pass

    @skip("Not used in view")
    def test_core_functionality_delete_fail(self):
        pass

    def method_test_data_src(self,
                             method,
                             data,
                             expected_code,
                             additional_check_func=None,
                             url_get=None,
                             form="json"):
        if url_get is None:
            def url_get(*_): return self._url

        for index, ent in enumerate(data):
            response = method(url_get(index, ent), ent, format=form)

            try:
                formatted_debug_msg = format_assert_code_mismatch_help(ent,
                                                                       response.status_code,
                                                                       expected_code,
                                                                       response.content)
            except AttributeError:
                formatted_debug_msg = format_assert_code_mismatch_help(ent,
                                                                       response.status_code,
                                                                       expected_code,
                                                                       response.streaming_content)

            self.assertEquals(response.status_code, expected_code, formatted_debug_msg)
            if additional_check_func is None:
                continue

            try:
                self.assertTrue(additional_check_func(ent, response.data, index),
                                format_assert_fail_help(ent, response.data, index,
                                                        additional_check_func))
            except AttributeError:
                self.assertTrue(additional_check_func(ent, response, index),
                                "An exotic response was received (likely a file response)"
                                "and check function failed in its check, no further debug"
                                " message available.")

    def method_test_single(self,
                           method,
                           data,
                           expected_code,
                           additional_check_func=None,
                           url=None,
                           form="json"):
        if url is None:
            url = self._url

        response = method(url, data, format=form)
        try:
            formatted_debug_msg = format_assert_code_mismatch_help(data,
                                                                   response.status_code,
                                                                   expected_code,
                                                                   response.content)
        except AttributeError:
            formatted_debug_msg = format_assert_code_mismatch_help(data,
                                                                   response.status_code,
                                                                   expected_code,
                                                                   response.streaming_content)

        self.assertEquals(response.status_code, expected_code, formatted_debug_msg)

        if additional_check_func is not None:
            try:
                self.assertTrue(additional_check_func(data, response.data),
                                format_assert_fail_help(data, response.data, None,
                                                        additional_check_func))
            except AttributeError:
                self.assertTrue(additional_check_func(data, response),
                                "An exotic response was received (likely a file response) "
                                "and check function failed in its check, no further debug "
                                "message available.")

    def _assert_with_help(self, pk_used, response, expected_code):
        self.assertEqual(response.status_code, expected_code,
                         format_assert_code_mismatch_help({"primary_key": pk_used},
                                                          response.status_code,
                                                          expected_code,
                                                          response.get("content")))

    @classmethod
    def tearDownClass(cls):
        delete_generated_test_data()


class NewRequestTestData(TestData):
    def get_test_data(self, method: str, status_code: int) -> Any:
        data = {
            ("post", status.HTTP_200_OK): self.__VALID_DATA,
            ("post", status.HTTP_400_BAD_REQUEST): self.__INVALID_DATA_400,
            ("post", status.HTTP_402_PAYMENT_REQUIRED): self.__INVALID_DATA_402,
            ("post", status.HTTP_404_NOT_FOUND): self.__INVALID_DATA_404,
            ("post", status.HTTP_415_UNSUPPORTED_MEDIA_TYPE): self.__INVALID_DATA_415,
            ("post", status.HTTP_501_NOT_IMPLEMENTED): self.__INVALID_DATA_501,
        }

        if (method, status_code) not in data:
            return None

        result = data[(method, status_code)]
        self.__seek_begin(result)

        return result

    @classmethod
    def __seek_begin(cls, data):
        for entry in data:
            if not isinstance(entry, dict):
                continue

            if "photo" not in entry or not isinstance(entry["photo"], io.IOBase):
                continue

            entry["photo"].seek(0, 0)

            for i in range(1, 5):
                if "watermark_" + str(i) not in entry or not isinstance(
                        entry["watermark_" + str(i)], io.IOBase):
                    continue
                entry["watermark_" + str(i)].seek(0, 0)

    @classmethod
    def get_irrelevant_key(cls) -> str:
        return cls.IRRELEVANT_KEY

    @classmethod
    def detect_fields(cls, name: str, val1: Any, val2: Any) -> bool:
        d = {"datetime": date_str_eq}
        if name not in d:
            return val1 == val2
        return d[name](val1, val2)

    @classmethod
    def get_universal_json_compare_function(cls):
        def cmp_func(_1, resp, _2):
            return resp.get("Content-Disposition") is not None
        return cmp_func

    IRRELEVANT_KEY = "ladfklsjdfkjsdfvsnajnbbuh"

    __VALID_DATA = [
        {
            "photo": (open(get_path("../test_photos/legend.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "reformat",
                    "parameter_values": {
                        "format": "jpeg",
                        "width": 1,
                        "x": 100
                    }
                }]
            })
        },
        {
            "photo": (open(get_path("../test_photos/python_logo.png", __file__), "rb")),
            "json": json.dumps({
                "username": "PanK",
                "edits": [{
                    "edit": "resize",
                    "parameter_values": {
                        "format": "jpeg",
                        "width": 10,
                        "height": 1920,
                        "x": 100,
                        "y": 250,
                    }
                }]
            })
        }, {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "watermark_1": (open(get_path("../test_photos/legend.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        },
        # This one should not fail, as the content type is sent with it,
        # an issue of serialization I think
        # {
        #     "photo": ("figure_1.png", open(get_path("../test_photos/figure_1", __file__), "rb"),
        #               'image/png'),
        #     "json": json.dumps({
        #         "username": "abs(legend)",
        #         "edit": "resize",
        #         "parameter_values": {
        #             "width": 150,
        #             "height": 150,
        #             "x": 100,
        #             "y": 350,
        #             "transparency": 40
        #         }
        #     })
        # }
    ]

    __INVALID_DATA_400 = [
        {
            "datetime": "2021-10-22T19:10:35+00:00",
            "price": 100,
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "session_id": 1,
            "datetime": "2021-10-22T19:10:35+00:00",
            "state": "pending",
            "fkeditconf": 1,
            "fkuser": 1,
            "photo": 1
        }, {
            "watermark_1": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits:": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }, {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }, {
            "photo": (open(get_path("../test_photos/python_logo.png", __file__), "rb")),
            "json": json.dumps({
                "username": "PanK",
                "edits": [{
                    "edit": "resize",
                    "parameter_values": {
                        "y": 250,
                    }
                }]
            })
        }, {
            "json": json.dumps({
                "username": "PanK",
                "edits": [{
                    "edit": "resize",
                    "parameter_values": {
                        "format": "jpeg",
                        "width": 10,
                        "height": 1920,
                        "x": 100,
                        "y": 250,
                    }
                }]
            })
        }
    ]

    __INVALID_DATA_404 = [
        {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "watermark_1": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "non existing usr",
                "edits": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }, {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "watermark_1": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "WaTeRmArK",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }
    ]

    __INVALID_DATA_415 = [
        {
            "photo": (open(get_path("../../README.md", __file__), "rb")),
            "watermark_1": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }, {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "watermark_1": (open(get_path("../__init__.py", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 150,
                        "height": 150,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }
    ]

    __INVALID_DATA_402 = [
        {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "watermark_1": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "Imaroz",
                "edits": [{
                    "edit": "watermark",
                    "parameter_values": {
                        "width": 350,
                        "height": 160,
                        "x": 100,
                        "y": 350,
                        "transparency": 40
                    }
                }]
            })
        }
    ]

    __INVALID_DATA_501 = [
        {
            "photo": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "watermark_1": (open(get_path("../test_photos/watermark.png", __file__), "rb")),
            "json": json.dumps({
                "username": "abs(legend)",
                "edits": [{
                    "edit": "compress",
                    "parameter_values": {}
                }]
            })
        }
    ]
