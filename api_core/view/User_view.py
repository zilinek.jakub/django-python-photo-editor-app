from drf_yasg.utils import swagger_auto_schema

from api_core.views import *
from rest_framework.views import APIView
from ..serializers import *

userMultiple = openapi.Response(description="response for get without id",
                                schema=UserSerializer(many=True))
userSingle = openapi.Response(description="response",
                              schema=UserSerializer(many=False))


class User_view(APIView):

    def find_by_username(self, _user):
        try:
            return User.objects.get(username=_user)
        except User.DoesNotExist:
            return []

    def find_by_id(self, _id):
        try:
            return User.objects.get(pk=_id)
        except User.DoesNotExist:
            return []

    def get_user(self, request):
        _id = request.GET.get('id', '')
        _name = request.GET.get('username', '')

        if _id == '' and _name == '' or _id != '' and _name != '':
            return 400
        if _id != '':
            if not _id.isnumeric():
                return 400
            user = self.find_by_id(_id)
            if not user:
                return 404
            return user
        else:
            user = self.find_by_username(_name)
            if not user:
                return 404
            return user

    @swagger_auto_schema(operation_summary=doc_sum_get("User"),
                         operation_description="Return single User object if `id` or `username` in "
                                               "query is present or return all Users  objects",
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric or both username and id is present',
                                    status.HTTP_200_OK: userMultiple},
                         manual_parameters=[doc_parm_id_get('user'), doc_parm_username_get('user')])
    def get(self, request):
        _id = request.GET.get('id', '')
        _name = request.GET.get('username', '')
        if _id != '' and _name != '':
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        if _id != '':
            if not _id.isnumeric():
                return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
            user = self.find_by_id(_id)
            if not user:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        elif _name != '':
            user = self.find_by_username(_name)
            if not user:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            user = User.objects.all()
            serializer = UserSerializer(user, many=True)
            return Response(serializer.data)
        serializer = UserSerializer(user)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Update User",
                         operation_description="Update User object if `id` or `username` in "
                                               "query is present",
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric or both username and id is present'
                                         'or both username and id is NOT present',
                                    status.HTTP_200_OK: userSingle},
                         request_body=UserSerializer,
                         manual_parameters=[doc_parm_id('user'), doc_parm_username('user')])
    def put(self, request):
        user = self.get_user(request)
        if user == 400:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        if user == 404:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = UserSerializer(user, data=request.data)
        return serializer_validation_put(serializer)

    @swagger_auto_schema(operation_summary="Delete User",
                         operation_description="Delete User object if `id` or `username` in "
                                               "query is present",
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric or both username and id is present '
                                         'or both username and id is NOT present',
                                    },
                         manual_parameters=[doc_parm_id('user'), doc_parm_username('user')])
    def delete(self, request):
        user = self.get_user(request)
        if user == 400:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        if user == 404:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        user.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(operation_summary="Add new User object",
                         responses={status.HTTP_201_CREATED: userSingle},
                         request_body=UserSerializer)
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        return serializer_validation(serializer)

class User_fk(APIView):

    def find_by_fk_account(self, _fk):
        user = User.objects.all()
        res = []
        for i in user:
            tmp = i.fk_account_id
            if str(tmp) == str(_fk):
                res.append(i)
        return res

    @swagger_auto_schema(operation_summary="Retrieve User object ",
                         operation_description="Return single User object if `id` of account in "
                                               "query is present",
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric, id is NOT present',
                                    status.HTTP_200_OK: userSingle},
                         manual_parameters=[doc_parm_id('user/fk_account')])
    def get(self, request):
        _id = request.GET.get('id', '')
        if _id == '':
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

        user = self.find_by_fk_account(_id)
        if not user:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = UserSerializer(user, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(operation_summary="Delete User object with account id ",
                         operation_description="Delete User object if `id` of account in "
                                               "query is present",
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric, id is NOT present'},
                         manual_parameters=[doc_parm_id('user/fk_account')])
    def delete(self, request):
        _id = request.GET.get('id', '')
        if _id == '':
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

        user = self.find_by_fk_account(_id)
        if not user:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        for i in user:
            i.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class User_requests(APIView):
    def find_by_username(self, _user):
        try:
            return User.objects.get(username=_user)
        except User.DoesNotExist:
            return []

    def find_by_id(self, _id):
        try:
            return User.objects.get(id=_id)
        except User.DoesNotExist:
            return []

    def find_by_fk_user(self, _fk):
        request = Request.objects.all()
        res = []
        for i in request:
            tmp = i.fkuser_id
            if tmp == _fk:
                res.append(i)
        return res

    @swagger_auto_schema(operation_summary="Retrieve request for user with  ",
                         operation_description="Get `requests` from user if user `id`  or `username` in "
                                               "query is present",
                         responses={404: 'object for this slug wasn\'t found',
                                    400: 'id is not numeric, both username and id is present,'
                                         ' neither username and is is present',
                                    200: openapi.Response(description="response",
                                            schema=UserRequestSerializer(many=False))
                                    },
                         manual_parameters=[doc_parm_id('user/request'),
                                            doc_parm_username('user/request')])
    def get(self, request):
        _id = request.GET.get('id', '')
        _name = request.GET.get('username', '')

        if _id == '' and _name == '' or _id != '' and _name != '':
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)
        if _id != '':
            user = self.find_by_id(_id)
            if not user:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            user = self.find_by_username(_name)
            if not user:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        req = self.find_by_fk_user(user.id)
        if not req:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        serializer = UserRequestSerializer(data=user)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
