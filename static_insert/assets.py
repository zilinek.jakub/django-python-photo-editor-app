import os

import sys

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "api.settings")
import django

django.setup()

_editations = ['resize', 'reformat', 'watermark', 'compress', 'object_detection']
_prices = [("resize", 100), ("reformat", 100), ("watermark", 100), ("compress", 100), ("object_detection", 100)]
_types = ['int', 'double', 'photo', 'string']
_parameter = [
    ("height", ["resize", "watermark"], "int"),
    ("width", ["resize", "watermark"], "int"),
    ("x", ["watermark"], "int"),
    ("y", ["watermark"], "int"),
    ("photo", ["watermark"], "photo"),
    ("format", ["reformat"], "string"),
    ("percent", ["compress"], "int"),
    ("transparency", ["watermark"], "int")
]
_super = [('admin', 'admin'), ('more can be added here', 'password')]

from insert_editations import edit_insert
from insert_prices import insert_prices
from insert_types import type_insert
from create_super import create_super
from insert_parameters import parameter_insert

_functions = [edit_insert, insert_prices, type_insert, parameter_insert, create_super]
