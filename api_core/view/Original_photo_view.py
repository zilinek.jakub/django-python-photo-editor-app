from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView

from api_core.views import *


class OriginalPhotoView(APIView):
    res_many = {400: 'id is NOT numeric or request has bad format',
                404: 'object for this slug wasn\'t found',
                200: openapi.Response(
                    description="response for get without id",
                    schema=OriginalPhotoSerializer(many=True))}

    @swagger_auto_schema(operation_summary=doc_sum_get("OriginalPhoto"),
                         operation_description=doc_desc_get("OriginalPhoto"),
                         responses=res_many,
                         manual_parameters=[doc_parm_id_get("original_photo")])
    def get(self, request):
        _pk = request.GET.get('photo', '')
        if _pk != '':
            return get_by_pk(_pk, OriginalPhoto, OriginalPhotoSerializer)
        photo = OriginalPhoto.objects.all()
        serializer = OriginalPhotoSerializer(photo, many=True)
        return Response(serializer.data)
